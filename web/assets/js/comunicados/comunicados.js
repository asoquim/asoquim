// var $ = jQuery.noConflict(),
//     $wizard = $("#myWizard"),
//     $template = $(".template"),
//     $next = $(".next-btn"),
//     $editor;
// $(document).on("ready", function () {
//     $wizard.bootstrapWizard({
//         tabClass: 'nav nav-tabs nav-justified',
//         onTabShow: function (tab, navigation, index) {
//             var $total = navigation.find('li').length;
//             var $current = index + 1;
//             var $percent = ($current / $total) * 100;
//             $wizard.find(".progress-bar").css({width: $percent + '%'});
//         },
//         onShow: function () {
//             $template.on("click", function () {
//                 var $id = $(this).data("id");
//                 $next.attr("data-template", $id);
//             });
//         },
//         onNext: function (tab, navigation, index) {
//             if (index === 1) {
//                 var $id = $next.data("template");
//                 var $content = $("#template" + $id).data("content");
//                 $("#newsletter").append("<div id='gjs'></div>");
//                 grapesjs.init({
//                     container: "#gjs",
//                     plugins: ["gjs-preset-newsletter"]
//                 });
//                 $editor.setComponents($content);
//             }
//         }
//     });
// });

// var $ = jQuery.noConflict(),
//     $inputDate = $(".sendDateContainer"),
//     $schdl = $(".scheduleMsg"),
//     $send = $(".sendNow"),
//     wizard = $("#myWizard"),
//     template = $(".template"),
//     date = new Date(),
//     dd = date.getDate(),
//     mm = date.getMonth() + 1,
//     yyyy = date.getFullYear(),
//     today = dd + '-' + '0' + mm + '-' + yyyy,
//     html,
//     css,
//     editor;
// $(document).ready(function () {
//     $schdl.on("ifChecked", function () {
//         $send.iCheck("uncheck");
//         if ($inputDate.find("input").hasAttr("disabled")) {
//             $inputDate.find("input").removeAttr("disabled");
//         }
//     });
//     $send.on("ifChecked", function () {
//         $inputDate.find("input").val("");
//         $schdl.iCheck("uncheck");
//         if (!$inputDate.find("input").hasAttr("disabled")) {
//             $inputDate.find("input").attr("disabled", true);
//         }
//     });
//     template.on("click", function () {
//         var id = $(this).data("id");
//         $(".next-btn").data("template", id);
//         if (!$(this).hasClass('activeHover')) {
//             template.removeClass('activeHover').addClass('unactiveHover');
//             $(this).removeClass('unactiveHover').addClass('activeHover');
//         } else {
//             template.removeClass('activeHover unactiveHover');
//         }
//     });
//     wizard.bootstrapWizard({
//         tabClass: 'nav nav-tabs nav-justified',
//         onTabShow: function (tab, navigation, index) {
//             var $total = navigation.find('li').length;
//             var $current = index + 1;
//             var $percent = ($current / $total) * 100;
//             wizard.find(".progress-bar").css({width: $percent + '%'});
//         },
//         onNext: function (tab, navigation, index) {
//             if (index === 1) {
//                 var templateId = $(".next-btn").data("template");
//                 var content = $("#template" + templateId).data("content");
//                 $("#newsletter").append("<div id='gjs'></div>");
//                 editor = grapesjs.init({
//                     container: '#gjs',
//                     plugins: ['gjs-preset-newsletter'],
//                     pluginsOpts: {
//                         'gjs-preset-newsletter': {
//                             modalTitleImport: 'Import template'
//                             // ... other options
//                         }
//                     }
//                 });
//                 editor.setComponents(content);
//             }
//             if (index === 2) {
//                 css = editor.getCss();
//                 html = editor.getHtml();
//                 $(".finish").css("display", "block");
//             }
//         }
//     });
// });
// $(".finish a").on("click", function (event) {
//     event.preventDefault();
//     var contact = $("select#listaCorreos :selected"),
//         arr = [],
//         btn = $("#enviar"),
//         subject = $("#subject").val();
//     if (contact.length > 1) {
//         contact.map(function (i, o) {
//             arr[i] = $(o).val();
//         });
//     }
//     $.ajax({
//         url: "enviado",
//         type: "POST",
//         data: {
//             contacts: (contact.length > 1) ? arr : contact.val(),
//             subject: subject,
//             html: html,
//             css: css,
//             sendDate: ($inputDate.find("input").val() === "") ? today : $inputDate.find("input").val()
//         },
//         dataType: "JSON",
//         beforeSend: function () {
//             btn.button("loading");
//         },
//         success: function (data) {
//             console.log(data);
//             if (data.success) {
//                 $.notify({
//                     title: 'Comunicado enviado',
//                     text: 'Su comunicado ha sido enviado con éxito.',
//                     image: "<i class='fa fa-check'></i>"
//                 }, {
//                     style: 'metro',
//                     className: "success",
//                     showAnimation: "show",
//                     clickToHide: true,
//                     autoHide: false
//                 });
//             } else {
//                 $.notify({
//                     title: 'Ha ocurrido un error',
//                     text: 'Lamentablemente ha fallado el envío. Vuelva a intentar',
//                     image: "<i class='fa fa-remove'></i>"
//                 }, {
//                     style: 'metro',
//                     className: "error",
//                     showAnimation: "show",
//                     clickToHide: true,
//                     autoHide: false
//                 });
//             }
//         },
//         complete: function () {
//             btn.button("reset");
//         },
//         error: function (data) {
//             console.log(data);
//         }
//     });
// });