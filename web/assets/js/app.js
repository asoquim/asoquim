$('.btn-tooltip').tooltip();

$(document).ready(function () {
    var pagos = $("#afiliadosbundle_pagos");
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"],
        dateFormat: "d-mm-yy"
    }).on("change", function () {
        $(pagos).bootstrapValidator('updateStatus', 'fechaDelPago', 'VALID').bootstrapValidator('updateStatus', 'fechaDeCobranza', 'VALIDATING');
    });
});

$("#fundacionAddEmpresa").datepicker({
    maxDate: 0,
    dateFormat: "d-mm-yy"
}).on("change", function () {
    $("#formEmpresa").bootstrapValidator('updateStatus', 'fundacionEmpresa', 'VALID');
});

$(".fechaAprobacion").datepicker({
    minDate: 0,
    dateFormat: "d-mm-yy"
});

$(".fechaEnvio").datepicker({
    minDate: 0,
    dateFormat: "yy-mm-d",
    onSelect: function () {
        var date = $(this).val();
        $(this).val(date + " 00:00:00");
    }
});

$("#foto").click(function () {
    var foto = $("#fotoPerfil").click();
    foto.bind("change", function () {
        $("#subirFoto").click();
    });
});

function activarUsuario(id) {
    $("#activarU").val(id)
}

function desactivarUsuario(id) {
    $("#desactivarU").val(id)
}

function activarUser() {
    var id = $("#activarU").val();
    $.ajax({
        url: "activar_usuario",
        type: "POST",
        dataType: "JSON",
        data: {usuario: id},
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        error: function (resp) {
            console.log(resp);
        }
    })
}

function desactivarUser() {
    var id = $("#desactivarU").val();
    $.ajax({
        url: "desactivar_usuario",
        type: "POST",
        dataType: "JSON",
        data: {usuario: id},
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        error: function (resp) {
            console.log(resp);
        }
    })
}

function editarPerfil(id) {
    var nombre = $("#telefonoPerfil").data("nombre");
    var telefono = $("#telefonoPerfil").data("telefono");
    var correo = $("#correoPerfil").data("correo");
    var cargo = $("#cargoPerfil").data("cargo");
    var direccion = $('#direccionPerfil').data("direccion");
    var tipo = $("#telefonoPerfil").data("tipo");
    $("#idUsuario").val(id);
    $("input.nombreUsuario").val(nombre);
    $("input.telefonoUsuario").val(telefono);
    $("input.cargoUsuario").val(cargo);
    $("input.correoUsuario").val(correo);
    $("textarea.direccionUsuario").val(direccion);
    $("select.tipoUsuario option[value='" + tipo + "']").prop("selected", "selected");
}

function editarActividad(id) {
    var nombre = $("#nombre" + id).data("nombre");
    var fechaI = $("#fechaini" + id).data("fechai");
    var fechaF = $("#fechafin" + id).data("fechaf");
    var descrip = $("#desc" + id).data("desc");
    var status = $("#status" + id).data("status");
    var iduser = $("#iduser" + id).data("iduser");
    $("#idactividad").val(id);
    $("input.nombreActividad").val(nombre);
    $("input.editfechainicioa").val(fechaI);
    $("input.editfechafina").val(fechaF);
    $("textarea.descripcionActividad").val(descrip);
    $("select#statusActividad option[value='" + status + "']").prop("selected", "selected");
    $("select.usuarioAsignado option:contains('" + iduser + "')").prop("selected", "selected");
}

$("#editActivad").on("click", function () {
    var id = $("#idactividad")
    var fechaI = $("input.fechainicioactividad").val();
    var fechaF = $("input.fechafinactividad").val();
    var nombre = $("input.nombreActividad").val();
    var descrip = $("textarea.descripcionActividad").val();
    var status = $("select#statusActividad").val();
    var iduser = $("select.usuarioAsignado").val();
    $.ajax({
        url: "editar_actividad",
        type: "POST",
        dataType: "JSON",
        data: {
            id: id,
            fechaI: fechaI,
            fechaF: fechaF,
            nombre: nombre,
            descrip: descrip,
            status: status,
            iduser: iduser
        },
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        error: function (resp) {
            console.log(resp);
        }
    });
});

function editarUsuario(id) {
    var nombre = $("#nombreUsuario" + id).data("username");
    var telefono = $("#telefonoUsuario" + id).data("telefono");
    var cargo = $("#cargoUsuario" + id).data("cargo");
    var correo = $("#correoUsuario" + id).data("email");
    var direccion = $("#direccionUsuario" + id).data("address");
    var tipo = $("#telefonoUsuario" + id).data("tipo");
    $("#idUsuario").val(id);
    $("input.nombreUsuario").val(nombre);
    $("input.telefonoUsuario").val(telefono);
    $("input.cargoUsuario").val(cargo);
    $("input.correoUsuario").val(correo);
    $("textarea.direccionUsuario").val(direccion);
    $("select.tipoUsuario option[value='" + tipo + "']").prop("selected", "selected");
}

$("#editUser").on("click", function () {
    var id = $("#idUsuario").val();
    var nombre = $("input.nombreUsuario").val();
    var telefono = $("input.telefonoUsuario").val();
    var cargo = $("input.cargoUsuario").val();
    var correo = $("input.correoUsuario").val();
    var direccion = $("textarea.direccionUsuario").val();
    var tipo = $("select.tipoUsuario").val();
    $.ajax({
        url: "editar_usuario",
        type: "POST",
        dataType: "JSON",
        data: {
            id: id,
            nombre: nombre,
            telefono: telefono,
            cargo: cargo,
            correo: correo,
            direccion: direccion,
            tipo: tipo
        },
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        error: function (resp) {
            console.log(resp);
        }
    });
});

function activarContacto(id) {
    $("#activar").val(id);
}

function desactivarContacto(id) {
    $("#desactivar").val(id);
}

function activar() {
    var id = $("#activar").val();
    var btn = $(".btn-activar-contacto");
    console.log(btn);
    $.ajax({
        url: "activar_contacto",
        type: "POST",
        dataType: "JSON",
        data: {contacto: id},
        beforeSend: function () {
            btn.button("loading");
        },
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        complete: function () {
            btn.button("reset");
        },
        error: function (resp) {
            console.log(resp);
        }
    });
}

function desactivar() {
    var id = $("#desactivar").val();
    var btn = $(".btn-desactivar-contacto");
    $.ajax({
        url: "desactivar_contacto",
        type: "POST",
        dataType: "JSON",
        data: {contacto: id},
        beforeSend: function () {
            btn.button("loading");
        },
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        complete: function () {
            btn.button("reset");
        },
        error: function (resp) {
            console.log(resp);
        }
    });
}

function editarContacto(id) {
    var contacto = $("#nombreContacto" + id);
    var nombre = $(contacto).data("nombre");
    var telefonoOficina = $("#telefonoOficina" + id).data("telefono-oficina");
    var telefonoCelular = $("#telefonoCelular" + id).data("telefono-celular");
    var cargo = $("#cargoContacto" + id).data("cargo");
    var correo = $("#correoContacto" + id).data("correo");
    var rif = $("#rifContacto" + id).data("rif");
    var region = $("#regionContacto" + id).data("region");
    var representante = $(contacto).data("representante");
    $("#idContacto").val(id);
    $("input.nombreContact").val(nombre);
    $("input.telefonoOfice").val(telefonoOficina);
    $("input.telefonoCel").val(telefonoCelular);
    $("input.cargoContact").val(cargo);
    $("input.correoContact").val(correo);
    $("select.rifContact option:contains('" + rif + "')").prop("selected", "selected");
    $("select.regionContact option:contains('" + region + "')").prop("selected", "selected");
    if (representante == 1) {
        $("#esRepresentante").iCheck('check');
    } else {
        $("#esRepresentante").iCheck('uncheck');
    }
}

function editarPago(id) {
    var fecha = $("#fechaPago" + id);
    var fechaPago = $(fecha).data("fecha-pago");
    var monto = $("#montoPago" + id).data("monto");
    var rif = $(fecha).data("rif");
    var descripcion = $("#descripcionPago" + id).data("descripcion");
    $("#idPago").val(id);
    $("input.fechaPago").val(fechaPago);
    $("input.monto").val(monto);
    $("select.rifPago option:contains('" + rif + "')").prop("selected", "selected");
    $("textarea.descripcion").val(descripcion);
}

function eliminarPago(id) {
    $("#eliminarPago").val(id);
}

function eliminar() {
    var id = $("#eliminarPago").val();
    $.ajax({
        url: "../eliminar_pago",
        type: "POST",
        dataType: "JSON",
        data: {
            id: id
        },
        success: function (resp) {
            if (resp.success == true) {
                location.reload();
            }
        },
        error: function (resp) {
            console.log(resp);
        }
    });
}

$("*#contactoEmpresa").on("click", function () {
    var rif = $(this).data("rif");
    var empresa = $(this).data("empresa");
    $("#contactoId").val(rif);
    $("#empresaContacto").html(empresa);
});

$("#ingresarRazonCancelacion").on("click", function () {
    $("#razonCancelacion").toggleClass("shown");
});

$("#esconderRazonCancelacion").on("click", function () {
    $("#razonCancelacion").removeClass("shown");
    $("#razonCancelar").val("");
});

$("input.esRepresentante").on("ifChecked", function () {
    $("input.noEsRepresentante").iCheck("uncheck");
});

$("input.noEsRepresentante").on("ifChecked", function () {
    $("input.esRepresentante").iCheck("uncheck");
});

$("input.rasda_si").on("ifChecked", function () {
    $(".rasdaArchivo.disappeared").toggleClass("shown");
});

$("input.rasda_no").on("ifChecked", function () {
    $(".rasdaArchivo.disappeared").removeClass("shown");
});

$("input.manejadorSP_si").on("ifChecked", function () {
    $(".autorizacionArchivo.disappeared").toggleClass("shown");
});

$("input.manejadorSP_no").on("ifChecked", function () {
    $(".autorizacionArchivo.disappeared").removeClass("shown");
});

$("input.leyDrogas_si").on("ifChecked", function () {
    $(".leyDrogasArchivo.disappeared").toggleClass("shown");
});

$("input.leyDrogas_no").on("ifChecked", function () {
    $(".leyDrogasArchivo.disappeared").removeClass("shown");
});

$("input.operadorExplosivos_si").on("ifChecked", function () {
    $(".operadorArchivo.disappeared").toggleClass("shown");
});

$("input.operadorExplosivos_no").on("ifChecked", function () {
    $(".operadorArchivo.disappeared").removeClass("shown");
});

$("input.datoImportante_si").on("ifChecked", function () {
    $(".importanteArchivo.disappeared").toggleClass("shown");
});

$("input.datoImportante_no").on("ifChecked", function () {
    $(".importanteArchivo.disappeared").removeClass("shown");
});

$("input.pais_si").on("ifChecked", function () {
    $(".paisExportacion.disappeared").toggleClass("shown");
    $("input.pais_no").iCheck("uncheck");
});

$("input.pais_no").on("ifChecked", function () {
    $(".paisExportacion.disappeared").removeClass("shown");
    $("input.pais_si").iCheck("uncheck");
});

$("input#empresaAdherenteSi").on("ifChecked", function () {
    $("input#empresaAdherenteNo").iCheck("uncheck");
});

$("input#empresaAdherenteNo").on("ifChecked", function () {
    $("input#empresaAdherenteSi").iCheck("uncheck");
});

$(function () {
    var $form = $("#formMailList"),
        $alert = $("#response-content"),
        btn = $("#guardarLista");
    $form.on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "agregar_lista_correos",
            type: "POST",
            dataType: "JSON",
            data: $form.serialize(),
            beforeSend: function () {
                btn.button("loading");
            },
            success: function (data) {
                console.log(data);
                if (data.success) {
                    $alert.html('<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '  <span aria-hidden="true">&times;</span>\n' +
                        '</button> <strong>¡Felicidades!</strong> Se ha creado una nueva lista de contactos. </div>');
                    setTimeout(function () {
                        $("#nuevaListaCorreo").modal("hide");
                    }, 1500);
                } else {
                    $alert.html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '  <span aria-hidden="true">&times;</span>\n' +
                        '</button> <strong>¡Ocurrió un error!</strong> Por favor vuelva a intentar.</div>');
                }
            },
            error: function (data) {
                console.log(data);
                $alert.html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                    '  <span aria-hidden="true">&times;</span>\n' +
                    '</button> <strong>¡Ocurrió un error!</strong> Por favor vuelva a intentar.</div>');
            },
            complete: function () {
                btn.button("reset");
            }
        });
    });
    $(".modal").on("hidden.bs.modal", function () {
        $("input").val("");
        $(this).find(".selectpicker").selectpicker("deselectAll");
        $(".selectpicker").selectpicker("refresh");
        $alert.html("");
    });
});

$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
}

$(function () {
    var $btn = $("*#btn-approve"),
        $apr = $("#aprobarComunicado"),
        $id,
        $listaID,
        $almostAppr;
    $btn.on("click", function () {
        $id = $(this).data("id");
        $listaID = $(this).data("lista");
        $almostAppr = $(this).data("casiaprobado");
        if ($almostAppr == 1) {
            $("#msgApprove").css("display", "block");
        } else {
            $("#msgApprove").css("display", "none");
        }
    });
    $apr.on("click", function () {
        $.ajax({
            url: "aprobar_comunicado",
            type: "POST",
            data: {
                id: $id,
                lista: $listaID
            },
            dataType: "JSON",
            beforeSend: function () {
                $apr.button("loading");
            },
            success: function (rsp) {
                console.log(rsp);
                if (rsp.success === true) {
                    window.location.reload();
                }
                if (rsp.sent === true) {
                    $.notify({
                        title: 'Comunicado enviado',
                        text: 'Su comunicado ha sido aprobado con éxito. Se enviará a la hora programada.',
                        image: "<i class='fa fa-check'></i>"
                    }, {
                        style: 'metro',
                        className: "success",
                        showAnimation: "show",
                        clickToHide: true,
                        autoHide: false
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            },
            complete: function () {
                $apr.button("reset");
            },
            error: function (rsp) {
                $.notify({
                    title: 'Ha ocurrido un error',
                    text: 'Lamentablemente ha fallado el envío. Vuelva a intentar. ' + rsp,
                    image: "<i class='fa fa-remove'></i>"
                }, {
                    style: 'metro',
                    className: "error",
                    showAnimation: "show",
                    clickToHide: true,
                    autoHide: false
                });
            }
        });
    });
});

$(function () {
    var $id;
    $("*#btn-reject").on("click", function () {
        $id = $(this).data("id");
    });
    var $btn = $("#rechazarComunicado");
    $btn.on("click", function () {
        $.ajax({
            url: "rechazar_comunicado",
            type: "POST",
            dataType: "JSON",
            data: {id: $id},
            beforeSend: function () {
                $btn.button("loading");
            },
            success: function (rsp) {
                console.log(rsp);
                if (rsp.success === true) {
                    window.location.reload();
                }
            },
            complete: function () {
                $btn.button("reset");
            },
            error: function (rsp) {
                console.log(rsp);
            }
        });
    });
});


function preLoader() {
    var opts = {
            lines: 13
            , length: 17
            , width: 13
            , radius: 46
            , scale: 1
            , corners: 1
            , color: '#000'
            , opacity: 0.35
            , rotate: 0
            , direction: 1
            , speed: 1.3
            , trail: 60
            , fps: 20
            , zIndex: 2e9
            , className: 'spinner'
            , top: '50%'
            , left: '50%'
            , shadow: true
            , hwaccel: false
            , position: 'absolute'
        },
        target = document.getElementById('foo'),
        parent = arguments.callee.caller.name,
        spinner;
    if (parent == "beforeSend") {
        spinner = new Spinner(opts).spin(target);
        $(target).data('spinner', spinner);
    } else {
        $(target).data("spinner").stop();
    }
}

$(function () {
    var $lista = $("#listaCorreos");
    $("#toggleListaCorreos").on("ifChecked", function () {
        $lista.removeClass("isMailList");
        $("#toggleListaContactos").iCheck("uncheck");
        $lista.find("option").remove();
        $.ajax({
            url: "lista_correos_afiliados",
            type: "POST",
            dataType: "JSON",
            beforeSend: function () {
                preLoader();
            },
            success: function (data) {
                if (data.success === true) {
                    var correos = data.correos,
                        arr = [];
                    correos.map(function (i, o) {
                        arr[o] = i;
                    });
                    $.each(arr, function (i, o) {
                        $lista.append($("<option>", {
                            value: o.idContacto,
                            text: o.correo
                        }));
                    });
                    $(".selectpicker").selectpicker("refresh");
                }
            },
            complete: function () {
                preLoader();
            },
            error: function (jqXHR, textStatus, err) {
                console.log(err);
            }
        });
    });

    $("#toggleListaContactos").on("ifChecked", function () {
        $lista.addClass("isMailList");
        $("#toggleListaCorreos").iCheck("uncheck");
        $lista.find("option").remove();
        $.ajax({
            url: "lista_contactos_afiliados",
            type: "POST",
            dataType: "JSON",
            beforeSend: function () {
                preLoader();
            },
            success: function (data) {
                if (data.success === true) {
                    var contactos = data.contactos;
                    $(contactos).each(function (idx, obj) {
                        $lista.append($("<option>", {
                            value: obj.idLista,
                            text: obj.nombre
                        }));
                    });
                    $(".selectpicker").selectpicker("refresh");
                }
            },
            complete: function () {
                preLoader();
            },
            error: function (jqXHR, textStatus, err) {
                console.log(err);
            }
        });
    });
});

$("#filter").on("change", function (e) {
    var opt = $(this).val();
    $("#filter-container img").each(function (idx, obj) {
        if (opt == "all") {
            $(obj).show();
        } else if ($(obj).data("category") == opt) {
            $(obj).filter("[data-category='" + opt + "']").show();
        } else {
            $(obj).hide();
        }
    });
});

$("#actividadEmpresa").change(function () {
    if ($(this).val() == 1) {
        $(".nit.disappeared").toggleClass("shown");
    } else {
        $(".nit.disappeared").removeClass("shown");
    }
});

$("*#botonEliminarActividad").click(function () {
    var id = $(this).data("id");
    $("#inputId").val(id);
});

function empresaRechazada() {
    var rif = $("#empresaRechazada").val();
    var btn = $(".btn-rechazar-empresa");
    $.ajax({
        url: "rechazar_empresa",
        type: "POST",
        data: {rif: rif},
        dataType: "JSON",
        beforeSend: function () {
            btn.button("loading");
        },
        success: function (rsp) {
            if (rsp.success) {
                window.location.reload();
            }
        },
        complete: function () {
            btn.button("reset");
        },
        error: function (rsp) {
            console.log(rsp);
        }
    });
}

// Funciones para el módulo casos ----------------------------------

function crearActividad() {
    var lockDate = {
        'begin': $("#fechaCaso").data("fecha-inicio"),
        'end': $("#fechaCaso").data("fecha-fin")
    };
    $('input.fechainicioactividad').datepicker('option', 'minDate', lockDate['begin']);
    $('input.fechafinactividad').datepicker('option', 'maxDate', lockDate['end']);
}

$("#fechainicio").datepicker({
    dateFormat: "d-mm-yy",
    onSelect: function () {
        var lockDate = new Date($('#fechainicio').datepicker('getDate'));
        $('input#fechafin').datepicker('option', 'minDate', lockDate);
        //     // $('input.fechainicioactividad').datepicker('option', 'minDate', lockDate);
    }
});

$("#fechafin").datepicker({
    dateFormat: 'd-mm-yy',
    onSelect: function () {
        var lockDate = new Date($('#fechafin').datepicker('getDate'));
        // $('input.fechainicioactividad').datepicker('option', 'maxDate', lockDate);
        // $('input.fechafinactividad').datepicker('option', 'maxDate', lockDate);
    }
});

$(".fechainicioactividad").datepicker({
    minDate: 0,
    dateFormat: 'd-mm-yy',
    onSelect: function () {
        var lockDate = new Date($('.fechainicioactividad').datepicker('getDate'));
        $('input.fechafinactividad').datepicker('option', 'minDate', lockDate);
    }
});

$(".fechafinactividad").datepicker({
    dateFormat: 'd-mm-yy'
});

$(".editfechainicioa").datepicker({
    minDate: 0,
    dateFormat: 'dd-mm-yy',
    onSelect: function () {
        var lockDate = new Date($('.editfechainicioa').datepicker('getDate'));
        $('.editfechafina').datepicker('option', 'minDate', lockDate);
        $('.editfechainicioa').datepicker('option', 'minDate', lockDate);
    }
});

$(".editfechafina").datepicker({
    minDate: 0,
    dateFormat: 'dd-mm-yy',
    onSelect: function () {
        var lockDate = new Date($('.editfechafina').datepicker('getDate'));
        $('.editfechainicioa').datepicker('option', 'maxDate', lockDate);
        $('.editfechafina').datepicker('option', 'maxDate', lockDate);
    }
});

$(".editfechainicioactividad").datepicker({
    minDate: 0,
    dateFormat: 'dd-mm-yy',
    onSelect: function () {
        var lockDate = new Date($('.editfechainicioactividad').datepicker('getDate'));
        $('input.editfechafinactividad').datepicker('option', 'minDate', lockDate);
    }
});

$(".editfechafinactividad").datepicker({
    dateFormat: 'dd-mm-yy'
});

$("#fechacerrar").datepicker({
    dateFormat: 'dd-mm-yy'
});

function buscarcodigo(valor) {
    $.ajax({
        type: "POST",
        url: "buscar_codigo",
        data: {codigo: valor},
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                $('#mensaje').show();
            } else {
                $('#mensaje').hide();
            }
        }
    });
}

function fechacierre(valor) {
    if (valor <= 1) {
        $('#cerrar_caso').addClass("disappeared");
        $("#fechacerrar").val("");
    } else if (valor == 2) {
        $('#cerrar_caso').removeClass("disappeared");
    }
}

$("*#agregarSi").on("ifChecked", function () {
    $(".document-field").addClass("shown").attr("required");
    $(".documentocaso").attr("required", "required");
});

$("*#agregarNo").on("ifChecked", function () {
    $(".document-field").removeClass("shown");
    $(".documentocaso").removeAttr("required");
});


function editarCaso(id) {
    $.ajax({
        type: "POST",
        url: "editarCaso",
        data: {codigo: id},
        dataType: "json",
        beforeSend: function () {
            preLoader();
        },
        success: function (response) {
            var nombre = response['casos'][0].nombre,
                codigo = response['casos'][0].codigo,
                status = response['casos'][0].status,
                personaResponsable = response['casos'][0].personaResponsable,
                documento = response['casos'][0].documento,
                idDocumento = response['casos'][0].idDocumento,
                datecreacion = response['casos'][0].fechaCreacion.split('-'),
                datecierre = response['casos'][0].fechaCierre.split('-'),
                datemodificacion = (response['casos'][0].fechaModificacion) ? response['casos'][0].fechaModificacion.split('-') : '',
                descripcion = response['casos'][0].descripcion,
                personaResponsable = [];
            empresas = [];
            $.each(response['empresas'], function (k, v) {
                empresas.push(v['rif']);
            });
            console.log(descripcion);
            personaResponsable.push(response['casos'][0].id);
            $('#editarafiliado').selectpicker('val', empresas);
            $('#editarencargado').selectpicker('val', personaResponsable);
            $("#editarencargadooculto").val(personaResponsable);
            $('#editarnombreCaso').val(nombre);
            $('#editarcodigo').val(codigo);
            $('#editarfechainicio').val(datecreacion[2] + '-' + datecreacion[1] + '-' + datecreacion[0]);
            $('#editarfechafin').val(datecierre[2] + '-' + datecierre[1] + '-' + datecierre[0]);
            $('#editarstatus > option[value=' + status + ']').prop('selected', 'selected');
            $("textarea#descripcionCasoEditar").html(descripcion);
            $("#idDocumento").val(idDocumento);
            if (status == 2) {
                $("#editarnombreCaso, #editarfechainicio, #editarfechafin, #agregarSi, #agregarNo").attr("readonly", true);
                $("#editarencargado, #editarafiliado").attr("disabled", true);
            }
            if (datemodificacion) {
                $('#cerrar_caso').removeClass("disappeared");
                $("#fechacerrar").val(datemodificacion[2] + '-' + datemodificacion[1] + '-' + datemodificacion[0]);
                $("#fechacerrar").attr("required", "required");
            } else {
                $('#cerrar_caso').addClass("disappeared");
                $("#fechacerrar").val("").removeAttr("required");
            }
            if (documento) {
                $("#documentover").html("<div class='col-md-4'><div class='form-group'><a href='/assets/documentos_caso/" + documento + "' target='_blank'>Ver documento</a></div></div>");
            } else {
                $("#documentover").html("");
            }
        },
        complete: function () {
            preLoader();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

$(".asignarCodigoAfiliado").one("change", function (e) {
    var id = $(this).val();
    var cdg = $("#codigo");
    $.ajax({
        url: "buscar_codigo_afiliado",
        type: "POST",
        data: {id: id.slice(-1).pop()},
        dataType: "JSON",
        beforeSend: function () {
            cdg.addClass("loading-input");
        },
        complete: function () {
            cdg.removeClass("loading-input");
        }
    })
        .done(function (data) {
            if (data.caso != null) {
                cdg.val(data.caso);
            } else {
                cdg.val(data.codigo);
            }
        })
        .fail(function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        });
});

$("#editarCobranza").on("click", function () {
    var fecha = $(this).data("fecha");
    $(".editfechaDeCobranza").val(fecha);
});

//------------------------------------------------------------------------------
// Validaciones de los formularios.
//------------------------------------------------------------------------------

$("#afiliadosbundle_pagos_fecha_cobranza, #afiliadosbundle_pagos_editar_fecha_cobranza").bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        fechaCobranza: {
            validators: {
                notEmpty: {
                    message: "Debe indicar un día de cobranza"
                }
            }
        }
    }
});

$("#editarContacto").bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombreContact: {
            selector: '.nombreContact',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre de contacto.'
                },
                stringLength: {
                    min: 3,
                    message: 'El nombre no puede ser menor a 3 caracteres.'
                }
            }
        },
        correoContact: {
            selector: '.correoContact',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo de contacto.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        telefonoCel: {
            selector: '.telefonoCel',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un teléfono celular.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                }
            }
        },
        telefonoOfice: {
            selector: '.telefonoOfice',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un teléfono de oficina.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                }
            }
        },
        cargoContact: {
            selector: '.cargoContact',
            validators: {
                notEmpty: {
                    message: 'Debe indicar el cargo del contacto.'
                },
                stringLength: {
                    min: 3,
                    message: 'El cargo no puede ser menor a 3 caracteres.'
                }
            }
        },
        rifContact: {
            selector: '.rifContact',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una empresa.'
                }
            }
        },
        regionContact: {
            selector: '.regionContact',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una region.'
                }
            }
        }
    }
});

$('#adminbundle_registration').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombreUsuarioReg: {
            selector: '.nombreUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre de usuario'
                },
                stringLength: {
                    min: 3,
                    message: 'El nombre no puede ser menor a 3 caracteres.'
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9_\.]+$/,
                    message: 'El nombre de usuario solo puede tener caractere alfabéticos, numericos, puntos y pisos.'
                }
            }
        },
        correoUsuarioReg: {
            selector: '.correoUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo válido.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        telefUsuarioReg: {
            selector: '.telefonoUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un teléfono válido.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                }
            }
        },
        direccionUsuarioReg: {
            selector: '.direccionUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección válida.'
                },
                stringLength: {
                    min: 5,
                    max: 255,
                    message: 'Este campo debe tener entre 5 y 255 caracteres.'
                }
            }
        },
        cargoUserReg: {
            selector: '.cargoUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El cargo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        tipoUsuarioReg: {
            selector: '.tipoUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar un tipo de usuario.'
                }
            }
        },
        passwordUsuarioReg: {
            selector: '.passUsuarioReg',
            validators: {
                notEmpty: {
                    message: 'Debe indicar una contraseña.'
                }
            }
        }
    }
});

$('#veditar_pagos').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        fechaPago: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar la fecha de pago.'
                }
            }
        },
        fechaCobranza: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar la fecha de la cobranza.'
                }
            }
        },
        monto: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un monto válido.'
                }
            }
        },
        descripcion: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una descrición válida.'
                },
                stringLength: {
                    min: 5,
                    max: 255,
                    message: 'La descripción debe tener entre 5 y 255 caracteres.'
                }
            }
        }
    }
});

$('#afiliadosbundle_pagos').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        fechaDelPago: {
            selector: '.fechaDelPago',
            validators: {
                notEmpty: {
                    message: 'Debe indicar la fecha de pago.'
                }
            }
        },
        montoPago: {
            selector: '.montoPago',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un monto válido.'
                },
                digits: {
                    message: 'El monto debe tener solo números.'
                }
            }
        },
        descripcionPago: {
            selector: '.descripcionPago',
            validators: {
                notEmpty: {
                    message: 'Debe indicar una descrición válida.'
                },
                stringLength: {
                    min: 5,
                    max: 255,
                    message: 'La descripción debe tener entre 5 y 255 caracteres.'
                }
            }
        }
    }
});


$('#afiliadosbundle_contactos').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombreContacto: {
            selector: '.nombreContacto',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre para el contacto.'
                },
                stringLength: {
                    min: 3,
                    message: 'El nombre no puede ser menor a 3 caracteres.'
                }
            }
        },
        correoContacto: {
            selector: '.correoContacto',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo válido.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        telefonoCContacto: {
            selector: '.telefonoCContacto',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de teléfono celular válido.'
                },
                digits: {
                    message: 'El teléfono debe contener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono celular debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        telefonoOContacto: {
            selector: '.telefonoOContacto',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de teléfono de oficina válido.'
                },
                digits: {
                    message: 'El teléfono debe contener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono de oficina debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        cargoContacto: {
            selector: '.cargoContacto',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo para el contacto.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El cargo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        rifEmpresaContacto: {
            selector: '.rifEmpresaContacto',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una empresa.'
                }
            }
        },
        regionContacto: {
            selector: '.regionContacto',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una region.'
                }
            }
        }
    }
});

$('#afiliadosbundle_contactosempresa').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombre: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre para el contacto.'
                },
                stringLength: {
                    min: 3,
                    message: 'El nombre no puede ser menor a 3 caracteres.'
                }
            }
        },
        correo: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo válido.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        telefonoCelular: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de teléfono celular válido.'
                },
                digits: {
                    message: 'El teléfono debe contener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono celular debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        telefonoOficina: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de teléfono de oficina válido.'
                },
                digits: {
                    message: 'El teléfono debe contener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono de oficina debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        cargo: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo para el contacto.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El cargo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        region: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una región para el contacto.'
                }
            }
        }
    }
});

$('#adminbundle_usuarios').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombreUsuarioAdmin: {
            selector: '.nombreUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre de usuario.'
                },
                stringLength: {
                    min: 3,
                    message: 'El nombre no puede ser menor a 3 caracteres.'
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9_\.]+$/,
                    message: 'El nombre de usuario solo puede tener caractere alfabéticos, numericos, puntos y pisos.'
                }
            }
        },
        emailUsuarioAdmin: {
            selector: '.emailUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo válido.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        telefonoUsuarioAdmin: {
            selector: '.telefonoUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un teléfono válido.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                }
            }
        },
        cargoUsuarioAdmin: {
            selector: '.cargoUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El campo cargo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        passwordUsuarioAdmin: {
            selector: '.passwordUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar una contraseña.'
                },
                stringLength: {
                    min: 5,
                    max: 255,
                    message: 'La contraseña debe tener entre 5 y 255 caracteres.'
                }
            }
        },
        tipoUsuarioAdmin: {
            selector: '.tipoUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar un tipo de usuario.'
                }
            }
        },
        direccionUsuarioAdmin: {
            selector: '.direccionUsuarioAdmin',
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección válida.'
                }
            }
        }
    }
});

$("#aprobandoEmpresa").bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        codigoAfiliado: {
            validators: {
                notEmpty: {
                    message: 'Debe escribir un código.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El código no debe ser menor a 5 caracteres.'
                }
            }
        },
        fechaAprobacion: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una fecha de aprobación.'
                }
            }
        },
        tipoMiembro: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar el tipo de miembro.'
                }
            }
        },
        grupoAfiliado: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar el grupo al que pertenecerá la empresa.'
                }
            }
        },
        segmentoAfiliado: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar el segmento al que pertenecerá la empresa.'
                }
            }
        },
        sectorAfiliado: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar el sector al que pertenecerá la empresa.'
                }
            }
        }
    }
});

$('#formEmpresa').bootstrapValidator({
    message: 'Este valor no es válido.',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombreEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 120,
                    message: 'El nombre no debe ser menor a 5 caracteres.'
                }
            }
        },
        rifEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un rif válido.'
                },
                stringLength: {
                    min: 5,
                    max: 13,
                    message: 'El rif no debe tener entre 5 y 13 caracteres.'
                }
            }
        },
        registroEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de registro.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El registro debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        fundacionEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una fecha de fundación.'
                }
            }
        },
        actividadEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar la actividad que realiza.'
                }
            }
        },
        direccionFiscalEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección fiscal.'
                },
                stringLength: {
                    min: 5,
                    max: 80,
                    message: 'La dirección fiscal debe tener entre 5 y 80 caracteres.'
                }
            }
        },
        faxOficina: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de fax de oficina.'
                },
                digits: {
                    message: 'El fax debe tener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El fax debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        telefonoOficina: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de telefono.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        ventasBrutas: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una opción.'
                }
            }
        },
        webEmpresa: {
            validators: {
                uri: {
                    message: 'Debe indicar una URL válida.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'La url debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        telefonoPlanta: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número telefónico.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        faxPlanta: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número de fax.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El fax debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        correoEmpresa: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        direccionOficina: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección válida.'
                },
                stringLength: {
                    min: 5,
                    max: 100,
                    message: 'La dirección debe tener entre 6 y 100 caracteres.'
                }
            }
        },
        direccionPlanta: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección válida.'
                },
                stringLength: {
                    min: 5,
                    max: 100,
                    message: 'La dirección debe tener entre 5 y 100 caracteres.'
                }
            }
        },
        paisOrigenAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar un país.'
                }
            }
        },
        empresaOrigenAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El nombre de la empresa origen debe tener entre 5 y 45 caracateres.'
                }
            }
        },
        serviciosAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar al menos un servicio.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        responsableAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        materiaPrimaAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Este campo no puede estar vacío.'
                }
            }
        },
        productosAsistencia: {
            validators: {
                notEmpty: {
                    message: 'Este campo no puede estar vacío.'
                }
            }
        },
        porcentajePrivadoCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un porcentaje (%) en números.'
                },
                digits: {
                    message: 'El porcentaje de capital privado debe tener solo números.'
                }
            }
        },
        porcentajeExtranjeroCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un porcentaje (%) en números.'
                },
                digits: {
                    message: 'El porcentaje de capital extranjero debe tener solo números.'
                }
            }
        },
        paisOrigenCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar un país.'
                }
            }
        },
        porcentajePublicoCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un porcentaje (%) en números.'
                },
                digits: {
                    message: 'El porcentaje de capital público debe tener solo números.'
                }
            }
        },
        principalAccionistaCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        nombreCasaMatrizCapital: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        porcentajeActividadProductos: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un porcentaje (%) en números.'
                },
                digits: {
                    message: 'Este campo debe tener solo números.'
                }
            }
        },
        capacidadInstaladaProductos: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un valor.'
                },
                digits: {
                    message: 'La capacidad instalada debe tener solo números.'
                }
            }
        },
        productosFabricaProductos: {
            validators: {
                notEmpty: {
                    message: 'Este campo no puede estar vacío.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        nombrePrincipalPersonal: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        cargoPrincipalPersonal: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'Este campo debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        correoPrincipalPersonal: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        cantidadAdministrativo: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una cantidad en números.'
                },
                digits: {
                    message: 'Este campo debe tener solo números.'
                }
            }
        },
        cantidadAlmacen: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una cantidad en números.'
                },
                digits: {
                    message: 'Este campo debe tener solo números.'
                }
            }
        },
        nombreRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un nombre.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El nombre del representante debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        cargoRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un cargo.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'El cargo del representante debe tener entre 5 y 45 caracteres.'
                }
            }
        },
        direccionRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar una dirección válida.'
                },
                stringLength: {
                    min: 5,
                    max: 45,
                    message: 'La dirección del representante debe tener entre 5 y 80 caracteres.'
                }
            }
        },
        telefonoRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un número telefónico.'
                },
                digits: {
                    message: 'El teléfono debe tener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El teléfono debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        faxRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un fax válido.'
                },
                digits: {
                    message: 'El fax debe tener solo números.'
                },
                stringLength: {
                    min: 5,
                    max: 15,
                    message: 'El fax debe tener entre 5 y 15 caracteres.'
                }
            }
        },
        correoRepresentante: {
            validators: {
                notEmpty: {
                    message: 'Debe indicar un correo.'
                },
                emailAddress: {
                    message: 'El correo electrónico no tiene un formato válido.'
                }
            }
        },
        registroArchivo: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una imagen.'
                }
            }
        },
        rifArchivo: {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una imagen.'
                }
            }
        },
        impuestoArchivo: {
            selector: '#impuestoArchivo',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una imagen.'
                }
            }
        },
        ivaArchivo: {
            selector: '#ivaArchivo',
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar una imagen.'
                }
            }
        }
    }
});

//CODIGO DE COMUNICADOS

$("#listacorreoSi").on("ifClicked", function () {
    $("#listascorreos").removeClass("disappeared");
    $("#listacontactos").addClass("disappeared");
});

$("#contactosSi").on("ifClicked", function () {
    $("#listacontactos").removeClass("disappeared");
    $("#listascorreos").addClass("disappeared");
});

$("#send_now").on("ifClicked", function () {
    $("#grupofecha").addClass("disappeared");
});

$("#send_schedule").on("ifClicked", function () {
    $("#grupofecha").removeClass("disappeared");
});