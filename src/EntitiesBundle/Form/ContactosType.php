<?php

namespace EntitiesBundle\Form;

use Doctrine\ORM\EntityRepository;
use EntitiesBundle\EntitiesBundle;
use EntitiesBundle\Entity\Contactos;
use EntitiesBundle\Entity\Empresas;
use EntitiesBundle\Entity\Region;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idContacto', HiddenType::class)
            ->add('nombre')
            ->add('telefonoOficina')
            ->add('telefonoCelular')
            ->add('cargo', TextType::class)
            ->add('correo', EmailType::class)
            ->add('status', HiddenType::class, array(
                'data' => 1
            ))
            ->add('rif', EntityType::class, array(
                'class' => Empresas::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1');
                },
                'choice_label' => 'nombre'
            ))
            ->add('id', EntityType::class, array(
                'class' => Region::class
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Contactos::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'afiliadosbundle_contactos';
    }

}
