<?php

namespace EntitiesBundle\Form;

use EntitiesBundle\Entity\Actividades;
use EntitiesBundle\Entity\Casos;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActividadesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder/*->add('nombre')
            ->add('fechaInicio', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'd-M-y',
                'html5' => FALSE
            ))
            ->add('fechaCierre', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'd-M-y',
                'html5' => FALSE
            ))*/
            ->add('descripcion')
            ->add('status', HiddenType::class, array(
                'data' => 1
            ))
            ->add('idUsuario')
            ->add('codigo', HiddenType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EntitiesBundle\Entity\Actividades'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_actividades';
    }


}
