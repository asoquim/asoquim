<?php

namespace EntitiesBundle\Form;

use EntitiesBundle\Entity\Empresas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmpresasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rif', TextType::class)
            ->add('nombre')
            ->add('actividad', ChoiceType::class, array(
                'choices' => array(
                    '- Seleccione -' => '',
                    'Industrial' => '1',
                    'Comercios y Servicios' => 'Comercios y Servicios')
            ))
            ->add('fundacion', DateType::class)
            ->add('status', HiddenType::class, array(
                'data' => 3
            ))
            ->add('direccionFiscal')
            ->add('registroEmpresa')
            ->add('direccionOficina', TextareaType::class)
            ->add('telefonoOficina', NumberType::class)
            ->add('faxOficina')
            ->add('faxPlanta')
            ->add('direccionPlanta', TextareaType::class)
            ->add('telefonoPlanta', NumberType::class)
            ->add('correo', EmailType::class)
            ->add('sitioWeb')
            ->add('responsable')
            ->add('servicios')
            ->add('ventasBrutas', ChoiceType::class, array(
                'choices' => array(
                    '- Seleccione -' => '',
                    'Hasta 1 MM' => '1',
                    'De 1 MM a 6 MM' => '2',
                    'De 1 MM a 15 MM' => '3',
                    'De 15 MM a 30 MM' => '4',
                    'De 30 MM a 80 MM' => '5',
                    'Más de 80 MM' => '6'
                )
            ))
            ->add('fechaSolicitud', DateType::class)
            ->add('cantidadAdministrativo', NumberType::class)
            ->add('cantidadAlmacen', NumberType::class)
            ->add('fechaCreacion', DateTimeType::class, array(
                'data' => new \DateTime('now')
            ))
            ->add('fechaModificacion', HiddenType::class)
            ->add('razonDesactivacion', HiddenType::class)
            ->add('idGrupo', HiddenType::class)
            ->add('idPais')
            ->add('idSegmento', HiddenType::class)
            ->add('nombreEjecutivo')
            ->add('cargoEjecutivo')
            ->add('correoEjecutivo', EmailType::class)
            ->add('idAsistenciaTecnica', CollectionType::class, array(
                'entry_type' => AsistenciaTecnicaType::class,
                'label' => FALSE,
                'allow_add' => TRUE,
                'allow_delete' => TRUE
            ));
//            ->add('AsistenciaTecnica', AsistenciaTecnicaType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Capital', CapitalType::class, array(
//                'label' => FALSE
//            ))
//            ->add('ProductosEmpresa', ProductosEmpresaType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Representantes', RepresentantesType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Referencias', RepeatedType::class, array(
//                'type' => ReferenciasType::class,
//                'label' => FALSE
//            ))
//            ->add('Exportaciones', ExportacionesType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Archivos', ArchivosType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Iva', IvaType::class, array(
//                'label' => FALSE
//            ))
//            ->add('Islr', IslrType::class, array(
//                'label' => FALSE
//            ))
//            ->add('ProductosControlados', ProductosControladosType::class, array(
//                'label' => FALSE
//            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Empresas::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'afiliadosbundle_empresas';
    }
}
