<?php

namespace EntitiesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuariosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class)
            ->add('email')
            ->add('telefono')
            ->add('direccion')
            ->add('cargo')
            ->add('roles')
            ->add('status', HiddenType::class, array(
                'data' => 1
            ))
            ->add('tipoUsuario', ChoiceType::class, array(
                'choices' => array(
                    '- Seleccione -' => '',
                    'Dirección Ejecutiva' => 1,
                    'Gerente de comercio' => 2,
                    'Coord. de estadísticas y análisis económico' => 3,
                    'Gerente de Responsabilidad Integral' => 4,
                    'Coord. Técnicas de Responsabilidad Integral' => 5,
                    'Coord. De Normas Técnicas y Aseguramiento de la Calidad' => 6,
                    'Gerente de Administración' => 7,
                    'Asistente Administrativo I' => 8,
                    'Asistente Administrativo II' => 9
                ),
            ))
            ->add('mailerContact', CheckboxType::class, array(
                'required' => FALSE
            ))
            ->add('fechaCreacion', HiddenType::class)
            ->add('fechaModificacion', HiddenType::class)
            ->add('enabled', HiddenType::class, array(
                'data' => 1
            ))
            ->add('foto', HiddenType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EntitiesBundle\Entity\Usuarios'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_usuarios';
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

}
