<?php

namespace EntitiesBundle\Entity;

/**
 * EmpresasCasos
 */
class EmpresasCasos
{
    /**
     * @var integer
     */
    private $idEmpresaCasos;

    /**
     * @var \EntitiesBundle\Entity\Casos
     */
    private $codigo;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \EntitiesBundle\Entity\Usuarios
     */
    private $idUsuario;


    /**
     * Set idEmpresaCasos
     *
     * @param integer $idEmpresaCasos
     *
     * @return EmpresasCasos
     */
    public function setIdEmpresaCasos($idEmpresaCasos)
    {
        $this->idEmpresaCasos = $idEmpresaCasos;

        return $this;
    }

    /**
     * Get idEmpresaCasos
     *
     * @return integer
     */
    public function getIdEmpresaCasos()
    {
        return $this->idEmpresaCasos;
    }

    /**
     * Set codigo
     *
     * @param \EntitiesBundle\Entity\Casos $codigo
     *
     * @return EmpresasCasos
     */
    public function setCodigo(\EntitiesBundle\Entity\Casos $codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return \EntitiesBundle\Entity\Casos
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return EmpresasCasos
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set idUsuario
     *
     * @param \EntitiesBundle\Entity\Usuarios $idUsuario
     *
     * @return EmpresasCasos
     */
    public function setIdUsuario(\EntitiesBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \EntitiesBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
