<?php

namespace EntitiesBundle\Entity;

/**
 * StatusCasos
 */
class StatusCasos
{
    /**
     * @var integer
     */
    private $idStatus;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Casos
     */
    private $codigo;


    /**
     * Set idStatus
     *
     * @param integer $idStatus
     *
     * @return StatusCasos
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return StatusCasos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set codigo
     *
     * @param \EntitiesBundle\Entity\Casos $codigo
     *
     * @return StatusCasos
     */
    public function setCodigo(\EntitiesBundle\Entity\Casos $codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return \EntitiesBundle\Entity\Casos
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
