<?php

namespace EntitiesBundle\Entity;

/**
 * StatusContacto
 */
class StatusContacto
{
    /**
     * @var integer
     */
    private $idStatus;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Contactos
     */
    private $idContacto;


    /**
     * Set idStatus
     *
     * @param integer $idStatus
     *
     * @return StatusContacto
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return StatusContacto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idContacto
     *
     * @param \EntitiesBundle\Entity\Contactos $idContacto
     *
     * @return StatusContacto
     */
    public function setIdContacto(\EntitiesBundle\Entity\Contactos $idContacto = null)
    {
        $this->idContacto = $idContacto;

        return $this;
    }

    /**
     * Get idContacto
     *
     * @return \EntitiesBundle\Entity\Contactos
     */
    public function getIdContacto()
    {
        return $this->idContacto;
    }
}
