<?php

namespace EntitiesBundle\Entity;

/**
 * Sector
 */
class Sector
{
    /**
     * @var integer
     */
    private $idSector;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Get idSector
     *
     * @return integer
     */
    public function getIdSector()
    {
        return $this->idSector;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Sector
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }


    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rif = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Sector
     */
    public function addRif(\EntitiesBundle\Entity\Empresas $rif)
    {
        $this->rif[] = $rif;

        return $this;
    }

    /**
     * Remove rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     */
    public function removeRif(\EntitiesBundle\Entity\Empresas $rif)
    {
        $this->rif->removeElement($rif);
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
}
