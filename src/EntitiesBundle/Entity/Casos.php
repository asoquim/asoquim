<?php

namespace EntitiesBundle\Entity;

/**
 * Casos
 */
class Casos
{
    /**
     * @var string
     */
    public $codigo;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \Date
     */
    private $fechaCreacion;

    /**
     * @var \Date
     */
    private $fechaCierre;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \Date
     */
    private $fechaModificacion;

    
    /**
     * @var string
     */
    private $personaResponsable;

    /**
     * @var \EntitiesBundle\Entity\Usuarios
     */
    private $idUsuario;


    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Casos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaCreacion
     *
     * @param \Date $fechaCreacion
     *
     * @return Casos
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \Date
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaCierre
     *
     * @param \Date $fechaCierre
     *
     * @return Casos
     */
    public function setFechaCierre($fechaCierre)
    {
        $this->fechaCierre = $fechaCierre;

        return $this;
    }

    /**
     * Get fechaCierre
     *
     * @return \Date
     */
    public function getFechaCierre()
    {
        return $this->fechaCierre;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Casos
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fechaModificacion
     *
     * @param \Date $fechaModificacion
     *
     * @return Casos
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \Date
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }
    

    /**
     * Set personaResponsable
     *
     * @param string $personaResponsable
     *
     * @return Casos
     */
    public function setPersonaResponsable($personaResponsable)
    {
        $this->personaResponsable = $personaResponsable;

        return $this;
    }

    /**
     * Get personaResponsable
     *
     * @return string
     */
    public function getPersonaResponsable()
    {
        return $this->personaResponsable;
    }


    /**
     * Set idUsuario
     *
     * @param \EntitiesBundle\Entity\Usuarios $idUsuario
     *
     * @return Casos
     */
    public function setIdUsuario(\EntitiesBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \EntitiesBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Casos
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idActividad;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idActividad = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idActividad
     *
     * @param \EntitiesBundle\Entity\Actividades $idActividad
     *
     * @return Casos
     */
    public function addIdActividad(\EntitiesBundle\Entity\Actividades $idActividad)
    {
        $this->idActividad[] = $idActividad;

        return $this;
    }

    /**
     * Remove idActividad
     *
     * @param \EntitiesBundle\Entity\Actividades $idActividad
     */
    public function removeIdActividad(\EntitiesBundle\Entity\Actividades $idActividad)
    {
        $this->idActividad->removeElement($idActividad);
    }

    /**
     * Get idActividad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }
    /**
     * @var string
     */
    private $descripcion;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Casos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idDocumento;


    /**
     * Add idDocumento
     *
     * @param \EntitiesBundle\Entity\DocumentosCasos $idDocumento
     *
     * @return Casos
     */
    public function addIdDocumento(\EntitiesBundle\Entity\DocumentosCasos $idDocumento)
    {
        $this->idDocumento[] = $idDocumento;

        return $this;
    }

    /**
     * Remove idDocumento
     *
     * @param \EntitiesBundle\Entity\DocumentosCasos $idDocumento
     */
    public function removeIdDocumento(\EntitiesBundle\Entity\DocumentosCasos $idDocumento)
    {
        $this->idDocumento->removeElement($idDocumento);
    }

    /**
     * Get idDocumento
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdDocumento()
    {
        return $this->idDocumento;
    }
}
