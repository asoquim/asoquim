<?php

namespace EntitiesBundle\Entity;

/**
 * Empresas
 */
class Empresas
{
    /**
     * @var string
     */
    private $rif;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $actividad;

    /**
     * @var \DateTime
     */
    private $fundacion;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $direccionFiscal;

    /**
     * @var string
     */
    private $registroEmpresa;

    /**
     * @var string
     */
    private $direccionOficina;

    /**
     * @var integer
     */
    private $telefonoOficina;

    /**
     * @var string
     */
    private $faxOficina;

    /**
     * @var string
     */
    private $faxPlanta;

    /**
     * @var string
     */
    private $direccionPlanta;

    /**
     * @var integer
     */
    private $telefonoPlanta;

    /**
     * @var string
     */
    private $correo;

    /**
     * @var string
     */
    private $sitioWeb;

    /**
     * @var string
     */
    private $responsable;

    /**
     * @var string
     */
    private $servicios;

    /**
     * @var float
     */
    private $ventasBrutas;

    /**
     * @var \DateTime
     */
    private $fechaSolicitud;

    /**
     * @var integer
     */
    private $cantidadAdministrativo;

    /**
     * @var integer
     */
    private $cantidadAlmacen;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaModificacion;

    /**
     * @var \DateTime
     */
    private $fechaAprobacion;

    /**
     * @var string
     */
    private $razonDesactivacion;

    /**
     * @var string
     */
    private $nit;

    /**
     * @var string
     */
    private $tipoMiembro;

    /**
     * @var boolean
     */
    private $empresaAdherente;

    /**
     * @var \EntitiesBundle\Entity\GrupoEmpresa
     */
    private $idGrupo;

    /**
     * @var \EntitiesBundle\Entity\SegmentoEmpresa
     */
    private $idSegmento;

    /**
     * @var string
     */
    private $nombreEjecutivo;

    /**
     * @var string
     */
    private $cargoEjecutivo;

    /**
     * @var string
     */
    private $correoEjecutivo;

    /**
     * @var string
     */
    private $codigoAfiliado;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $idAsistenciaTecnica;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idCapital;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idAsistenciaTecnica = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idCapital = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idArchivo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idExportacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idProductosEmpresa = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idProductoControlado = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set rif
     *
     * @param string $rif
     *
     * @return Empresas
     */
    public function setRif($rif)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return string
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Empresas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set actividad
     *
     * @param integer $actividad
     *
     * @return Empresas
     */
    public function setActividad($actividad)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return integer
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set fundacion
     *
     * @param \DateTime $fundacion
     *
     * @return Empresas
     */
    public function setFundacion($fundacion)
    {
        $this->fundacion = $fundacion;

        return $this;
    }

    /**
     * Get fundacion
     *
     * @return \DateTime
     */
    public function getFundacion()
    {
        return $this->fundacion;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Empresas
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set direccionFiscal
     *
     * @param string $direccionFiscal
     *
     * @return Empresas
     */
    public function setDireccionFiscal($direccionFiscal)
    {
        $this->direccionFiscal = $direccionFiscal;

        return $this;
    }

    /**
     * Get direccionFiscal
     *
     * @return string
     */
    public function getDireccionFiscal()
    {
        return $this->direccionFiscal;
    }

    /**
     * Set registroEmpresa
     *
     * @param string $registroEmpresa
     *
     * @return Empresas
     */
    public function setRegistroEmpresa($registroEmpresa)
    {
        $this->registroEmpresa = $registroEmpresa;

        return $this;
    }

    /**
     * Get registroEmpresa
     *
     * @return string
     */
    public function getRegistroEmpresa()
    {
        return $this->registroEmpresa;
    }

    /**
     * Set direccionOficina
     *
     * @param string $direccionOficina
     *
     * @return Empresas
     */
    public function setDireccionOficina($direccionOficina)
    {
        $this->direccionOficina = $direccionOficina;

        return $this;
    }

    /**
     * Get direccionOficina
     *
     * @return string
     */
    public function getDireccionOficina()
    {
        return $this->direccionOficina;
    }

    /**
     * Set telefonoOficina
     *
     * @param integer $telefonoOficina
     *
     * @return Empresas
     */
    public function setTelefonoOficina($telefonoOficina)
    {
        $this->telefonoOficina = $telefonoOficina;

        return $this;
    }

    /**
     * Get telefonoOficina
     *
     * @return integer
     */
    public function getTelefonoOficina()
    {
        return $this->telefonoOficina;
    }

    /**
     * Set faxOficina
     *
     * @param string $faxOficina
     *
     * @return Empresas
     */
    public function setFaxOficina($faxOficina)
    {
        $this->faxOficina = $faxOficina;

        return $this;
    }

    /**
     * Get faxOficina
     *
     * @return string
     */
    public function getFaxOficina()
    {
        return $this->faxOficina;
    }

    /**
     * Set faxPlanta
     *
     * @param string $faxPlanta
     *
     * @return Empresas
     */
    public function setFaxPlanta($faxPlanta)
    {
        $this->faxPlanta = $faxPlanta;

        return $this;
    }

    /**
     * Get faxPlanta
     *
     * @return string
     */
    public function getFaxPlanta()
    {
        return $this->faxPlanta;
    }

    /**
     * Set direccionPlanta
     *
     * @param string $direccionPlanta
     *
     * @return Empresas
     */
    public function setDireccionPlanta($direccionPlanta)
    {
        $this->direccionPlanta = $direccionPlanta;

        return $this;
    }

    /**
     * Get direccionPlanta
     *
     * @return string
     */
    public function getDireccionPlanta()
    {
        return $this->direccionPlanta;
    }

    /**
     * Set telefonoPlanta
     *
     * @param integer $telefonoPlanta
     *
     * @return Empresas
     */
    public function setTelefonoPlanta($telefonoPlanta)
    {
        $this->telefonoPlanta = $telefonoPlanta;

        return $this;
    }

    /**
     * Get telefonoPlanta
     *
     * @return integer
     */
    public function getTelefonoPlanta()
    {
        return $this->telefonoPlanta;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Empresas
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set sitioWeb
     *
     * @param string $sitioWeb
     *
     * @return Empresas
     */
    public function setSitioWeb($sitioWeb)
    {
        $this->sitioWeb = $sitioWeb;

        return $this;
    }

    /**
     * Get sitioWeb
     *
     * @return string
     */
    public function getSitioWeb()
    {
        return $this->sitioWeb;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return Empresas
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set servicios
     *
     * @param string $servicios
     *
     * @return Empresas
     */
    public function setServicios($servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * Get servicios
     *
     * @return string
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Set ventasBrutas
     *
     * @param float $ventasBrutas
     *
     * @return Empresas
     */
    public function setVentasBrutas($ventasBrutas)
    {
        $this->ventasBrutas = $ventasBrutas;

        return $this;
    }

    /**
     * Get ventasBrutas
     *
     * @return float
     */
    public function getVentasBrutas()
    {
        return $this->ventasBrutas;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     *
     * @return Empresas
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set cantidadAdministrativo
     *
     * @param integer $cantidadAdministrativo
     *
     * @return Empresas
     */
    public function setCantidadAdministrativo($cantidadAdministrativo)
    {
        $this->cantidadAdministrativo = $cantidadAdministrativo;

        return $this;
    }

    /**
     * Get cantidadAdministrativo
     *
     * @return integer
     */
    public function getCantidadAdministrativo()
    {
        return $this->cantidadAdministrativo;
    }

    /**
     * Set cantidadAlmacen
     *
     * @param integer $cantidadAlmacen
     *
     * @return Empresas
     */
    public function setCantidadAlmacen($cantidadAlmacen)
    {
        $this->cantidadAlmacen = $cantidadAlmacen;

        return $this;
    }

    /**
     * Get cantidadAlmacen
     *
     * @return integer
     */
    public function getCantidadAlmacen()
    {
        return $this->cantidadAlmacen;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Empresas
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Empresas
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set razonDesactivacion
     *
     * @param string $razonDesactivacion
     *
     * @return Empresas
     */
    public function setRazonDesactivacion($razonDesactivacion)
    {
        $this->razonDesactivacion = $razonDesactivacion;

        return $this;
    }

    /**
     * Get razonDesactivacion
     *
     * @return string
     */
    public function getRazonDesactivacion()
    {
        return $this->razonDesactivacion;
    }

    /**
     * Set idGrupo
     *
     * @param \EntitiesBundle\Entity\GrupoEmpresa $idGrupo
     *
     * @return Empresas
     */
    public function setIdGrupo(\EntitiesBundle\Entity\GrupoEmpresa $idGrupo = null)
    {
        $this->idGrupo = $idGrupo;

        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \EntitiesBundle\Entity\GrupoEmpresa
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set idSegmento
     *
     * @param \EntitiesBundle\Entity\SegmentoEmpresa $idSegmento
     *
     * @return Empresas
     */
    public function setIdSegmento(\EntitiesBundle\Entity\SegmentoEmpresa $idSegmento = null)
    {
        $this->idSegmento = $idSegmento;

        return $this;
    }

    /**
     * Get idSegmento
     *
     * @return \EntitiesBundle\Entity\SegmentoEmpresa
     */
    public function getIdSegmento()
    {
        return $this->idSegmento;
    }

    /**
     * Set nombreEjecutivo
     *
     * @param string $nombreEjecutivo
     *
     * @return Empresas
     */
    public function setNombreEjecutivo($nombreEjecutivo)
    {
        $this->nombreEjecutivo = $nombreEjecutivo;

        return $this;
    }

    /**
     * Get nombreEjecutivo
     *
     * @return string
     */
    public function getNombreEjecutivo()
    {
        return $this->nombreEjecutivo;
    }

    /**
     * Set cargoEjecutivo
     *
     * @param string $cargoEjecutivo
     *
     * @return Empresas
     */
    public function setCargoEjecutivo($cargoEjecutivo)
    {
        $this->cargoEjecutivo = $cargoEjecutivo;

        return $this;
    }

    /**
     * Get idAsistenciaTecnica
     *
     * @return \EntitiesBundle\Entity\AsistenciaTecnica
     */
    public function getIdAsistenciaTecnica()
    {
        return $this->idAsistenciaTecnica;
    }

    /**
     * Set idAsistenciaTecnica
     *
     * @param \EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica
     *
     * @return Empresas
     */
    public function setIdAsistenciaTecnica(\EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica)
    {
        $this->idAsistenciaTecnica = $idAsistenciaTecnica;
        return $this;
    }

    /**
     * Get cargoEjecutivo
     *
     * @return string
     */
    public function getCargoEjecutivo()
    {
        return $this->cargoEjecutivo;
    }

    /**
     * Set correoEjecutivo
     *
     * @param string $correoEjecutivo
     *
     * @return Empresas
     */
    public function setCorreoEjecutivo($correoEjecutivo)
    {
        $this->correoEjecutivo = $correoEjecutivo;

        return $this;
    }

    /**
     * Get correoEjecutivo
     *
     * @return string
     */
    public function getCorreoEjecutivo()
    {
        return $this->correoEjecutivo;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Add idAsistenciaTecnica
     *
     * @param \EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica
     *
     * @return Empresas
     */
    public function addIdAsistenciaTecnica(\EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica)
    {
        $this->idAsistenciaTecnica[] = $idAsistenciaTecnica;
//        $idAsistenciaTecnica->setIdAsistenciaTecnica($this);
        return $this;
    }

    /**
     * Remove idAsistenciaTecnica
     *
     * @param \EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica
     */
    public function removeIdAsistenciaTecnica(\EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica)
    {
        $this->idAsistenciaTecnica->removeElement($idAsistenciaTecnica);
    }

    /**
     * Set codigoAfiliado
     *
     * @param string $codigoAfiliado
     *
     * @return Empresas
     */
    public function setCodigoAfiliado($codigoAfiliado)
    {
        $this->codigoAfiliado = $codigoAfiliado;

        return $this;
    }

    /**
     * Get codigoAfiliado
     *
     * @return string
     */
    public function getCodigoAfiliado()
    {
        return $this->codigoAfiliado;
    }

    /**
     * Set fechaAprobacion
     *
     * @param \DateTime $fechaAprobacion
     *
     * @return Empresas
     */
    public function setFechaAprobacion($fechaAprobacion)
    {
        $this->fechaAprobacion = $fechaAprobacion;

        return $this;
    }

    /**
     * Get fechaAprobacion
     *
     * @return \DateTime
     */
    public function getFechaAprobacion()
    {
        return $this->fechaAprobacion;
    }

    /**
     * Set tipoMiembro
     *
     * @param string $tipoMiembro
     *
     * @return Empresas
     */
    public function setTipoMiembro($tipoMiembro)
    {
        $this->tipoMiembro = $tipoMiembro;

        return $this;
    }

    /**
     * Get tipoMiembro
     *
     * @return string
     */
    public function getTipoMiembro()
    {
        return $this->tipoMiembro;
    }

    /**
     * Set empresaAdherente
     *
     * @param boolean $empresaAdherente
     *
     * @return Empresas
     */
    public function setEmpresaAdherente($empresaAdherente)
    {
        $this->empresaAdherente = $empresaAdherente;

        return $this;
    }

    /**
     * Get empresaAdherente
     *
     * @return boolean
     */
    public function getEmpresaAdherente()
    {
        return $this->empresaAdherente;
    }

    /**
     * Set nit
     *
     * @param string $nit
     *
     * @return Empresas
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string
     */
    public function getNit()
    {
        return $this->nit;
    }


    /**
     * Add idCapital
     *
     * @param \EntitiesBundle\Entity\Capital $idCapital
     *
     * @return Empresas
     */
    public function addIdCapital(\EntitiesBundle\Entity\Capital $idCapital)
    {
        $this->idCapital[] = $idCapital;

        return $this;
    }

    /**
     * Remove idCapital
     *
     * @param \EntitiesBundle\Entity\Capital $idCapital
     */
    public function removeIdCapital(\EntitiesBundle\Entity\Capital $idCapital)
    {
        $this->idCapital->removeElement($idCapital);
    }

    /**
     * Get idCapital
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdCapital()
    {
        return $this->idCapital;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idProductosEmpresa;


    /**
     * Add idProductosEmpresa
     *
     * @param \EntitiesBundle\Entity\ProductosEmpresa $idProductosEmpresa
     *
     * @return Empresas
     */
    public function addIdProductosEmpresa(\EntitiesBundle\Entity\ProductosEmpresa $idProductosEmpresa)
    {
        $this->idProductosEmpresa[] = $idProductosEmpresa;

        return $this;
    }

    /**
     * Remove idProductosEmpresa
     *
     * @param \EntitiesBundle\Entity\ProductosEmpresa $idProductosEmpresa
     */
    public function removeIdProductosEmpresa(\EntitiesBundle\Entity\ProductosEmpresa $idProductosEmpresa)
    {
        $this->idProductosEmpresa->removeElement($idProductosEmpresa);
    }

    /**
     * Get idProductosEmpresa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdProductosEmpresa()
    {
        return $this->idProductosEmpresa;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idRepresentante;


    /**
     * Add idRepresentante
     *
     * @param \EntitiesBundle\Entity\Representantes $idRepresentante
     *
     * @return Empresas
     */
    public function addIdRepresentante(\EntitiesBundle\Entity\Representantes $idRepresentante)
    {
        $this->idRepresentante[] = $idRepresentante;

        return $this;
    }

    /**
     * Remove idRepresentante
     *
     * @param \EntitiesBundle\Entity\Representantes $idRepresentante
     */
    public function removeIdRepresentante(\EntitiesBundle\Entity\Representantes $idRepresentante)
    {
        $this->idRepresentante->removeElement($idRepresentante);
    }

    /**
     * Get idRepresentante
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdRepresentante()
    {
        return $this->idRepresentante;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idExportacion;


    /**
     * Add idExportacion
     *
     * @param \EntitiesBundle\Entity\Exportaciones $idExportacion
     *
     * @return Empresas
     */
    public function addIdExportacion(\EntitiesBundle\Entity\Exportaciones $idExportacion)
    {
        $this->idExportacion[] = $idExportacion;

        return $this;
    }

    /**
     * Remove idExportacion
     *
     * @param \EntitiesBundle\Entity\Exportaciones $idExportacion
     */
    public function removeIdExportacion(\EntitiesBundle\Entity\Exportaciones $idExportacion)
    {
        $this->idExportacion->removeElement($idExportacion);
    }

    /**
     * Get idExportacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdExportacion()
    {
        return $this->idExportacion;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idArchivo;


    /**
     * Add idArchivo
     *
     * @param \EntitiesBundle\Entity\Archivos $idArchivo
     *
     * @return Empresas
     */
    public function addIdArchivo(\EntitiesBundle\Entity\Archivos $idArchivo)
    {
        $this->idArchivo[] = $idArchivo;

        return $this;
    }

    /**
     * Remove idArchivo
     *
     * @param \EntitiesBundle\Entity\Archivos $idArchivo
     */
    public function removeIdArchivo(\EntitiesBundle\Entity\Archivos $idArchivo)
    {
        $this->idArchivo->removeElement($idArchivo);
    }

    /**
     * Get idArchivo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdArchivo()
    {
        return $this->idArchivo;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idProductoControlado;


    /**
     * Add idProductoControlado
     *
     * @param \EntitiesBundle\Entity\ProductosControlados $idProductoControlado
     *
     * @return Empresas
     */
    public function addIdProductoControlado(\EntitiesBundle\Entity\ProductosControlados $idProductoControlado)
    {
        $this->idProductoControlado[] = $idProductoControlado;

        return $this;
    }

    /**
     * Remove idProductoControlado
     *
     * @param \EntitiesBundle\Entity\ProductosControlados $idProductoControlado
     */
    public function removeIdProductoControlado(\EntitiesBundle\Entity\ProductosControlados $idProductoControlado)
    {
        $this->idProductoControlado->removeElement($idProductoControlado);
    }

    /**
     * Get idProductoControlado
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdProductoControlado()
    {
        return $this->idProductoControlado;
    }
    /**
     * @var string
     */
    private $personaReferencia1;

    /**
     * @var string
     */
    private $empresaReferencia1;

    /**
     * @var string
     */
    private $personaReferencia2;

    /**
     * @var string
     */
    private $empresaReferencia2;


    /**
     * Set personaReferencia1
     *
     * @param string $personaReferencia1
     *
     * @return Empresas
     */
    public function setPersonaReferencia1($personaReferencia1)
    {
        $this->personaReferencia1 = $personaReferencia1;

        return $this;
    }

    /**
     * Get personaReferencia1
     *
     * @return string
     */
    public function getPersonaReferencia1()
    {
        return $this->personaReferencia1;
    }

    /**
     * Set empresaReferencia1
     *
     * @param string $empresaReferencia1
     *
     * @return Empresas
     */
    public function setEmpresaReferencia1($empresaReferencia1)
    {
        $this->empresaReferencia1 = $empresaReferencia1;

        return $this;
    }

    /**
     * Get empresaReferencia1
     *
     * @return string
     */
    public function getEmpresaReferencia1()
    {
        return $this->empresaReferencia1;
    }

    /**
     * Set personaReferencia2
     *
     * @param string $personaReferencia2
     *
     * @return Empresas
     */
    public function setPersonaReferencia2($personaReferencia2)
    {
        $this->personaReferencia2 = $personaReferencia2;

        return $this;
    }

    /**
     * Get personaReferencia2
     *
     * @return string
     */
    public function getPersonaReferencia2()
    {
        return $this->personaReferencia2;
    }

    /**
     * Set empresaReferencia2
     *
     * @param string $empresaReferencia2
     *
     * @return Empresas
     */
    public function setEmpresaReferencia2($empresaReferencia2)
    {
        $this->empresaReferencia2 = $empresaReferencia2;

        return $this;
    }

    /**
     * Get empresaReferencia2
     *
     * @return string
     */
    public function getEmpresaReferencia2()
    {
        return $this->empresaReferencia2;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idSector;


    /**
     * Add idSector
     *
     * @param \EntitiesBundle\Entity\Sector $idSector
     *
     * @return Empresas
     */
    public function addIdSector(\EntitiesBundle\Entity\Sector $idSector)
    {
        $this->idSector[] = $idSector;

        return $this;
    }

    /**
     * Remove idSector
     *
     * @param \EntitiesBundle\Entity\Sector $idSector
     */
    public function removeIdSector(\EntitiesBundle\Entity\Sector $idSector)
    {
        $this->idSector->removeElement($idSector);
    }

    /**
     * Get idSector
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdSector()
    {
        return $this->idSector;
    }



    /**
     * Set idSector
     *
     * @param \EntitiesBundle\Entity\Sector $idSector
     *
     * @return Empresas
     */
    public function setIdSector(\EntitiesBundle\Entity\Sector $idSector = null)
    {
        $this->idSector = $idSector;

        return $this;
    }
    /**
     * @var string
     */
    private $fechaCobranza;


    /**
     * Set fechaCobranza
     *
     * @param string $fechaCobranza
     *
     * @return Empresas
     */
    public function setFechaCobranza($fechaCobranza)
    {
        $this->fechaCobranza = $fechaCobranza;

        return $this;
    }

    /**
     * Get fechaCobranza
     *
     * @return string
     */
    public function getFechaCobranza()
    {
        return $this->fechaCobranza;
    }
}
