<?php

namespace EntitiesBundle\Entity;

/**
 * Archivos
 */
class Archivos
{
    /**
     * @var integer
     */
    private $idArchivo;

    /**
     * @var string
     */
    private $registroEmpresa;

    /**
     * @var string
     */
    private $rifEmpresa;

    /**
     * @var string
     */
    private $nitEmpresa;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idIslr;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idIslr = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idIva = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idArchivo
     *
     * @return integer
     */
    public function getIdArchivo()
    {
        return $this->idArchivo;
    }

    /**
     * Set registroEmpresa
     *
     * @param string $registroEmpresa
     *
     * @return Archivos
     */
    public function setRegistroEmpresa($registroEmpresa)
    {
        $this->registroEmpresa = $registroEmpresa;

        return $this;
    }

    /**
     * Get registroEmpresa
     *
     * @return string
     */
    public function getRegistroEmpresa()
    {
        return $this->registroEmpresa;
    }

    /**
     * Set rifEmpresa
     *
     * @param string $rifEmpresa
     *
     * @return Archivos
     */
    public function setRifEmpresa($rifEmpresa)
    {
        $this->rifEmpresa = $rifEmpresa;

        return $this;
    }

    /**
     * Get rifEmpresa
     *
     * @return string
     */
    public function getRifEmpresa()
    {
        return $this->rifEmpresa;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Archivos
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    public function __toString()
    {
        return $this->getRifEmpresa();
    }


    /**
     * Set nitEmpresa
     *
     * @param string $nitEmpresa
     *
     * @return Archivos
     */
    public function setNitEmpresa($nitEmpresa)
    {
        $this->nitEmpresa = $nitEmpresa;

        return $this;
    }

    /**
     * Get nitEmpresa
     *
     * @return string
     */
    public function getNitEmpresa()
    {
        return $this->nitEmpresa;
    }

    /**
     * Add idIslr
     *
     * @param \EntitiesBundle\Entity\Islr $idIslr
     *
     * @return Archivos
     */
    public function addIdIslr(\EntitiesBundle\Entity\Islr $idIslr)
    {
        $this->idIslr[] = $idIslr;

        return $this;
    }

    /**
     * Remove idIslr
     *
     * @param \EntitiesBundle\Entity\Islr $idIslr
     */
    public function removeIdIslr(\EntitiesBundle\Entity\Islr $idIslr)
    {
        $this->idIslr->removeElement($idIslr);
    }

    /**
     * Get idIslr
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdIslr()
    {
        return $this->idIslr;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idIva;


    /**
     * Add idIva
     *
     * @param \EntitiesBundle\Entity\Iva $idIva
     *
     * @return Archivos
     */
    public function addIdIva(\EntitiesBundle\Entity\Iva $idIva)
    {
        $this->idIva[] = $idIva;

        return $this;
    }

    /**
     * Remove idIva
     *
     * @param \EntitiesBundle\Entity\Iva $idIva
     */
    public function removeIdIva(\EntitiesBundle\Entity\Iva $idIva)
    {
        $this->idIva->removeElement($idIva);
    }

    /**
     * Get idIva
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdIva()
    {
        return $this->idIva;
    }
}
