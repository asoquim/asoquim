<?php

namespace EntitiesBundle\Entity;

/**
 * Templates
 */
class Templates
{
    /**
     * @var int
     */
    private $idTemplate;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var string
     */
    private $header;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $footer;


    /**
     * Get idTemplate
     *
     * @return int
     */
    public function getIdTemplate()
    {
        return $this->idTemplate;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     *
     * @return Templates
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set header
     *
     * @param string $header
     *
     * @return Templates
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Templates
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set footer
     *
     * @param string $footer
     *
     * @return Templates
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * Get footer
     *
     * @return string
     */
    public function getFooter()
    {
        return $this->footer;
    }
    /**
     * @var \EntitiesBundle\Entity\CategoriaComunicado
     */
    private $idCategoria;


    /**
     * Set idCategoria
     *
     * @param \EntitiesBundle\Entity\CategoriaComunicado $idCategoria
     *
     * @return Templates
     */
    public function setIdCategoria(\EntitiesBundle\Entity\CategoriaComunicado $idCategoria = null)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    /**
     * Get idCategoria
     *
     * @return \EntitiesBundle\Entity\CategoriaComunicado
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getContent();
    }
}
