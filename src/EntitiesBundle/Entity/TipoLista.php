<?php

namespace EntitiesBundle\Entity;

/**
 * TipoLista
 */
class TipoLista
{
    /**
     * @var integer
     */
    private $idTipo;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Set idTipo
     *
     * @param integer $idTipo
     *
     * @return TipoLista
     */
    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    /**
     * Get idTipo
     *
     * @return integer
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoLista
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getDescripcion();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idLista;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idLista = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idListum
     *
     * @param \EntitiesBundle\Entity\Lista $idListum
     *
     * @return TipoLista
     */
    public function addIdListum(\EntitiesBundle\Entity\Lista $idListum)
    {
        $this->idLista[] = $idListum;

        return $this;
    }

    /**
     * Remove idListum
     *
     * @param \EntitiesBundle\Entity\Lista $idListum
     */
    public function removeIdListum(\EntitiesBundle\Entity\Lista $idListum)
    {
        $this->idLista->removeElement($idListum);
    }

    /**
     * Get idLista
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdLista()
    {
        return $this->idLista;
    }
}
