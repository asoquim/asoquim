<?php

namespace EntitiesBundle\Entity;

/**
 * DocumentosActvidades
 */
class DocumentosActvidades
{
    /**
     * @var integer
     */
    private $idDocumento;

    /**
     * @var string
     */
    private $documento;

    /**
     * @var \EntitiesBundle\Entity\Actividades
     */
    private $idActividad;


    /**
     * Set idDocumento
     *
     * @param integer $idDocumento
     *
     * @return DocumentosActvidades
     */
    public function setIdDocumento($idDocumento)
    {
        $this->idDocumento = $idDocumento;

        return $this;
    }

    /**
     * Get idDocumento
     *
     * @return integer
     */
    public function getIdDocumento()
    {
        return $this->idDocumento;
    }

    /**
     * Set documento
     *
     * @param string $documento
     *
     * @return DocumentosActvidades
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set idActividad
     *
     * @param \EntitiesBundle\Entity\Actividades $idActividad
     *
     * @return DocumentosActvidades
     */
    public function setIdActividad(\EntitiesBundle\Entity\Actividades $idActividad = null)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return \EntitiesBundle\Entity\Actividades
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }
}
