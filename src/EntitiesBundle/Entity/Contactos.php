<?php

namespace EntitiesBundle\Entity;

/**
 * Contactos
 */
class Contactos
{
    /**
     * @var integer
     */
    private $idContacto;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $telefonoOficina;

    /**
     * @var integer
     */
    private $telefonoCelular;

    /**
     * @var string
     */
    private $cargo;

    /**
     * @var string
     */
    private $correo;

    /**
     * @var boolean
     */
    private $representante;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Get idContacto
     *
     * @return integer
     */
    public function getIdContacto()
    {
        return $this->idContacto;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Contactos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return Contactos
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Contactos
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Contactos
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Contactos
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set telefonoOficina
     *
     * @param string $telefonoOficina
     *
     * @return Contactos
     */
    public function setTelefonoOficina($telefonoOficina)
    {
        $this->telefonoOficina = $telefonoOficina;

        return $this;
    }

    /**
     * Get telefonoOficina
     *
     * @return string
     */
    public function getTelefonoOficina()
    {
        return $this->telefonoOficina;
    }

    /**
     * Set telefonoCelular
     *
     * @param string $telefonoCelular
     *
     * @return Contactos
     */
    public function setTelefonoCelular($telefonoCelular)
    {
        $this->telefonoCelular = $telefonoCelular;

        return $this;
    }

    /**
     * Get telefonoCelular
     *
     * @return string
     */
    public function getTelefonoCelular()
    {
        return $this->telefonoCelular;
    }

    /**
     * Set representante
     *
     * @param boolean $representante
     *
     * @return Contactos
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;

        return $this;
    }

    /**
     * Get representante
     *
     * @return boolean
     */
    public function getRepresentante()
    {
        return $this->representante;
    }
    /**
     * @var \EntitiesBundle\Entity\Region
     */
    private $id;


    /**
     * Set id
     *
     * @param \EntitiesBundle\Entity\Region $id
     *
     * @return Contactos
     */
    public function setId(\EntitiesBundle\Entity\Region $id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \EntitiesBundle\Entity\Region
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}
