<?php

namespace EntitiesBundle\Entity;

/**
 * TipoUsuarios
 */
class TipoUsuarios
{
    /**
     * @var integer
     */
    private $idTipo;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Usuarios
     */
    private $idUsuario;


    /**
     * Get idTipo
     *
     * @return integer
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoUsuarios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idUsuario
     *
     * @param \EntitiesBundle\Entity\Usuarios $idUsuario
     *
     * @return TipoUsuarios
     */
    public function setIdUsuario(\EntitiesBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \EntitiesBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
