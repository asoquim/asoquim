<?php

namespace EntitiesBundle\Entity;

/**
 * Comunicado
 */
class Comunicado
{

    /**
     * @var integer
     */
    private $idComunicado;

    /**
     * @var integer
     */
    private $email_id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \DateTime
     */
    private $fechaEnvio;

    /**
     * @var string
     */
    private $comunicado;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \EntitiesBundle\Entity\CategoriaComunicado
     */
    private $idCategoria;

    /**
     * @var \EntitiesBundle\Entity\Lista
     */
    private $idLista;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idComunicado
     *
     * @return integer
     */
    public function getIdComunicado()
    {
        return $this->idComunicado;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return Comunicado
     */
    public function setEmailId($emailId)
    {
        $this->email_id = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->email_id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Comunicado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaEnvio
     *
     * @param \DateTime $fechaEnvio
     *
     * @return Comunicado
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    /**
     * Get fechaEnvio
     *
     * @return \DateTime
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }

    /**
     * Set comunicado
     *
     * @param string $comunicado
     *
     * @return Comunicado
     */
    public function setComunicado($comunicado)
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    /**
     * Get comunicado
     *
     * @return string
     */
    public function getComunicado()
    {
        return $this->comunicado;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Comunicado
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idCategoria
     *
     * @param \EntitiesBundle\Entity\CategoriaComunicado $idCategoria
     *
     * @return Comunicado
     */
    public function setIdCategoria(\EntitiesBundle\Entity\CategoriaComunicado $idCategoria = null)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    /**
     * Get idCategoria
     *
     * @return \EntitiesBundle\Entity\CategoriaComunicado
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * Set idLista
     *
     * @param \EntitiesBundle\Entity\Lista $idLista
     *
     * @return Comunicado
     */
    public function setIdLista(\EntitiesBundle\Entity\Lista $idLista = null)
    {
        $this->idLista = $idLista;

        return $this;
    }

    /**
     * Get idLista
     *
     * @return \EntitiesBundle\Entity\Lista
     */
    public function getIdLista()
    {
        return $this->idLista;
    }

    /**
     * Add id
     *
     * @param \EntitiesBundle\Entity\Usuarios $id
     *
     * @return Comunicado
     */
    public function addId(\EntitiesBundle\Entity\Usuarios $id)
    {
        $this->id[] = $id;

        return $this;
    }

    /**
     * Remove id
     *
     * @param \EntitiesBundle\Entity\Usuarios $id
     */
    public function removeId(\EntitiesBundle\Entity\Usuarios $id)
    {
        $this->id->removeElement($id);
    }

    public function countId()
    {
        $id = count($this->id);
        return $id;
    }

    /**
     * Get id
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $listas;


    /**
     * Add lista
     *
     * @param \EntitiesBundle\Entity\Lista $lista
     *
     * @return Comunicado
     */
    public function addLista(\EntitiesBundle\Entity\Lista $lista)
    {
        $this->listas[] = $lista;

        return $this;
    }

    /**
     * Remove lista
     *
     * @param \EntitiesBundle\Entity\Lista $lista
     */
    public function removeLista(\EntitiesBundle\Entity\Lista $lista)
    {
        $this->listas->removeElement($lista);
    }

    /**
     * Get listas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListas()
    {
        return $this->listas;
    }
    /**
     * @var integer
     */
    private $aprobacion_id;


    /**
     * Set aprobacionId
     *
     * @param integer $aprobacionId
     *
     * @return Comunicado
     */
    public function setAprobacionId($aprobacionId)
    {
        $this->aprobacion_id = $aprobacionId;

        return $this;
    }

    /**
     * Get aprobacionId
     *
     * @return integer
     */
    public function getAprobacionId()
    {
        return $this->aprobacion_id;
    }
}
