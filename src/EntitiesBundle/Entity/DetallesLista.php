<?php

namespace EntitiesBundle\Entity;

/**
 * DetallesLista
 */
class DetallesLista
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \EntitiesBundle\Entity\Lista
     */
    private $idLista;


    /**
     * Set idLista
     *
     * @param \EntitiesBundle\Entity\Lista $idLista
     *
     * @return DetallesLista
     */
    public function setIdLista(\EntitiesBundle\Entity\Lista $idLista = null)
    {
        $this->idLista = $idLista;

        return $this;
    }

    /**
     * Get idLista
     *
     * @return \EntitiesBundle\Entity\Lista
     */
    public function getIdLista()
    {
        return $this->idLista;
    }
    /**
     * @var \EntitiesBundle\Entity\Contactos
     */
    private $idContacto;


    /**
     * Set idContacto
     *
     * @param \EntitiesBundle\Entity\Contactos $idContacto
     *
     * @return DetallesLista
     */
    public function setIdContacto(\EntitiesBundle\Entity\Contactos $idContacto = null)
    {
        $this->idContacto = $idContacto;

        return $this;
    }

    /**
     * Get idContacto
     *
     * @return \EntitiesBundle\Entity\Contactos
     */
    public function getIdContacto()
    {
        return $this->idContacto;
    }
    /**
     * @var \EntitiesBundle\Entity\Usuarios
     */
    private $idUsuario;


    /**
     * Set idUsuario
     *
     * @param \EntitiesBundle\Entity\Usuarios $idUsuario
     *
     * @return DetallesLista
     */
    public function setIdUsuario(\EntitiesBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \EntitiesBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
