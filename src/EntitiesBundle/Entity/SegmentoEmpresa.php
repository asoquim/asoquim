<?php

namespace EntitiesBundle\Entity;

/**
 * SegmentoEmpresa
 */
class SegmentoEmpresa
{
    /**
     * @var integer
     */
    private $idSegmento;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get idSegmento
     *
     * @return integer
     */
    public function getIdSegmento()
    {
        return $this->idSegmento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return SegmentoEmpresa
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
}
