<?php

namespace EntitiesBundle\Entity;

/**
 * Actividades
 */
class Actividades
{
    /**
     * @var integer
     */
    private $idActividad;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \Date
     */
    private $fechaInicio;

    /**
     * @var \Date
     */
    private $fechaCierre;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \EntitiesBundle\Entity\Usuarios
     */
    private $idUsuario;

    /**
     * @var \EntitiesBundle\Entity\Casos
     */
    private $codigo;


    /**
     * Set idActividad
     *
     * @param integer $idActividad
     *
     * @return Actividades
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return integer
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Actividades
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaInicio
     *
     * @param \Date $fechaInicio
     *
     * @return Actividades
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \Date
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaCierre
     *
     * @param \Date $fechaCierre
     *
     * @return Actividades
     */
    public function setFechaCierre($fechaCierre)
    {
        $this->fechaCierre = $fechaCierre;

        return $this;
    }

    /**
     * Get fechaCierre
     *
     * @return \Date
     */
    public function getFechaCierre()
    {
        return $this->fechaCierre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Actividades
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Actividades
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idUsuario
     *
     * @param \EntitiesBundle\Entity\Usuarios $idUsuario
     *
     * @return Actividades
     */
    public function setIdUsuario(\EntitiesBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \EntitiesBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set codigo
     *
     * @param \EntitiesBundle\Entity\Casos $codigo
     *
     * @return Actividades
     */
    public function setCodigo(\EntitiesBundle\Entity\Casos $codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return \EntitiesBundle\Entity\Casos
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}
