<?php

namespace EntitiesBundle\Entity;

/**
 * CategoriaComunicado
 */
class CategoriaComunicado
{
    /**
     * @var int
     */
    private $idCategoria;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get idCategoria
     *
     * @return int
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return CategoriaComunicado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @var \EntitiesBundle\Entity\Comunicado
     */
    private $idComunicado;


    /**
     * Set idComunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $idComunicado
     *
     * @return CategoriaComunicado
     */
    public function setIdComunicado(\EntitiesBundle\Entity\Comunicado $idComunicado = null)
    {
        $this->idComunicado = $idComunicado;

        return $this;
    }

    /**
     * Get idComunicado
     *
     * @return \EntitiesBundle\Entity\Comunicado
     */
    public function getIdComunicado()
    {
        return $this->idComunicado;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getDescripcion();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idComunicado = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idComunicado
     *
     * @param \EntitiesBundle\Entity\Templates $idComunicado
     *
     * @return CategoriaComunicado
     */
    public function addIdComunicado(\EntitiesBundle\Entity\Templates $idComunicado)
    {
        $this->idComunicado[] = $idComunicado;

        return $this;
    }

    /**
     * Remove idComunicado
     *
     * @param \EntitiesBundle\Entity\Templates $idComunicado
     */
    public function removeIdComunicado(\EntitiesBundle\Entity\Templates $idComunicado)
    {
        $this->idComunicado->removeElement($idComunicado);
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idTemplate;


    /**
     * Add idTemplate
     *
     * @param \EntitiesBundle\Entity\Templates $idTemplate
     *
     * @return CategoriaComunicado
     */
    public function addIdTemplate(\EntitiesBundle\Entity\Templates $idTemplate)
    {
        $this->idTemplate[] = $idTemplate;

        return $this;
    }

    /**
     * Remove idTemplate
     *
     * @param \EntitiesBundle\Entity\Templates $idTemplate
     */
    public function removeIdTemplate(\EntitiesBundle\Entity\Templates $idTemplate)
    {
        $this->idTemplate->removeElement($idTemplate);
    }

    /**
     * Get idTemplate
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdTemplate()
    {
        return $this->idTemplate;
    }
}
