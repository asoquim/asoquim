<?php

namespace EntitiesBundle\Entity;

/**
 * Islr
 */
class Islr
{
    /**
     * @var integer
     */
    private $idIslr;

    /**
     * @var string
     */
    private $islr;

    /**
     * @var \EntitiesBundle\Entity\Archivos
     */
    private $idArchivo;


    /**
     * Get idIslr
     *
     * @return integer
     */
    public function getIdIslr()
    {
        return $this->idIslr;
    }

    /**
     * Set islr
     *
     * @param string $islr
     *
     * @return Islr
     */
    public function setIslr($islr)
    {
        $this->islr = $islr;

        return $this;
    }

    /**
     * Get islr
     *
     * @return string
     */
    public function getIslr()
    {
        return $this->islr;
    }

    /**
     * Set idArchivo
     *
     * @param \EntitiesBundle\Entity\Archivos $idArchivo
     *
     * @return Islr
     */
    public function setIdArchivo(\EntitiesBundle\Entity\Archivos $idArchivo = null)
    {
        $this->idArchivo = $idArchivo;

        return $this;
    }

    /**
     * Get idArchivo
     *
     * @return \EntitiesBundle\Entity\Archivos
     */
    public function getIdArchivo()
    {
        return $this->idArchivo;
    }
}
