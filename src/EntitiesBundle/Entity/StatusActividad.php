<?php

namespace EntitiesBundle\Entity;

/**
 * StatusActividad
 */
class StatusActividad
{
    /**
     * @var integer
     */
    private $idStatus;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Actividades
     */
    private $idActividad;


    /**
     * Set idStatus
     *
     * @param integer $idStatus
     *
     * @return StatusActividad
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return StatusActividad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idActividad
     *
     * @param \EntitiesBundle\Entity\Actividades $idActividad
     *
     * @return StatusActividad
     */
    public function setIdActividad(\EntitiesBundle\Entity\Actividades $idActividad = null)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return \EntitiesBundle\Entity\Actividades
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }
}
