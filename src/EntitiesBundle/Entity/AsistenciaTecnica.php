<?php

namespace EntitiesBundle\Entity;

/**
 * AsistenciaTecnica
 */
class AsistenciaTecnica
{
    /**
     * @var integer
     */
    private $idAsistenciaTecnica;

    /**
     * @var string
     */
    private $empresaOrigen;

    /**
     * @var string
     */
    private $servicios;

    /**
     * @var string
     */
    private $responsable;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \EntitiesBundle\Entity\Paises
     */
    private $idPais;

    /**
     * @var string
     */
    private $productosComercializa;

    /**
     * @var string
     */
    private $materiaPrima;


    /**
     * Set idAsistenciaTecnica
     *
     * @param integer $idAsistenciaTecnica
     *
     * @return AsistenciaTecnica
     */
    public function setIdAsistenciaTecnica($idAsistenciaTecnica)
    {
        $this->idAsistenciaTecnica = $idAsistenciaTecnica;

        return $this;
    }

    /**
     * Get idAsistenciaTecnica
     *
     * @return integer
     */
    public function getIdAsistenciaTecnica()
    {
        return $this->idAsistenciaTecnica;
    }

    /**
     * Set empresaOrigen
     *
     * @param string $empresaOrigen
     *
     * @return AsistenciaTecnica
     */
    public function setEmpresaOrigen($empresaOrigen)
    {
        $this->empresaOrigen = $empresaOrigen;

        return $this;
    }

    /**
     * Get empresaOrigen
     *
     * @return string
     */
    public function getEmpresaOrigen()
    {
        return $this->empresaOrigen;
    }

    /**
     * Set servicios
     *
     * @param string $servicios
     *
     * @return AsistenciaTecnica
     */
    public function setServicios($servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * Get servicios
     *
     * @return string
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return AsistenciaTecnica
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return AsistenciaTecnica
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set idPais
     *
     * @param \EntitiesBundle\Entity\Paises $idPais
     *
     * @return AsistenciaTecnica
     */
    public function setIdPais(\EntitiesBundle\Entity\Paises $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \EntitiesBundle\Entity\Paises
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set productosComercializa
     *
     * @param string $productosComercializa
     *
     * @return AsistenciaTecnica
     */
    public function setProductosComercializa($productosComercializa)
    {
        $this->productosComercializa = $productosComercializa;

        return $this;
    }

    /**
     * Get productosComercializa
     *
     * @return string
     */
    public function getProductosComercializa()
    {
        return $this->productosComercializa;
    }

    /**
     * Set materiaPrima
     *
     * @param string $materiaPrima
     *
     * @return AsistenciaTecnica
     */
    public function setMateriaPrima($materiaPrima)
    {
        $this->materiaPrima = $materiaPrima;

        return $this;
    }

    /**
     * Get materiaPrima
     *
     * @return string
     */
    public function getMateriaPrima()
    {
        return $this->materiaPrima;
    }
}
