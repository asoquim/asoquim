<?php

namespace EntitiesBundle\Entity;

/**
 * Iva
 */
class Iva
{
    /**
     * @var integer
     */
    private $idIva;

    /**
     * @var string
     */
    private $iva;

    /**
     * @var \EntitiesBundle\Entity\Archivos
     */
    private $idArchivo;


    /**
     * Get idIva
     *
     * @return integer
     */
    public function getIdIva()
    {
        return $this->idIva;
    }

    /**
     * Set iva
     *
     * @param string $iva
     *
     * @return Iva
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return string
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set idArchivo
     *
     * @param \EntitiesBundle\Entity\Archivos $idArchivo
     *
     * @return Iva
     */
    public function setIdArchivo(\EntitiesBundle\Entity\Archivos $idArchivo = null)
    {
        $this->idArchivo = $idArchivo;

        return $this;
    }

    /**
     * Get idArchivo
     *
     * @return \EntitiesBundle\Entity\Archivos
     */
    public function getIdArchivo()
    {
        return $this->idArchivo;
    }
}
