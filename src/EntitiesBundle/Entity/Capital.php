<?php

namespace EntitiesBundle\Entity;

/**
 * Capital
 */
class Capital
{
    /**
     * @var integer
     */
    private $idCapital;

    /**
     * @var integer
     */
    private $porentajeCapitalPrivado;

    /**
     * @var integer
     */
    private $porcentajeCapitalExtranjero;

    /**
     * @var integer
     */
    private $porcentajeCapitalPublic; 

    /**
     * @var string
     */
    private $principalAccionista;

    /**
     * @var string
     */
    private $nombreCasaMatriz;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \EntitiesBundle\Entity\Paises
     */
    private $idPais;

    /**
     * Get idCapital
     *
     * @return integer
     */
    public function getIdCapital()
    {
        return $this->idCapital;
    }

    /**
     * Set porentajeCapitalPrivado
     *
     * @param integer $porentajeCapitalPrivado
     *
     * @return Capital
     */
    public function setPorentajeCapitalPrivado($porentajeCapitalPrivado)
    {
        $this->porentajeCapitalPrivado = $porentajeCapitalPrivado;

        return $this;
    }

    /**
     * Get porentajeCapitalPrivado
     *
     * @return integer
     */
    public function getPorentajeCapitalPrivado()
    {
        return $this->porentajeCapitalPrivado;
    }

    /**
     * Set porcentajeCapitalExtranjero
     *
     * @param integer $porcentajeCapitalExtranjero
     *
     * @return Capital
     */
    public function setPorcentajeCapitalExtranjero($porcentajeCapitalExtranjero)
    {
        $this->porcentajeCapitalExtranjero = $porcentajeCapitalExtranjero;

        return $this;
    }

    /**
     * Get porcentajeCapitalExtranjero
     *
     * @return integer
     */
    public function getPorcentajeCapitalExtranjero()
    {
        return $this->porcentajeCapitalExtranjero;
    }

    /**
     * Set porcentajeCapitalPublic
     *
     * @param integer $porcentajeCapitalPublic
     *
     * @return Capital
     */
    public function setPorcentajeCapitalPublic($porcentajeCapitalPublic)
    {
        $this->porcentajeCapitalPublic = $porcentajeCapitalPublic;

        return $this;
    }

    /**
     * Get porcentajeCapitalPublic
     *
     * @return integer
     */
    public function getPorcentajeCapitalPublic()
    {
        return $this->porcentajeCapitalPublic;
    }

    /**
     * Set principalAccionista
     *
     * @param string $principalAccionista
     *
     * @return Capital
     */
    public function setPrincipalAccionista($principalAccionista)
    {
        $this->principalAccionista = $principalAccionista;

        return $this;
    }

    /**
     * Get principalAccionista
     *
     * @return string
     */
    public function getPrincipalAccionista()
    {
        return $this->principalAccionista;
    }

    /**
     * Set nombreCasaMatriz
     *
     * @param string $nombreCasaMatriz
     *
     * @return Capital
     */
    public function setNombreCasaMatriz($nombreCasaMatriz)
    {
        $this->nombreCasaMatriz = $nombreCasaMatriz;

        return $this;
    }

    /**
     * Get nombreCasaMatriz
     *
     * @return string
     */
    public function getNombreCasaMatriz()
    {
        return $this->nombreCasaMatriz;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Capital
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set idPais
     *
     * @param \EntitiesBundle\Entity\Paises $idPais
     *
     * @return Capital
     */
    public function setIdPais(\EntitiesBundle\Entity\Paises $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \EntitiesBundle\Entity\Paises
     */
    public function getIdPais()
    {
        return $this->idPais;
    }
}
