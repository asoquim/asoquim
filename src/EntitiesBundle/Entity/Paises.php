<?php

namespace EntitiesBundle\Entity;

/**
 * Paises
 */
class Paises
{
    /**
     * @var integer
     */
    private $idPais;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \EntitiesBundle\Entity\AsistenciaTecnica
     */
    protected $idAsistenciaTecnica;


    /**
     * Get idPais
     *
     * @return integer
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Paises
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idAsistenciaTecnica
     *
     * @return \EntitiesBundle\Entity\AsistenciaTecnica
     */
    public function getIdAsistenciaTecnica()
    {
        return $this->idAsistenciaTecnica;
    }

    /**
     * Set idAsistenciaTecnica
     *
     * @param \EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica
     *
     * @return Paises
     */
    public function setIdAsistenciaTecnica(\EntitiesBundle\Entity\AsistenciaTecnica $idAsistenciaTecnica)
    {
        $this->idAsistenciaTecnica = $idAsistenciaTecnica;
        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}
