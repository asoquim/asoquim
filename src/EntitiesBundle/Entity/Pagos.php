<?php

namespace EntitiesBundle\Entity;

/**
 * Pagos
 */
class Pagos
{
    /**
     * @var integer
     */
    private $idPago;

    /**
     * @var \DateTime
     */
    private $fechaPago;

    /**
     * @var \DateTime
     */
    private $fechaCobranza;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var float
     */
    private $monto;

    /**
     * Set idPago
     *
     * @param integer $idPago
     *
     * @return Pagos
     */
    public function setIdPago($idPago)
    {
        $this->idPago = $idPago;

        return $this;
    }

    /**
     * Get idPago
     *
     * @return integer
     */
    public function getIdPago()
    {
        return $this->idPago;
    }

    /**
     * Set fechaPago
     *
     * @param \DateTime $fechaPago
     *
     * @return Pagos
     */
    public function setFechaPago($fechaPago)
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    /**
     * Get fechaPago
     *
     * @return \DateTime
     */
    public function getFechaPago()
    {
        return $this->fechaPago;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Pagos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Pagos
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * @param float $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }


    /**
     * Set fechaCobranza
     *
     * @param \DateTime $fechaCobranza
     *
     * @return Pagos
     */
    public function setFechaCobranza($fechaCobranza)
    {
        $this->fechaCobranza = $fechaCobranza;

        return $this;
    }

    /**
     * Get fechaCobranza
     *
     * @return \DateTime
     */
    public function getFechaCobranza()
    {
        return $this->fechaCobranza;
    }
}
