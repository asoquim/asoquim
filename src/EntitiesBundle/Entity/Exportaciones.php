<?php

namespace EntitiesBundle\Entity;

/**
 * Exportaciones
 */
class Exportaciones
{
    /**
     * @var integer
     */
    private $idExportacion;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \EntitiesBundle\Entity\Paises
     */
    private $idPais;


    /**
     * Set idExportacion
     *
     * @param integer $idExportacion
     *
     * @return Exportaciones
     */
    public function setIdExportacion($idExportacion)
    {
        $this->idExportacion = $idExportacion;

        return $this;
    }

    /**
     * Get idExportacion
     *
     * @return integer
     */
    public function getIdExportacion()
    {
        return $this->idExportacion;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Exportaciones
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set idPais
     *
     * @param \EntitiesBundle\Entity\Paises $idPais
     *
     * @return Exportaciones
     */
    public function setIdPais(\EntitiesBundle\Entity\Paises $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \EntitiesBundle\Entity\Paises
     */
    public function getIdPais()
    {
        return $this->idPais;
    }
}
