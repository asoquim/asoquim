<?php

namespace EntitiesBundle\Entity;

/**
 * Lista
 */
class Lista
{
    /**
     * @var integer
     */
    private $idLista;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $id;

    /**
     * @var \EntitiesBundle\Entity\TipoLista
     */
    private $idTipo;

    /**
     * @var integer
     */
    private $idGrupo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idLista
     *
     * @return integer
     */
    public function getIdLista()
    {
        return $this->idLista;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Lista
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add id
     *
     * @param \EntitiesBundle\Entity\DetallesLista $id
     *
     * @return Lista
     */
    public function addId(\EntitiesBundle\Entity\DetallesLista $id)
    {
        $this->id[] = $id;

        return $this;
    }

    /**
     * Remove id
     *
     * @param \EntitiesBundle\Entity\DetallesLista $id
     */
    public function removeId(\EntitiesBundle\Entity\DetallesLista $id)
    {
        $this->id->removeElement($id);
    }

    /**
     * Get id
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTipo
     *
     * @param \EntitiesBundle\Entity\TipoLista $idTipo
     *
     * @return Lista
     */
    public function setIdTipo(\EntitiesBundle\Entity\TipoLista $idTipo = null)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    /**
     * Get idTipo
     *
     * @return \EntitiesBundle\Entity\TipoLista
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * @return int
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * @param int $idGrupo
     */
    public function setIdGrupo($idGrupo)
    {
        $this->idGrupo = $idGrupo;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idComunicado;


    /**
     * Add idComunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $idComunicado
     *
     * @return Lista
     */
    public function addIdComunicado(\EntitiesBundle\Entity\Comunicado $idComunicado)
    {
        $this->idComunicado[] = $idComunicado;

        return $this;
    }

    /**
     * Remove idComunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $idComunicado
     */
    public function removeIdComunicado(\EntitiesBundle\Entity\Comunicado $idComunicado)
    {
        $this->idComunicado->removeElement($idComunicado);
    }

    /**
     * Get idComunicado
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdComunicado()
    {
        return $this->idComunicado;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getNombre();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comunicados;


    /**
     * Add comunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $comunicado
     *
     * @return Lista
     */
    public function addComunicado(\EntitiesBundle\Entity\Comunicado $comunicado)
    {
        $this->comunicados[] = $comunicado;

        return $this;
    }

    /**
     * Remove comunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $comunicado
     */
    public function removeComunicado(\EntitiesBundle\Entity\Comunicado $comunicado)
    {
        $this->comunicados->removeElement($comunicado);
    }

    /**
     * Get comunicados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComunicados()
    {
        return $this->comunicados;
    }
}
