<?php

namespace EntitiesBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * Usuarios
 */
class Usuarios extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    private $telefono;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $cargo;

    /**
     * @var string
     */
    private $foto = 'user.png';

    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @var integer
     */
    private $status = 1;

    private $tipoUsuario;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaModificacion;

    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     *
     * @return Usuarios
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Usuarios
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return Usuarios
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Usuarios
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Usuarios
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Usuarios
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    public function __toString()
    {
        return parent::__toString();
    }

    /**
     * Set tipoUsuario
     *
     * @param integer $tipoUsuario
     *
     * @return Usuarios
     */
    public function setTipoUsuario($tipoUsuario)
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get tipoUsuario
     *
     * @return integer
     */
    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }
    /**
     * @var string
     */
    private $manyToMany;


    /**
     * Set manyToMany
     *
     * @param string $manyToMany
     *
     * @return Usuarios
     */
    public function setManyToMany($manyToMany)
    {
        $this->manyToMany = $manyToMany;

        return $this;
    }

    /**
     * Get manyToMany
     *
     * @return string
     */
    public function getManyToMany()
    {
        return $this->manyToMany;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idComunicado;


    /**
     * Add idComunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $idComunicado
     *
     * @return Usuarios
     */
    public function addIdComunicado(\EntitiesBundle\Entity\Comunicado $idComunicado)
    {
        $this->idComunicado[] = $idComunicado;

        return $this;
    }

    /**
     * Remove idComunicado
     *
     * @param \EntitiesBundle\Entity\Comunicado $idComunicado
     */
    public function removeIdComunicado(\EntitiesBundle\Entity\Comunicado $idComunicado)
    {
        $this->idComunicado->removeElement($idComunicado);
    }

    /**
     * Get idComunicado
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdComunicado()
    {
        return $this->idComunicado;
    }
    /**
     * @var bool
     */
    /**
     * @var boolean
     */
    private $mailerContact;


    /**
     * Set mailerContact
     *
     * @param boolean $mailerContact
     *
     * @return Usuarios
     */
    public function setMailerContact($mailerContact)
    {
        $this->mailerContact = $mailerContact;

        return $this;
    }

    /**
     * Get mailerContact
     *
     * @return boolean
     */
    public function getMailerContact()
    {
        return $this->mailerContact;
    }
}
