<?php

namespace EntitiesBundle\Entity;

/**
 * Referencias
 */
class Referencias
{
    /**
     * @var integer
     */
    private $idReferencia;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;

    /**
     * @var \EntitiesBundle\Entity\Contactos
     */
    private $idContacto;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rifRegistrado;
    
    /**
     * Get idReferencia
     *
     * @return integer
     */
    public function getIdReferencia()
    {
        return $this->idReferencia;
    }


    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Referencias
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set idContacto
     *
     * @param \EntitiesBundle\Entity\Contactos $idContacto
     *
     * @return Referencias
     */
    public function setIdContacto(\EntitiesBundle\Entity\Contactos $idContacto = null)
    {
        $this->idContacto = $idContacto;

        return $this;
    }

    /**
     * Get idContacto
     *
     * @return \EntitiesBundle\Entity\Contactos
     */
    public function getIdContacto()
    {
        return $this->idContacto;
    }

    /**
     * Set rifRegistrado
     *
     * @param \EntitiesBundle\Entity\Empresas $rifRegistrado
     *
     * @return Referencias
     */
    public function setRifRegistrado(\EntitiesBundle\Entity\Empresas $rifRegistrado = null)
    {
        $this->rifRegistrado = $rifRegistrado;

        return $this;
    }

    /**
     * Get rifRegistrado
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRifRegistrado()
    {
        return $this->rifRegistrado;
    }

    public function unsetEmpresa()
    {
        if (null === $this->rif) {
            return;
        }

        unset($this->rif);
    }

    public function unsetContacto()
    {
        if (null === $this->idContacto) {
            return;
        }

        unset($this->idContacto);
    }
}
