<?php

namespace EntitiesBundle\Entity;

/**
 * ProductosEmpresa
 */
class ProductosEmpresa
{
    /**
     * @var integer
     */
    private $idProductosEmpresa;

    /**
     * @var string
     */
    private $productos;

    /**
     * @var integer
     */
    private $capacidadInstalada;

    /**
     * @var integer
     */
    private $porcentajeActividad;

    /**
     * @var string
     */
    private $materiaPrima;

    /**
     * @var string
     */
    private $marcaComercial;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Set idProductosEmpresa
     *
     * @param integer $idProductosEmpresa
     *
     * @return ProductosEmpresa
     */
    public function setIdProductosEmpresa($idProductosEmpresa)
    {
        $this->idProductosEmpresa = $idProductosEmpresa;

        return $this;
    }

    /**
     * Get idProductosEmpresa
     *
     * @return integer
     */
    public function getIdProductosEmpresa()
    {
        return $this->idProductosEmpresa;
    }

    /**
     * Set productos
     *
     * @param string $productos
     *
     * @return ProductosEmpresa
     */
    public function setProductos($productos)
    {
        $this->productos = $productos;

        return $this;
    }

    /**
     * Get productos
     *
     * @return string
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Set capacidadInstalada
     *
     * @param integer $capacidadInstalada
     *
     * @return ProductosEmpresa
     */
    public function setCapacidadInstalada($capacidadInstalada)
    {
        $this->capacidadInstalada = $capacidadInstalada;

        return $this;
    }

    /**
     * Get capacidadInstalada
     *
     * @return integer
     */
    public function getCapacidadInstalada()
    {
        return $this->capacidadInstalada;
    }

    /**
     * Set porcentajeActividad
     *
     * @param integer $porcentajeActividad
     *
     * @return ProductosEmpresa
     */
    public function setPorcentajeActividad($porcentajeActividad)
    {
        $this->porcentajeActividad = $porcentajeActividad;

        return $this;
    }

    /**
     * Get porcentajeActividad
     *
     * @return integer
     */
    public function getPorcentajeActividad()
    {
        return $this->porcentajeActividad;
    }

    /**
     * Set materiaPrima
     *
     * @param string $materiaPrima
     *
     * @return ProductosEmpresa
     */
    public function setMateriaPrima($materiaPrima)
    {
        $this->materiaPrima = $materiaPrima;

        return $this;
    }

    /**
     * Get materiaPrima
     *
     * @return string
     */
    public function getMateriaPrima()
    {
        return $this->materiaPrima;
    }

    /**
     * Set marcaComercial
     *
     * @param string $marcaComercial
     *
     * @return ProductosEmpresa
     */
    public function setMarcaComercial($marcaComercial)
    {
        $this->marcaComercial = $marcaComercial;

        return $this;
    }

    /**
     * Get marcaComercial
     *
     * @return string
     */
    public function getMarcaComercial()
    {
        return $this->marcaComercial;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return ProductosEmpresa
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }
}
