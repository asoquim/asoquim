<?php

namespace EntitiesBundle\Entity;

/**
 * DocumentosCasos
 */
class DocumentosCasos
{
    /**
     * @var integer
     */
    private $idDocumento;

    /**
     * @var string
     */
    private $documento;

    /**
     * @var \EntitiesBundle\Entity\Casos
     */
    private $codigo;


    /**
     * Set idDocumento
     *
     * @param integer $idDocumento
     *
     * @return DocumentosCasos
     */
    public function setIdDocumento($idDocumento)
    {
        $this->idDocumento = $idDocumento;

        return $this;
    }

    /**
     * Get idDocumento
     *
     * @return integer
     */
    public function getIdDocumento()
    {
        return $this->idDocumento;
    }

    /**
     * Set documento
     *
     * @param string $documento
     *
     * @return DocumentosCasos
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set codigo
     *
     * @param \EntitiesBundle\Entity\Casos $codigo
     *
     * @return DocumentosCasos
     */
    public function setCodigo(\EntitiesBundle\Entity\Casos $codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return \EntitiesBundle\Entity\Casos
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
