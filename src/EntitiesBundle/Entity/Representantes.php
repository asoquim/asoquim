<?php

namespace EntitiesBundle\Entity;

/**
 * Representantes
 */
class Representantes
{
    /**
     * @var integer
     */
    private $idRepresentante;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $cargo;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $direccionOficina;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $correo;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Set idRepresentante
     *
     * @param integer $idRepresentante
     *
     * @return Representantes
     */
    public function setIdRepresentante($idRepresentante)
    {
        $this->idRepresentante = $idRepresentante;

        return $this;
    }

    /**
     * Get idRepresentante
     *
     * @return integer
     */
    public function getIdRepresentante()
    {
        return $this->idRepresentante;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Representantes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return Representantes
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     *
     * @return Representantes
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccionOficina
     *
     * @param string $direccionOficina
     *
     * @return Representantes
     */
    public function setDireccionOficina($direccionOficina)
    {
        $this->direccionOficina = $direccionOficina;

        return $this;
    }

    /**
     * Get direccionOficina
     *
     * @return string
     */
    public function getDireccionOficina()
    {
        return $this->direccionOficina;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Representantes
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Representantes
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return Representantes
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }
}
