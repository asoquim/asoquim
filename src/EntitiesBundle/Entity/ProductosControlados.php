<?php

namespace EntitiesBundle\Entity;

/**
 * ProductosControlados
 */
class ProductosControlados
{
    /**
     * @var integer
     */
    private $idProductoControlado;

    /**
     * @var string
     */
    private $registroActividadesSuceptibles;

    /**
     * @var string
     */
    private $leyDrogas;

    /**
     * @var string
     */
    private $autorizacionManejoSustancias;

    /**
     * @var string
     */
    private $permisoOperadorExplosivos;

    /**
     * @var string
     */
    private $archivoOpcional;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Set idProductoControlado
     *
     * @param integer $idProductoControlado
     *
     * @return ProductosControlados
     */
    public function setIdProductoControlado($idProductoControlado)
    {
        $this->idProductoControlado = $idProductoControlado;

        return $this;
    }

    /**
     * Get idProductoControlado
     *
     * @return integer
     */
    public function getIdProductoControlado()
    {
        return $this->idProductoControlado;
    }

    /**
     * Set registroActividadesSuceptibles
     *
     * @param string $registroActividadesSuceptibles
     *
     * @return ProductosControlados
     */
    public function setRegistroActividadesSuceptibles($registroActividadesSuceptibles)
    {
        $this->registroActividadesSuceptibles = $registroActividadesSuceptibles;

        return $this;
    }

    /**
     * Get registroActividadesSuceptibles
     *
     * @return string
     */
    public function getRegistroActividadesSuceptibles()
    {
        return $this->registroActividadesSuceptibles;
    }

    /**
     * Set leyDrogas
     *
     * @param string $leyDrogas
     *
     * @return ProductosControlados
     */
    public function setLeyDrogas($leyDrogas)
    {
        $this->leyDrogas = $leyDrogas;

        return $this;
    }

    /**
     * Get leyDrogas
     *
     * @return string
     */
    public function getLeyDrogas()
    {
        return $this->leyDrogas;
    }

    /**
     * Set autorizacionManejoSustancias
     *
     * @param string $autorizacionManejoSustancias
     *
     * @return ProductosControlados
     */
    public function setAutorizacionManejoSustancias($autorizacionManejoSustancias)
    {
        $this->autorizacionManejoSustancias = $autorizacionManejoSustancias;

        return $this;
    }

    /**
     * Get autorizacionManejoSustancias
     *
     * @return string
     */
    public function getAutorizacionManejoSustancias()
    {
        return $this->autorizacionManejoSustancias;
    }

    /**
     * Set permisoOperadorExplosivos
     *
     * @param string $permisoOperadorExplosivos
     *
     * @return ProductosControlados
     */
    public function setPermisoOperadorExplosivos($permisoOperadorExplosivos)
    {
        $this->permisoOperadorExplosivos = $permisoOperadorExplosivos;

        return $this;
    }

    /**
     * Get permisoOperadorExplosivos
     *
     * @return integer
     */
    public function getPermisoOperadorExplosivos()
    {
        return $this->permisoOperadorExplosivos;
    }

    /**
     * Set archivoOpcional
     *
     * @param string $archivoOpcional
     *
     * @return ProductosControlados
     */
    public function setArchivoOpcional($archivoOpcional)
    {
        $this->archivoOpcional = $archivoOpcional;

        return $this;
    }

    /**
     * Get archivoOpcional
     *
     * @return string
     */
    public function getArchivoOpcional()
    {
        return $this->archivoOpcional;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return ProductosControlados
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }
}
