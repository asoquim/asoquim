<?php

namespace EntitiesBundle\Entity;

/**
 * GrupoEmpresa
 */
class GrupoEmpresa
{
    /**
     * @var integer
     */
    private $idGrupo;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get idGrupo
     *
     * @return integer
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return GrupoEmpresa
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rif;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rif = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return GrupoEmpresa
     */
    public function addRif(\EntitiesBundle\Entity\Empresas $rif)
    {
        $this->rif[] = $rif;

        return $this;
    }

    /**
     * Remove rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     */
    public function removeRif(\EntitiesBundle\Entity\Empresas $rif)
    {
        $this->rif->removeElement($rif);
    }

    /**
     * Get rif
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRif()
    {
        return $this->rif;
    }
}
