<?php

namespace EntitiesBundle\Entity;

/**
 * ActividadEmpresa
 */
class ActividadEmpresa
{
    /**
     * @var integer
     */
    private $idActividad;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \EntitiesBundle\Entity\Empresas
     */
    private $rif;


    /**
     * Set idActividad
     *
     * @param integer $idActividad
     *
     * @return ActividadEmpresa
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * Get idActividad
     *
     * @return integer
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return ActividadEmpresa
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set rif
     *
     * @param \EntitiesBundle\Entity\Empresas $rif
     *
     * @return ActividadEmpresa
     */
    public function setRif(\EntitiesBundle\Entity\Empresas $rif = null)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return \EntitiesBundle\Entity\Empresas
     */
    public function getRif()
    {
        return $this->rif;
    }
}
