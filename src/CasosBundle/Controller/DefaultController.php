<?php

namespace CasosBundle\Controller;

use EntitiesBundle\Form\ActividadesType;
use EntitiesBundle\Form\UsuariosType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use EntitiesBundle\Entity\Casos;
use EntitiesBundle\Entity\Usuarios;
use EntitiesBundle\Entity\DocumentosCasos;
use EntitiesBundle\Entity\EmpresasCasos;
use EntitiesBundle\Entity\Contactos;
use EntitiesBundle\Entity\Empresas;
use EntitiesBundle\Entity\Actividades;
use Symfony\Component\Validator\Constraints\DateTime;


class DefaultController extends Controller
{
    public function listadocasoAction()
    {
        /**
         * @Route("/casos", name="casos")
         */
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            $user = $this->getUser()->getUsername();
            $userid = $this->getUser()->getId();
            $foto = $this->getUser()->getFoto();
            $nivel = $this->getUser()->getTipoUsuario();
            $em = $this->getDoctrine()->getManager();
//            $query = $em->createQuery('SELECT c.nombre,c.descripcion,c.codigo,c.fechaCreacion,c.fechaCierre,IDENTITY(c.idUsuario)as idUsuario,c.status,u.username, d.documento FROM EntitiesBundle:Casos c LEFT JOIN EntitiesBundle:Usuarios u WITH c.personaResponsable = u.id INNER JOIN EntitiesBundle:DocumentosCasos d WITH c.codigo = d.codigo');
            $listado = $em->createQuery('SELECT c.nombre,c.descripcion,c.codigo,c.fechaCreacion,c.fechaCierre,IDENTITY(c.idUsuario)as idUsuario,c.status,u.username FROM EntitiesBundle:Casos c LEFT JOIN EntitiesBundle:Usuarios u WITH c.personaResponsable = u.id')->getScalarResult();
            $qr = $em->createQuery('SELECT n.username,n.id FROM EntitiesBundle:Usuarios n WHERE n.status = 1');
            $usuarios = $qr->getScalarResult();
            $qry = $em->createQuery('SELECT e.nombre,e.rif FROM EntitiesBundle:Empresas e WHERE e.status = 1');
            $empresa = $qry->getScalarResult();
            return $this->render('CasosBundle:Default:listado.html.twig', array(
                'datos' => $listado,
                'username' => $user,
                'userid' => $userid,
                'usuarios' => $usuarios,
                'empresa' => $empresa,
                'foto' => $foto,
                'nivel' => $nivel
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function actividadesAction($id, Request $request)
    {
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            $em = $this->getDoctrine()->getManager();
            $nombre = $em->getRepository('EntitiesBundle:Casos')
                ->createQueryBuilder('c')
                ->select('c.nombre')
                ->where('c.codigo = :codigo')
                ->setParameter('codigo', $id)
                ->setMaxResults(1)
                ->getQuery()->getSingleScalarResult();
            $caso = $em->getRepository('EntitiesBundle:Casos')->find($id);
            $actividad = $em->getRepository('EntitiesBundle:Actividades')
                ->createQueryBuilder('a')
                ->where('a.codigo = :codigo')
                ->setParameter('codigo', $id)
                ->getQuery()
                ->getResult();
            $usuarios = $em->getRepository('EntitiesBundle:Usuarios')
                ->createQueryBuilder('u')
                ->getQuery()
                ->getResult();
            $codigo = $em->getRepository('EntitiesBundle:Casos')->findOneBy(['codigo' => $id]);
            $actividades = new Actividades();
            $form = $this->createForm(ActividadesType::class, $actividades);
            $form->handleRequest($request);
            if ($request->isMethod('POST')) {
                $actividades->setCodigo($codigo);
                $em->persist($actividades);
                $em->flush();
                return $this->redirectToRoute('actividades', array('id' => $id));
            }
            return $this->render('CasosBundle:Default:actividades.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'nivel' => $this->getUser()->getTipoUsuario(),
                'actividad' => $actividad,
                'form' => $form->createView(),
                'usuarios' => $usuarios,
                'nombre' => $nombre,
                'caso' => $caso
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function editarActividadAction(Request $request)
    {
        /**
         * @Route("/editar_actividad", name="editar_actividad")
         */
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $codigo = $request->get('codigo');
            /*$nombre = $request->get('nombre');*/
            $idUsuario = $request->get('usuario');
            /*$fechaInicio = $request->get('fechaInicio');
            $fechaCierre = $request->get('fechaCierre');*/
            $status = $request->get('statusActividad');
            $descripcion = $request->get('descripcion');
            $actividad = $em->getRepository('EntitiesBundle:Actividades')->find($id);
            $usuario = $em->getRepository('EntitiesBundle:Usuarios')->find($idUsuario);
            if (!$actividad) {
                throw $this->createNotFoundException("No se encontro una actividad con el codigo " . $id);
            }
            /*$actividad->setNombre($nombre);*/
            $actividad->setIdUsuario($usuario);
            /*$actividad->setFechaInicio(new \DateTime($fechaInicio));
            $actividad->setFechaCierre(new \DateTime($fechaCierre));*/
            $actividad->setStatus($status);
            $actividad->setDescripcion($descripcion);
            $em->flush();
            return $this->redirectToRoute('actividades', array('id' => $codigo));
        }
    }

    public function eliminarActividadAction($id, Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $idActividad = $request->get('idActividad');
            $em = $this->getDoctrine()->getManager();
            $actividad = $em->getRepository('EntitiesBundle:Actividades')->find($idActividad);
            if (!$actividad) {
                throw $this->createNotFoundException("No se encontro una actividad con el codigo " . $idActividad);
            }
            $em->remove($actividad);
            $em->flush();
            return $this->redirectToRoute('actividades', array('id' => $id));
        }
    }

    public function buscarcodigoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $codigo = $request->get('codigo');
            $em = $this->getDoctrine()->getManager();
            $existente = $em->getRepository('EntitiesBundle:Casos')->findOneBycodigo($codigo);
            if ($existente) {
                $response = new JsonResponse(array('success' => true));
            } else {
                $response = new JsonResponse(array('success' => false));
            }
            return $response;
        }

    }

    public function editarCasoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $codigo = $request->get('codigo');
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c.nombre,c.codigo,c.fechaCreacion,c.fechaCierre, c.fechaModificacion, c.status, c.descripcion ,u.id,d.documento,d.idDocumento FROM EntitiesBundle:Casos c LEFT JOIN EntitiesBundle:DocumentosCasos d WITH c.codigo = d.codigo LEFT JOIN EntitiesBundle:Usuarios u WITH c.personaResponsable = u.id WHERE c.codigo = :codigo')->setParameter('codigo', $codigo);
            $casos = $query->getScalarResult();
            $qr = $em->createQuery('SELECT IDENTITY(n.rif)as rif FROM EntitiesBundle:Casos c LEFT JOIN EntitiesBundle:EmpresasCasos n WITH c.codigo = n.codigo WHERE n.codigo = :codigo')->setParameter('codigo', $codigo);
            $empresas = $qr->getScalarResult();
            return new JsonResponse(array('casos' => $casos, 'empresas' => $empresas));
        }
    }

    public function actividadescasosAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $codigo = $request->get('codigo');
            $em = $this->getDoctrine()->getManager();
            $qry = $em->createQuery('SELECT a,n.codigo FROM EntitiesBundle:Actividades a LEFT JOIN EntitiesBundle:Casos n WITH a.codigo = n.codigo WHERE n.codigo = :codigo')->setParameter('codigo', $codigo);
            $actividades = $qry->getScalarResult();
            $qery = $em->createQuery('SELECT count(a)as cantidad FROM EntitiesBundle:Actividades a LEFT JOIN EntitiesBundle:Casos n WITH a.codigo = n.codigo WHERE n.codigo = :codigo')->setParameter('codigo', $codigo);
            $cantidad_act = $qery->getScalarResult();
            return new JsonResponse(array('actividades' => $actividades, 'cantidad' => $cantidad_act));

        }
    }

    public function agregarCasosAction(Request $request)
    {
        /**
         * @Route("/nuevocaso", name="nuevocaso")
         */

        $user = $this->getUser()->getUsername();
        $id_usuario = $this->getDoctrine()->getRepository('EntitiesBundle:Usuarios')->findOneByusername($user);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $nombre = $request->get('nombreCaso');
            $fechainicio = $request->get('fechainicio');
            $fechafin = $request->get('fechafin');
            $encargado = $request->get('encargado');
            $afiliado = $request->get('afiliado');
            $archivo = $request->get('documentocaso');
            $codigo = $request->get('codigo');
            // Descripción, nuevo campo para casos.
            $descripcion = $request->get('descripcionCaso');
            $documento = $request->files->get('documentocaso');
            if (!empty($documento)) {
                $ext = $documento->guessExtension();
                $file_name = md5(uniqid()) . "." . $ext;
                $documento->move("assets/documentos_caso", $file_name);
            } else {
                $file_name = NULL;
            }
//            $codigo = '';
////            $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
////            $max = strlen($pattern) - 1;
////            for ($i = 0; $i < 10; $i++)
////                $codigo .= $pattern{
////                mt_rand(0, $max)
////                };
            $caso = new Casos();
            $caso->setNombre($nombre);
            $caso->setCodigo($codigo);
            $caso->setFechaCreacion(new \DateTime($fechainicio));
            $caso->setFechaCierre(new \DateTime($fechafin));
            $caso->setPersonaResponsable($encargado);
            $caso->setStatus(1);
            $caso->setIdUsuario($id_usuario);
            $caso->setDescripcion($descripcion);

            $em->persist($caso);
            if ($file_name !== NULL) {
                $upload = new DocumentosCasos();
                $upload->setDocumento($file_name);
                $upload->setCodigo($caso);
                $em->persist($upload);
            }
            for ($i = 0; $i < count($afiliado); $i++) {
                $id_empresa = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas')->findOneByRif($afiliado[$i]);
                $empresas = new EmpresasCasos();
                $empresas->setCodigo($caso);
                $empresas->setRif($id_empresa);
                $empresas->setIdUsuario($id_usuario);
                $em->persist($empresas);
            }
            $em->flush();
            return $this->redirectToRoute('casos_listadoCasos');
        }
    }

    public function buscarCodigoAfiliadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $rif = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $codigo = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('c')
                ->select('c.codigoAfiliado')
                ->where('c.rif = :rif')
                ->setParameter('rif', $rif)
                ->getQuery()
                ->getSingleScalarResult();
//            $caso = $em->getRepository('EntitiesBundle:Casos')
//                ->createQueryBuilder('c')
//                ->select('c, MAX(c.codigo) AS codigo')
//                ->where('c.codigo LIKE :codigo')
//                ->setParameter('codigo', '%' . $codigo . '%')
//                ->setMaxResults(1)
//                ->orderBy('codigo', 'DESC')
//                ->getQuery()
//                ->getResult();
//            return new JsonResponse(['caso' => ++$caso[0]['codigo']]);
            $caso = $em->getRepository('EntitiesBundle:Casos')
                ->createQueryBuilder('c')
                ->select('c, MAX(c.codigo) AS codigo')
                ->where('c.codigo LIKE :codigo')
                ->setParameter('codigo', '%' . $codigo . '%')
                ->setMaxResults(1)
                ->orderBy('codigo', 'DESC')
                ->getQuery()
                ->getOneOrNullResult();
            if (!empty($caso['codigo'])) {
                return new JsonResponse(['caso' => ++$caso['codigo']]);
            } else {
                return new JsonResponse(['codigo' => $codigo . '-1']);
            }
        }
    }

    public function guardaEdicionAction(Request $request)
    {
        /**
         * @Route("/guardaedicion", name="guardaedicion")
         */

        $user = $this->getUser()->getUsername();
        $id_usuario = $this->getDoctrine()->getRepository('EntitiesBundle:Usuarios')->findOneByusername($user);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $nombre = $request->get('editarnombreCaso');
            $codigo = $request->get('editarcodigo');
            $fechainicio = $request->get('editarfechainicio');
            $afiliado = $request->get('editarafiliado');
            $encargado = $request->get('editarencargado');
            $descripcion = $request->get('descripcionCaso');
            $estatus = $request->get('editarstatus');
            $idDocumento = $request->get('idDocumento');
            $archivo = $request->get('editardocumentocaso');
            $documento = $request->files->get('documentocaso');
            $caso = $this->getDoctrine()->getRepository('EntitiesBundle:Casos')->findOneByCodigo($codigo);
            $upload = $this->getDoctrine()->getRepository('EntitiesBundle:DocumentosCasos')->findOneByIdDocumento($idDocumento);
            if (!empty($documento)) {
                $ext = $documento->guessExtension();
                $file_name = md5(uniqid()) . "." . $ext;
                $documento->move("assets/documentos_caso", $file_name);
                $upload->setDocumento($file_name);
                $upload->setCodigo($caso);
            }
            $caso->setNombre($nombre);
            $caso->setDescripcion($descripcion);
            $caso->setCodigo($codigo);
            if (!empty($fechacierre)) {
                $caso->setFechaModificacion(new \DateTime($fechacierre));
            } else {
                $caso->setFechaModificacion(NULL);
            }
            $caso->setPersonaResponsable($encargado);
            $caso->setStatus($estatus);
            $caso->setIdUsuario($id_usuario);
            $empresas = $this->getDoctrine()->getRepository('EntitiesBundle:EmpresasCasos')->findBy(['codigo' => $codigo]);
            if (count($afiliado) > 1) {
                for ($i = 0; $i < count($afiliado); $i++) {
                    $rif = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas')->findOneBy(['rif' => $afiliado[$i]]);
                    $empresas[$i]->setCodigo($caso);
                    $empresas[$i]->setRif($rif);
                    $empresas[$i]->setIdUsuario($id_usuario);
                }
            } elseif (count($afiliado) === 1) {
                for ($i = 0; $i < count($empresas); $i++) {
                    $rif = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas')->findOneBy(['rif' => $afiliado]);
                    $empresas[$i]->setCodigo($caso);
                    $empresas[$i]->setRif($rif);
                    $empresas[$i]->setIdUsuario($id_usuario);
                }
            }
            $em->flush();
            return $this->redirectToRoute('casos_listadoCasos');
        }
    }

    public function EdicionActividadesAction(Request $request)
    {
        /**
         * @Route("/guardaedicion", name="guardaedicion")
         */

        $user = $this->getUser()->getUsername();
        $id_usuario = $this->getDoctrine()->getRepository('EntitiesBundle:Usuarios')->findOneByusername($user);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $codigo = $request->get('actividadcodigo');
            $cant_nuevo = $request->get('cantidad_nuevo');
            $id_actividad = $request->get('idactividad');
            /*$nombre_act = $request->get('editnombreactividad');
            $fechain_act = $request->get('editfechainicioactividad');
            $fechafin_act = $request->get('editfechafinactividad');*/
            $status_act = $request->get('editstatusactividad');
            $descripcion_act = $request->get('editdescripcion');
            $nombre_act_nuevo = $request->get('nombreactinuevo');
            $fechain_act_nuevo = $request->get('fechainiactnuevo');
            $fechafin_act_nuevo = $request->get('fechafinactnuevo');
            $status_act_nuevo = $request->get('statusactnuevo');
            $descripcion_act_nuevo = $request->get('descripactnuevo');

            $caso = $this->getDoctrine()->getRepository('EntitiesBundle:Casos')->findOneByCodigo($codigo);
            if (!empty($id_actividad)) {
                $cant_act = count($id_actividad);
                for ($j = 1; $j < $cant_act; $j++) {
                    $actividades = $this->getDoctrine()->getRepository('EntitiesBundle:Actividades')->findOneByIdActividad($id_actividad[$j]);
                    /*$actividades->setNombre($nombre_act[$j]);
                    $actividades->setFechaInicio(new \DateTime($fechain_act[$j]));
                    $actividades->setFechaCierre(new \DateTime($fechafin_act[$j]));*/
                    $actividades->setDescripcion($descripcion_act[$j]);
                    $actividades->setStatus($status_act[$j]);
                    $actividades->setIdUsuario($id_usuario);
                    $actividades->setCodigo($caso);
                    $em->persist($actividades);
                }
            }
            if (!empty($cant_nuevo)) {
                for ($n = 0; $n < $cant_nuevo; $n++) {
                    $actividades_n = new Actividades;
                    /*$actividades_n->setNombre($nombre_act_nuevo[$n]);
                    $actividades_n->setFechaInicio(new \DateTime($fechain_act_nuevo[$n]));
                    $actividades_n->setFechaCierre(new \DateTime($fechafin_act_nuevo[$n]));*/
                    $actividades_n->setDescripcion($descripcion_act_nuevo[$n]);
                    $actividades_n->setStatus($status_act_nuevo[$n]);
                    $actividades_n->setIdUsuario($id_usuario);
                    $actividades_n->setCodigo($caso);
                    $em->persist($actividades_n);
                }
            }
            $em->flush();
            return $this->redirectToRoute('casos_listadoCasos');
        }
    }
}
