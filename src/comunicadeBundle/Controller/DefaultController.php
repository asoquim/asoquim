<?php

namespace comunicadeBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use EntitiesBundle\Entity\CategoriaComunicado;
use EntitiesBundle\Entity\DetallesLista;
use EntitiesBundle\Entity\Templates;
use EntitiesBundle\Form\CategoriaComunicadoType;
use Netpeople\JangoMailBundle\CampaignSending;
use Netpeople\JangoMailBundle\Exception\JangoMailException;
use Netpeople\JangoMailBundle\Groups\Group;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use EntitiesBundle\Entity\Comunicado;
use EntitiesBundle\Entity\Usuarios;
use EntitiesBundle\Entity\Lista;
use Netpeople\JangoMailBundle\AbstractSending;
use Netpeople\JangoMailBundle\Exception\TransactionalException;
use Netpeople\JangoMailBundle\TransactionalSending;
use Psr\Log\LoggerInterface;
use Netpeople\JangoMailBundle\Form\Type\LogsFilterType;
use Netpeople\JangoMailBundle\JangoMail;
use SoapClient;
use SoapFault;
use Netpeople\JangoMailBundle\Form\Type\EmailType;
use Netpeople\JangoMailBundle\Emails\Email;
use Netpeople\JangoMailBundle\Emails\EmailInterface;
use Netpeople\JangoMailBundle\Recipients\RecipientsCollection;
use Netpeople\JangoMailBundle\Recipients\Recipient;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Netpeople\JangoMailBundle\Groups\GroupAdmin;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;


class DefaultController extends Controller
{

    public function indexAction()
    {
        return $this->render('comunicadeBundle:Default:index.html.twig');
    }

    public function comunicado2Action()
    {
        return $this->render('comunicadeBundle:Default:comunicado2.html.twig');
    }


    public function comunicado1Action()
    {
        return $this->render('comunicadeBundle:Default:otromas.html.twig');
    }

    public function categoriasAction(Request $request)
    {
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            $em = $this->getDoctrine()->getManager();
            $results = $em->getRepository('EntitiesBundle:CategoriaComunicado')
                ->createQueryBuilder('cc')
                ->getQuery()
                ->getResult();
            $user = $this->getUser()->getUsername();
            $foto = $this->getUser()->getFoto();
            $categoria = new CategoriaComunicado();
            $form = $this->createForm(CategoriaComunicadoType::class, $categoria);
            $form->handleRequest($request);
            if ($request->isMethod('POST')) {
                $em->persist($categoria);
                $em->flush();
                return $this->redirectToRoute('comunicado_categorias');
            }
            return $this->render('comunicadeBundle:Default:categorias.html.twig', array(
                'username' => $user,
                'foto' => $foto,
                'results' => $results,
                'form' => $form->createView()
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function editarCategoriaAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $descripcion = $request->get('descripcion');
            $categoria = $em->getRepository('EntitiesBundle:CategoriaComunicado')->find($id);
            if (!$categoria) {
                return $this->createNotFoundException("No se encontró una categoría con el id " . $id);
            }
            $categoria->setDescripcion($descripcion);
            $em->flush();
            return $this->redirectToRoute('comunicado_categorias');
        }
    }

    public function comunicadosAction(Request $request)
    {
        /**
         * @Route("/lista_comunicados", name="lista_comunicados")
         */
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser()->getUsername();
            $foto = $this->getUser()->getFoto();
            $nivel = $this->getUser()->getTipoUsuario();
            $results = $em->getRepository('EntitiesBundle:Comunicado')
                ->createQueryBuilder('c')
                ->getQuery()
                ->getResult();
            $mailerContacts = $em->getRepository('EntitiesBundle:Usuarios')
                ->createQueryBuilder('u')
                ->select('u.mailerContact')
                ->where('u.mailerContact = true')
                ->getQuery()
                ->getResult();
            return $this->render('comunicadeBundle:Default:listacomunicado.html.twig', array(
                'username' => $user,
                'foto' => $foto,
                'nivel' => $nivel,
                'results' => $results,
                'mailerContacts' => count($mailerContacts)
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function aprobacionAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $comunicado = $em
            ->getRepository('EntitiesBundle:Comunicado')
            ->find($id);
        if (!$comunicado) {
            return $this->createNotFoundException("No se encontró un comunicado con el id " . $id);
        }
        $comunicado->setStatus(2);
        $em->flush();
        return $this->redirectToRoute('lista_comunicados');
    }

    public function verComunicadoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $nombre = $em->getRepository('EntitiesBundle:Comunicado')
            ->createQueryBuilder('c')
            ->select('c.nombre')
            ->where('c.idComunicado = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
        $comunicado = $em->getRepository('EntitiesBundle:Comunicado')
            ->createQueryBuilder('c')
            ->select('c.comunicado')
            ->where('c.idComunicado = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
        return $this->render('comunicadeBundle:Default:vercomunicado.html.twig', array(
            'username' => $this->getUser()->getUsername(),
            'foto' => $this->getUser()->getFoto(),
            'comunicado' => $comunicado,
            'nombre' => $nombre
        ));
    }

    public function editandoComunicadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $html = $request->get('html');
            $comunicado = $em->getRepository('EntitiesBundle:Comunicado')->find($id);
            if (!$comunicado) {
                return new JsonResponse('No se encontro un comunicado con ese id');
            } else {
                $comunicado->setComunicado($html);
                $em->flush();
                return new JsonResponse(['success' => true]);
            }
        }
    }

    public function eliminarCategoriaAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $categoria = $em->getRepository('EntitiesBundle:CategoriaComunicado')->find($id);
            if (!$categoria) {
                return $this->createNotFoundException("No se encontró una categoría con el id " . $id);
            }
            $em->remove($categoria);
            $em->flush();
            return $this->redirectToRoute('comunicado_categorias');
        }
    }

    public function rechazarComunicadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $comunicado = $em->getRepository('EntitiesBundle:Comunicado')
                ->find($id);
            if ($comunicado) {
                $comunicado->setStatus(0);
                $em->flush();
                return new JsonResponse(['success' => TRUE]);
            } else {
                return new JsonResponse(['success' => FALSE]);
            }
        }
    }

    public function aprobarComunicadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $lista = $request->get('lista');
            $listas = [];
            $em = $this->getDoctrine()->getManager();
            $comunicado = $em->getRepository('EntitiesBundle:Comunicado')
                ->find($id);
            $mailerContacts = $em->getRepository('EntitiesBundle:Usuarios')
                ->createQueryBuilder('u')
                ->select('u.mailerContact')
                ->where('u.mailerContact = true')
                ->getQuery()
                ->getResult();
            if (strpos($lista, ',') != FALSE) {
                $listas = explode(',', $lista);
                $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                    ->findByIdLista($listas);
            } else {
                $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                    ->find($lista);
            }
            $aprobacion = $em->getRepository('EntitiesBundle:Usuarios')
                ->find($this->getUser()->getId());
            $aprobacion->addIdComunicado($comunicado);
            $em->flush();
            if ($comunicado->countId() == count($mailerContacts)) {
                set_time_limit(300);
                $email = new Email();
                $grupo = new Group();
                if (count($grupoBD) > 1) {
                    foreach ($grupoBD as $item) {
                        $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($item->getIdGrupo());
                    }
                } else {
                    $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupoBD->getIdGrupo());
                }
                $email->addGroup($grupo);
                $email->setMessage($comunicado->getComunicado());
                $email->setSubject($comunicado->getNombre());
                $email->setOptions(['SendDate' => $comunicado->getFechaEnvio()->format('Y-m-d H:i:s')]);
                if ($this->get('jango_mail')->send($email)) {
                    $comunicado->setStatus(1);
                    $em->flush();
                    return new JsonResponse(['sent' => TRUE]);
                } else {
                    return new JsonResponse(['error' => $this->get('jango_mail')->getError()]);
                }
            } else {
                return new JsonResponse(['success' => TRUE]);
            }
        }
    }

    public function correosAction(Request $request)
    {
        /**
         * @Route("/lista_correos", name="lista_correos")
         */
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser()->getUsername();
            $foto = $this->getUser()->getFoto();
            $nivel = $this->getUser()->getTipoUsuario();
            $lista = $em->getRepository('EntitiesBundle:TipoLista')
                ->createQueryBuilder('t')
                ->getQuery()
                ->getResult();
            $contactos = $em->getRepository('EntitiesBundle:Contactos')
                ->createQueryBuilder('c')
                ->select('c.idContacto, c.nombre')
                ->where('c.status = 1')
                ->getQuery()
                ->getResult();
            $result = $em->getRepository('EntitiesBundle:Lista')
                ->createQueryBuilder('l')
                ->getQuery()
                ->getResult();
            return $this->render('comunicadeBundle:Default:listacorreo.html.twig', array(
                'username' => $user,
                'foto' => $foto,
                'nivel' => $nivel,
                'result' => $result,
                'lista' => $lista,
                'contactos' => $contactos
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function comunicadowAction()
    {
        $em = $this->getDoctrine()->getManager();
        $template = $em->getRepository('EntitiesBundle:Templates')
            ->createQueryBuilder('t')
            ->getQuery()
            ->getResult();
        return $this->render('comunicadeBundle:Default:wizard.html.twig', array(
            'username' => $this->getUser()->getUsername(),
            'foto' => $this->getUser()->getFoto(),
            'template' => $template
        ));
    }

    public function crearComunicadoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $templates = $em->getRepository('EntitiesBundle:Templates')
            ->createQueryBuilder('t')
            ->getQuery()
            ->getResult();
        $lista = $em->getRepository('EntitiesBundle:TipoLista')
            ->createQueryBuilder('t')
            ->getQuery()
            ->getResult();
        $contactos = $em->getRepository('EntitiesBundle:Contactos')
            ->createQueryBuilder('c')
            ->select('c.idContacto, c.nombre')
            ->where('c.status = 1')
            ->getQuery()
            ->getResult();
        $categorias = $em->getRepository('EntitiesBundle:CategoriaComunicado')
            ->createQueryBuilder('c')
            ->getQuery()
            ->getResult();
        return $this->render('comunicadeBundle:Default:crearcomunicado.html.twig', array(
            'username' => $this->getUser()->getUsername(),
            'foto' => $this->getUser()->getFoto(),
            'templates' => $templates,
            'lista' => $lista,
            'contactos' => $contactos,
            'categorias' => $categorias
        ));
    }

    public function listaCorreosAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $correos = $em->getRepository('EntitiesBundle:Contactos')
                ->createQueryBuilder('c')
                ->select('c.idContacto, c.correo')
                ->where('c.status = :status')
                ->setParameter('status', 1)
                ->getQuery()
                ->getResult();
            if ($correos) {
                return new JsonResponse(['success' => TRUE, 'correos' => $correos]);
            } else {
                return new JsonResponse(['success' => FALSE]);
            }
        }
    }

    public function agregarListaAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $nombre = $request->get('nombre-lista');
            $tipo = $request->get('tipo-lista');
            $contactos = $request->get('contactos-lista');
            $idTipo = $em->getRepository('EntitiesBundle:TipoLista')->find($tipo);
            $idContactos = $em->getRepository('EntitiesBundle:Contactos')->findAll($contactos);
            $lista = new Lista();
            $lista->setNombre($nombre);
            $lista->setIdTipo($idTipo);
            $grupo = new Group();
            $detalles = new DetallesLista();
            foreach ($idContactos as $contacto) {
                $recipient = new Recipient($contacto->getCorreo(), $contacto->getNombre());
                $grupo->addRecipient($recipient);
            }
            $grupo->setName($lista->getNombre());
            ini_set('max_execution_time', 0);
            $grupo = $this->get('jango_mail')->getGroupAdmin()->addGroup($grupo);
            if ($grupo != false) {
                $lista->setIdGrupo($grupo->getGroupID());
                $em->persist($lista);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->addMembers($grupo);
            } else
                return new JsonResponse(["success" => false]);

            for ($i = 0; $i < sizeof($idContactos); $i++) {
                $detalles->setIdContacto($idContactos[$i]);
                $detalles->setIdLista($lista);
                $em->persist($detalles);
                unset($detalles);
                $detalles = new DetallesLista();
            }
            $em->flush();
            return new JsonResponse(["success" => TRUE]);
        }
    }

    public function listaContactosAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $contactos = $em->getRepository('EntitiesBundle:Lista')
                ->createQueryBuilder('l')
                ->select('l.nombre, l.idLista')
                ->where('l.nombre != :nombre')
                ->setParameter('nombre', 'Lista de pruebas Asoquim')
                ->getQuery()
                ->getResult();
            if ($contactos) {
                return new JsonResponse(['success' => TRUE, 'contactos' => $contactos]);
            } else {
                return new JsonResponse(['success' => FALSE]);
            }
        }
    }

    public function detallesListaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $lista = $em->getRepository('EntitiesBundle:Lista')->findOneByIdLista($id);
        $detalles = $em->getRepository('EntitiesBundle:DetallesLista')->findByIdLista($lista);
        $qb = $em->createQueryBuilder();
        foreach ($detalles as $contacto) {
            $enlista[] = $contacto->getIdContacto()->getIdContacto();
        }
        $contactos = $qb->select('c')->from('EntitiesBundle:Contactos', 'c')
            ->add('where', $qb->expr()->notIn('c.idContacto', $enlista))
            ->getQuery()->getResult();
        return $this->render('comunicadeBundle:Default:detalleslista.html.twig', array(
            'username' => $this->getUser()->getUsername(),
            'foto' => $this->getUser()->getFoto(),
            'contactos' => $detalles,
            'lista' => $lista,
            'agregarcontacto' => $contactos
        ));
    }

    public function verAprobacionAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $comunicado = $em->getRepository('EntitiesBundle:Comunicado')
            ->find($id);
        $usuario = $em->getRepository('EntitiesBundle:Usuarios')->find($this->getUser()->getId());
        $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
        if ($comunicado->getEmailId()) {
            $maildata = $mailer->GetMassEmailReport($comunicado->getEmailId());
            $detailmail = $mailer->GetDetails($comunicado->getEmailId());
        } else {
            $maildata = $mailer->GetMassEmailReport($comunicado->getAprobacionId());
            $detailmail = $mailer->GetDetails($comunicado->getAprobacionId());
        }
        return $this->render('comunicadeBundle:Default:veraprobacion.html.twig', array(
            'username' => $this->getUser()->getUsername(),
            'foto' => $this->getUser()->getFoto(),
            'comunicado' => $comunicado,
            'estadisticas' => $maildata,
            'detalles' => $detailmail
        ));
    }

    public function enviadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            set_time_limit(300);
            $contacts = $request->get('contacts');
            $bool = $request->get('bool');
            $subject = $request->get('subject');
            $sendDate = $request->get('sendDate');
            $html = $request->get('html');
            $css = $request->get('css');
            $converter = $this->get('css_to_inline_email_converter');
            $message = $converter->inlineCSS($html, $css);
            $group = $request->get('group');
            $em = $this->getDoctrine()->getManager();
            $email = new Email();
            $grupo = new Group();
            if ($bool) {
                if (count($contacts) > 1) {
                    $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                        ->findBy(['idLista' => $contacts]);
                    foreach ($grupoBD as $item) {
                        $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($item->getIdGrupo());;
                    }
                } else {
                    $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                        ->findOneBy(['idLista' => $contacts]);
                    $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupoBD->getIdGrupo());
                }
                $email->addGroup($grupo);
                $email->setMessage($message);
                $email->setSubject($subject);
            } else {
                $tipoLista = $em
                    ->getRepository('EntitiesBundle:TipoLista')
                    ->find(4);
                $contactsID = $em
                    ->getRepository('EntitiesBundle:DetallesLista')
                    ->findBy(['idLista' => $contacts]);
                $contactsInfo = $em
                    ->getRepository('EntitiesBundle:Contactos')
                    ->findBy(['idContacto' => $contactsID]);
                foreach ($contactsInfo as $item) {
                    $recipient = new Recipient($item->getCorreo(), $item->getNombre());
                    $grupo->addRecipient($recipient);
                }
                $grupo->setName('Grupo default-' . substr(md5(microtime()), rand(0, 26), 4));
                $campana = new CampaignSending($this->get('jango_mail'));
                $campana->setEmail($email);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->addGroup($grupo);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->addMembers($grupo);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupo->getGroupID());
                $email->addGroup($grupo);
                $email->setMessage($message);
                $email->setSubject($subject);
//                $email->setOptions(['SendDate' => '2017-10-31 09:20:00']);
                $lista = new Lista();
                $lista->setNombre($grupo->getName());
                $lista->setIdTipo($tipoLista);
                $em->persist($lista);
                foreach ($contactsInfo as $item) {
                    $detalle = new DetallesLista();
                    $detalle->setIdLista($lista);
                    $detalle->setIdContacto($item);
                    $em->persist($detalle);
                }
                $em->flush();
            }
            if ($this->get('jango_mail')->send($email)) {
                $comunicado = new Comunicado();
                $comunicado->setEmailId($email->getEmailID());
                $comunicado->setNombre($subject);
                $comunicado->setComunicado($message);
                $comunicado->setFechaEnvio(new \DateTime($sendDate));
                $comunicado->setStatus(2);
                $em->persist($comunicado);
                $em->flush();
                return new JsonResponse(['success' => TRUE]);
            } else {
                return new JsonResponse(['success' => FALSE, 'error' => $this->get('jango_mail')->getError()]);
            }
        }
    }

    public function guardarComunicadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            set_time_limit(300);
            $contacts = $request->get('contacts');
            $bool = $request->get('bool');
            $subject = $request->get('subject');
            $sendDate = $request->get('sendDate');
            $message = $request->get('html');
//            $css = $request->get('css');
//            $converter = $this->get('css_to_inline_email_converter');
//            $message = $converter->inlineCSS($html, $css);
            //Adrian no me borres
            $group = $request->get('group');
            $em = $this->getDoctrine()->getManager();
            $fecha = new \DateTime($sendDate, new \DateTimeZone('America/Caracas'));
            $email = new Email();
            $grupo = new Group();
            $comunicado = new Comunicado();
            if ($bool) {
                $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                    ->findOneByNombre('Lista de pruebas Asoquim');
                $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupoBD->getIdGrupo());
                $email->addGroup($grupo);
                $email->setMessage($message);
                $email->setSubject($subject);
                if (count($contacts) > 1) {
                    foreach ($contacts as $item) {
                        $comunicado->addLista($em
                            ->getRepository('EntitiesBundle:Lista')
                            ->findOneByIdLista($item));
                    }
                } else {
                    $comunicado->addLista($em
                        ->getRepository('EntitiesBundle:Lista')
                        ->find($contacts));
                }
            } else {
                $tipoLista = $em
                    ->getRepository('EntitiesBundle:TipoLista')
                    ->find(1);
                if (count($contacts) > 1) {
                    $contactsID = $em
                        ->getRepository('EntitiesBundle:DetallesLista')
                        ->findBy(['idContacto' => $contacts]);
                    $contactsInfo;
                    foreach ($contacts as $item) {
                        $contactsInfo = $em
                            ->getRepository('EntitiesBundle:Contactos')
                            ->findBy(['idContacto' => $item]);
                        foreach ($contactsInfo as $info) {
                            $recipient = new Recipient($info->getCorreo(), $info->getNombre());
                            $grupo->addRecipient($recipient);
                        }
                    }
                } else {
                    $contactsInfo = $em
                        ->getRepository('EntitiesBundle:Contactos')
                        ->find($contacts);
                    $recipient = new Recipient($contactsInfo->getCorreo(), $contactsInfo->getNombre());
                    $grupo->addRecipient($recipient);
                }
                $grupo->setName('Grupo-' . substr(md5(microtime()), rand(0, 26), 4));
                $campana = new CampaignSending($this->get('jango_mail'));
                $campana->setEmail($email);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->addGroup($grupo);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->addMembers($grupo);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupo->getGroupID());
                $lista = new Lista();
                $lista->setNombre($grupo->getName());
                $lista->setIdTipo($tipoLista);
                $lista->setIdGrupo($grupo->getGroupID());
                $em->persist($lista);
                if (count($contacts) > 1) {
                    foreach ($contacts as $item) {
                        $contactsInfo = $em
                            ->getRepository('EntitiesBundle:Contactos')
                            ->findBy(['idContacto' => $item]);
                        foreach ($contactsInfo as $info) {
                            $detalle = new DetallesLista();
                            $detalle->setIdLista($lista);
                            $detalle->setIdContacto($info);
                            $em->persist($detalle);
                        }
                    }
                } else {
                    $contactsInfo = $em
                        ->getRepository('EntitiesBundle:Contactos')
                        ->find($contacts);
                    $detalle = new DetallesLista();
                    $detalle->setIdLista($lista);
                    $detalle->setIdContacto($contactsInfo);
                    $em->persist($detalle);
                }
                $em->flush();
                $grupoBD = $em->getRepository('EntitiesBundle:Lista')
                    ->find(2);
                $grupo = $this->get('jango_mail')->getGroupAdmin()->getGroupByGroupID($grupoBD->getIdGrupo());
                $email->addGroup($grupo);
                $email->setMessage($message);
                $email->setSubject($subject);
                $comunicado->addLista($lista);
            }
            if ($this->get('jango_mail')->send($email)) {
                $comunicado->setAprobacionId($email->getEmailID());
                $comunicado->setNombre($subject);
                $comunicado->setComunicado($message);
                $comunicado->setFechaEnvio($fecha);
                $comunicado->setStatus(2);
                $em->persist($comunicado);
                $em->flush();
                return new JsonResponse(['success' => TRUE]);
            } else {
                return new JsonResponse(['success' => FALSE, 'error' => $this->get('jango_mail')->getError()]);
            }
        }
    }


    public function edicion2Action(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $plantilla = $request->get('template');
            $template = $em->getRepository('EntitiesBundle:Templates')
                ->createQueryBuilder('t')
                ->select('t.header')
                ->where('t.id = :id')
                ->setParameter('id', $plantilla)
                ->getQuery()
                ->getSingleScalarResult();
            return $this->render('comunicadeBundle:Default:otromas.html.twig', array(
                'template' => $template
            ));
        }
    }

    public function ultimopasoAction(Request $request)
    {
        $user = $this->getUser()->getUsername();
        $foto = $this->getUser()->getFoto();
        return $this->render('comunicadeBundle:Default:ultimopaso.html.twig', array(
            'username' => $user,
            'foto' => $foto
        ));
    }


    public function listadoprogramadosAction()
    {

        $em = $this->getDoctrine()->getManager();
        $comunicados = $em->getRepository('EntitiesBundle:Comunicado')->findALL();
        return $this->render('comunicadeBundle:Default:listadoprogramados.html.twig', array('datos' => $comunicados));

    }

    public function listasAction()
    {

        $user = $this->getUser()->getUsername();
        $foto = $this->getUser()->getFoto();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT l.nombre,l.descripcion FROM EntitiesBundle:Lista l');
        $listado = $query->getScalarResult();

        return $this->render('comunicadeBundle:Default:listado.html.twig', array(
            'datos' => $listado,
            'username' => $user,
            'foto' => $foto
        ));
    }

    public function listas21Action(Request $Request)
    {

        $contacto = $this->getDoctrine()
            ->getRepository('EntitiesBundle:Contactos')
            ->findAll();

        return $this->render('comunicadeBundle:Default:listas.html.twig', array('contacto' => $contacto));
    }


    public function detallecomunicado3Action(Request $request, $id)
    {
        $datos = $this->getDoctrine()
            ->getRepository('EntitiesBundle:Comunicado')
            ->findOneByIdComunicado($id);

        $nombre = $datos->getNombre();
        $cuerpo = $datos->getComunicado();

        return $this->render('comunicadeBundle:Default:detallecomunicado3.html.twig', array('nombre' => $nombre, 'cuerpo' => $cuerpo));
    }


    public function htmlAction()
    {
        return $this->render('comunicadeBundle:Default:html.html.twig');
    }

    public function html23Action()
    {
        return $this->render('comunicadeBundle:Default:html.html.twig');
    }

    public function emailBouncesAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $comunicado = $em->getRepository('EntitiesBundle:Comunicado')->find($id);
            $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
            if ($comunicado->getEmailId())
                $rebotes = $mailer->GetBounces($comunicado->getEmailId());
            else
                $rebotes = $mailer->GetBounces($comunicado->getAprobacionId());
            return new JsonResponse(['success' => true, 'rebotes' => $rebotes]);
        }
    }

    public function emailOpensAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $comunicado = $em->getRepository('EntitiesBundle:Comunicado')->find($id);
            $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
            if ($comunicado->getEmailId())
                $opens = $mailer->GetOpens($comunicado->getEmailId());
            else
                $opens = $mailer->GetOpens($comunicado->getAprobacionId());
            return new JsonResponse(['success' => true, 'opens' => $opens]);
        }
    }

    public function groupRenameAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $nombre = $request->get('nombre');
            $em = $this->getDoctrine()->getManager();
            $lista = $em->getRepository('EntitiesBundle:Lista')->findOneByIdLista($id);
            $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
            $result = $mailer->EditGroupName($lista->getNombre(), $nombre);
            if ($result) {
                $lista->setNombre($nombre);
                $em->persist($lista);
                $em->flush();
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($request->headers->get('referer'));
        }
    }

    public function groupDeleteAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $lista = $em->getRepository('EntitiesBundle:Lista')->findOneByIdLista($id);
            ini_set('max_execution_time', 0);
            $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
            if ($lista->getIdGrupo())
                $result = $mailer->DeleteGroup($lista->getIdGrupo());
            if ($lista->getIdGrupo() == '' || $result) {
                $em->remove($lista);
                $em->flush();
                return $this->redirect($request->headers->get('referer'));
            }
        }
    }

    public function contactEditAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $correo = $request->get('email');
            $em = $this->getDoctrine()->getManager();
            $contacto = $em->getRepository('EntitiesBundle:Contactos')->findOneByIdContacto($id);
            $listas = $em->getRepository('EntitiesBundle:DetallesLista')->findByIdContacto($contacto);
            foreach ($listas as $lista) {
                if ($lista->getIdLista()->getIdGrupo()) {
                    $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
                    $respuesta = $mailer->UserEdit($lista->getIdLista()->getNombre(), $contacto->getCorreo(), $correo);
                }
            }
            $contacto->setCorreo($correo);
            $em->persist($contacto);
            $em->flush();
            return $this->redirect($request->headers->get('referer'));
        }
    }

    public function contactDeleteAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $grupoId = $request->get('grupo');
            $correoId = $request->get('email');
            $em = $this->getDoctrine()->getManager();
            $contacto = $em->getRepository('EntitiesBundle:Contactos')->findOneByIdContacto($correoId);
            $grupo = $em->getRepository('EntitiesBundle:Lista')->findOneByIdLista($grupoId);
            $lista = $em->getRepository('EntitiesBundle:DetallesLista')->findOneBy(['idLista' => $grupo, 'idContacto' => $contacto]);
            ini_set('max_execution_time', 0);
            $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
            if ($grupo->getIdGrupo())
                $result = $mailer->DeleteEmailFromGroup($grupo->getNombre(), $contacto->getCorreo());
            if ($grupo->getIdGrupo() == '' || $result) {
                $em->remove($lista);
                $em->flush();
                return $this->redirect($request->headers->get('referer'));
            }
        }
    }

    public function addContactsAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $contactos = $request->get("contactos");
            $lista = $request->get("lista");
            $contactos = explode('&', $contactos);
            foreach ($contactos as $contacto) {
                $contactsid[] = str_replace('contactos-lista=', '', $contacto);
            }
            $em = $this->getDoctrine()->getManager();
            $contactos = $em->getRepository('EntitiesBundle:Contactos')->findByIdContacto($contactsid);
            $lista = $em->getRepository('EntitiesBundle:Lista')->findOneByIdLista($lista);
            foreach ($contactos as $contacto) {
                $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
                $respuesta = $mailer->addGroupMember($lista->getNombre(), $contacto->getCorreo());
                dump($respuesta);
                $detallelista = new DetallesLista();
                $detallelista->setIdContacto($contacto);
                $detallelista->setIdLista($lista);
                $em->persist($detallelista);
                $em->flush();
                unset($detallelista);
            }
            return new JsonResponse(['success' => true]);
        }
    }
}