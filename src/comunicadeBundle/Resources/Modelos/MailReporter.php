<?php

namespace comunicadeBundle\Resources\Modelos;

class MailReporter
{

    private $usuario;
    private $password;

    public function __construct($usuario, $password)
    {
        $this->usuario = $usuario;
        $this->password = $password;

    }


    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function GetMassEmailReport($mailid)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/GetMassEmailReport?Username=' . $this->usuario . '&Password=' . $this->password . '&JobID=' . $mailid;
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        curl_close($ch);
        if ($content) {
            $result['destinatary'] = $content->int[0];
            $result['open'] = $content->int[1];
            $result['clicks'] = $content->int[2];
            $result['unsubscribes'] = $content->int[3];
            $result['bounces'] = $content->int[4];
            $result['forwards'] = $content->int[5];
            $result['replies'] = $content->int[6];
            $result['views'] = $content->int[7];
            return $result;
        }
        return $content;
    }

    public function GetDetails($mailid)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/GetCampaignDetails_XML?Username=' . $this->usuario . '&Password=' . $this->password . '&JobId=' . $mailid . '&Options=';
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function GetBounces($mailid)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/Reports_GetBouncesByCampaign_XML?Username=' . $this->usuario . '&Password=' . $this->password . '&JobID=' . $mailid . '&Options=';
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        curl_close($ch);

        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function GetOpens($mailid)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/Reports_GetOpens_String?Username=' . $this->usuario . '&Password=' . $this->password . '&JobID=' . $mailid . '&SortBy=e&SortOrder=a&WhichTime=f&RowDelimiter=n&ColDelimiter=,&TextQualifier="';
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        curl_close($ch);

        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function EditGroupName($OldName, $NewName)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/Groups_Rename?Username=' . $this->usuario . '&Password=' . $this->password . '&OldGroupName=' . urlencode($OldName) . '&NewGroupName=' . urlencode($NewName);
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function DeleteGroup($idGroup)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/DeleteGroupByID?Username=' . $this->usuario . '&Password=' . $this->password . '&GroupID=' . $idGroup;
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function UserEdit($groupName, $contact, $value)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/Groups_EditMember_ByAddress?Username=' . $this->usuario . '&Password=' . $this->password . '&GroupName=' . urlencode($groupName) . '&EmailAddress=' . $contact . '&FieldName=EmailAddress&NewFieldValue=' . $value;
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function DeleteEmailFromGroup($groupName, $contact)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/DeleteGroupMember?Username=' . $this->usuario . '&Password=' . $this->password . '&GroupName=' . urlencode($groupName) . '&EmailAddress=' . $contact;
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }

    public function addGroupMember($groupName, $Email)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        $url = 'http://api.jangomail.com/api.asmx/AddGroupMember?Username=' . $this->usuario . '&Password=' . $this->password . '&GroupName=' . urlencode($groupName) . '&EmailAddress=' . $Email . '&FieldNames=&FieldValues=';
        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);
        $content = simplexml_load_string($content);
        return $content;
    }


}


?>