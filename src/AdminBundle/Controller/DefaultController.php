<?php

namespace AdminBundle\Controller;

use EntitiesBundle\Entity\Usuarios;
use EntitiesBundle\Form\UsuariosType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends Controller
{

    public function registerAction()
    {
        $usuarios = new Usuarios();
        $form = $this->createForm(UsuariosType::class, $usuarios);
        return $this->render('AdminBundle:Default:layout.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function loginAction()
    {
        $usuarios = new Usuarios();
        $form = $this->createForm(UsuariosType::class, $usuarios);
        return $this->render('AdminBundle:Default:layout.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function inicioAction(Request $request)
    {
        /**
         * @Route("/admin", name="admin")
         */
        return $this->redirectToRoute('fos_user_security_login');
    }

    public function usuariosAction(Request $request)
    {
        /**
         * @Route("/admin_usuarios", name="usuarios")
         */
        $usr = $this->getDoctrine()->getRepository('EntitiesBundle:Usuarios');
        $qb = $usr->createQueryBuilder('u')
            ->getQuery();
        $results = $qb->getResult();
        $user = $this->getUser()->getUserName();
        $foto = $this->getUser()->getFoto();
        $nivel = $this->getUser()->getTipoUsuario();
        $manager = $this->container->get('fos_user.user_manager');
        $formEdit = $this->createForm('EntitiesBundle\Form\UsuariosType');
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $usuarios = $manager->createUser();

            $usuarios->setUsername($request->get('nombre'))
                ->setEmail($request->get('email'))
                ->setTelefono($request->get('telefono'))
                ->setCargo($request->get('cargo'))
                ->setDireccion($request->get('direccion'))
                ->setTipoUsuario($request->get('tipo'))
                ->setPlainPassword($request->get('pass'))
                ->setPassword($request->get('pass'))
                ->setEnabled(true);

            $em->persist($usuarios);
            $em->flush();

            return $this->redirectToRoute('admin_usuarios');
        }

        return $this->render('AdminBundle:Default:usuarios.html.twig', array(
            'username' => $user,
            'foto' => $foto,
            'results' => $results,
            'formEdit' => $formEdit->createView(),
            'nivel' => $nivel
        ));
    }

    public function perfilAction(Request $request)
    {
        /**
         * @Route("/perfil", name="perfil")
         */
        $results = $this->getDoctrine()
            ->getRepository('EntitiesBundle:Usuarios')
            ->findOneById($this->getUser()->getId());
        $user = $this->getUser()->getUserName();
        $foto = $this->getUser()->getFoto();
        $nivel = $this->getUser()->getTipoUsuario();
        $formEdit = $this->createForm('EntitiesBundle\Form\UsuariosType');
        return $this->render('AdminBundle:Default:profile.html.twig', array(
            'username' => $user,
            'foto' => $foto,
            'results' => $results,
            'formEdit' => $formEdit->createView(),
            'nivel' => $nivel
        ));
    }

    public function editarFotoAction(Request $request)
    {
        /**
         * @Route("/foto_perfil", name="foto_perfil")
         */
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $foto = $request->files->get('fotoperfil');
            if (!empty($foto)) {
                $file_name = md5(uniqid()) . "." . $foto->guessExtension();
                $foto->move("assets/images/users", $file_name);
                $usuario = $this->getDoctrine()->getRepository('EntitiesBundle:Usuarios')->find($this->getUser()->getId());
                $usuario->setFoto($file_name);
                $em->flush();
            } else {
                echo "No se cargo el archivo";
            }
            return $this->redirectToRoute('perfil_usuario');
        }
    }

    public function editarUsuarioAction(Request $request)
    {
        /**
         * @Route("/editar_usuario", name="editar_usuario")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $nombre = $request->get('nombre');
            $telefono = $request->get('telefono');
            $cargo = $request->get('cargo');
            $correo = $request->get('correo');
            $direccion = $request->get('direccion');
            $tipo = $request->get('tipo');
            $em = $this->getDoctrine()->getManager();
            $usuario = $em->getRepository('EntitiesBundle:Usuarios')->find($id);
            if (!$usuario) {
                throw $this->createNotFoundException("No se encontro un contacto con el id " . $id);
            }
            $usuario->setUsername($nombre);
            $usuario->setTelefono($telefono);
            $usuario->setCargo($cargo);
            $usuario->setEmail($correo);
            $usuario->setDireccion($direccion);
            $usuario->setTipoUsuario($tipo);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public function desactivarUsuarioAction(Request $request)
    {
        /**
         * @Route("/desactivar_usuario", name="desactivar_usuario")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('usuario');
            $em = $this->getDoctrine()->getManager();
            $usuario = $em->getRepository('EntitiesBundle:Usuarios')->find($id);
            if (!$usuario) {
                throw $this->createNotFoundException("No se encontró un usuario con el id " . $id);
            }
            $usuario->setStatus(0);
            $em->flush();
            return new JsonResponse(array('success' => TRUE));
        }
    }

    public function activarUsuarioAction(Request $request)
    {
        /**
         * @Route("/activar_usuario", name="activar_usuario")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('usuario');
            $em = $this->getDoctrine()->getManager();
            $usuario = $em->getRepository('EntitiesBundle:Usuarios')->find($id);
            if (!$usuario) {
                throw $this->createNotFoundException("No se encontró un usuario con el id " . $id);
            }
            $usuario->setStatus(1);
            $em->flush();
            return new JsonResponse(array('success' => TRUE));
        }
    }

}