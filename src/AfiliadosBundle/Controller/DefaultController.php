<?php

namespace AfiliadosBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use EntitiesBundle\Entity\Archivos;
use EntitiesBundle\Entity\Islr;
use EntitiesBundle\Entity\Iva;
use EntitiesBundle\Entity\Pagos;
use EntitiesBundle\Entity\ProductosControlados;
use EntitiesBundle\Entity\Sector;
use EntitiesBundle\Entity\Usuarios;
use EntitiesBundle\Entity\AsistenciaTecnica;
use EntitiesBundle\Entity\Capital;
use EntitiesBundle\Entity\Contactos;
use EntitiesBundle\Entity\Empresas;
use EntitiesBundle\Entity\Exportaciones;
use EntitiesBundle\Entity\Paises;
use EntitiesBundle\Entity\ProductosEmpresa;
use EntitiesBundle\Entity\Referencias;
use EntitiesBundle\Entity\Representantes;
use EntitiesBundle\Form\AsistenciaTecnicaType;
use EntitiesBundle\Form\ContactosType;
use EntitiesBundle\Form\EmpresasType;
use EntitiesBundle\Form\PagosType;
use EntitiesBundle\Form\PaisesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Query\ResultSetMapping;

class DefaultController extends Controller
{

    public function registrarAction()
    {
        /**
         * @Route("/registro", name="registro")
         */
        $em = $this->getDoctrine()->getManager();
        $paises = $em->getRepository('EntitiesBundle:Paises')
            ->createQueryBuilder('p')
            ->getQuery()
            ->getResult();
        return $this->render('AfiliadosBundle:Default:registro.html.twig', array(
            'paises' => $paises
        ));
    }

    public function dashboardAction()
    {
        /**
         * @Route("/dashboard", name="dashboard")
         */
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            $user = $this->getUser()->getUsername();
            $foto = $this->getUser()->getFoto();
            $nivel = $this->getUser()->getTipoUsuario();
            $em = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas');
            $pendientes = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 3')
                ->getQuery()->getSingleScalarResult();
            $activos = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 1')
                ->getQuery()->getSingleScalarResult();
            $inactivos = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 0')
                ->getQuery()->getSingleScalarResult();
            $ca = $this->getDoctrine()->getRepository('EntitiesBundle:Casos');
            $casosabiertos = $ca->createQueryBuilder('c')
                ->select('count(c.status)')
                ->where('c.status = 1')
                ->getQuery()->getSingleScalarResult();
            $casoscerrados = $ca->createQueryBuilder('c')
                ->select('count(c.status)')
                ->where('c.status = 2')
                ->getQuery()->getSingleScalarResult();
//            $fechaCierre = $em->createQuery("SELECT c.fechaCierre CASE WHEN c.fechaCierre < NOW() THEN 'expirado' ELSE '' END FROM EntitiesBundle:Casos c");
            return $this->render('AfiliadosBundle:Default:index.html.twig', array(
                'username' => $user,
                'foto' => $foto,
                'pendientes' => $pendientes,
                'activos' => $activos,
                'inactivos' => $inactivos,
                'nivel' => $nivel,
                'casosabiertos' => $casosabiertos,
                'casoscerrados' => $casoscerrados,
//                'casosexpirados' => ($fechaCierre < new \DateTime('today')) ? '' : ''
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function afiliadosAction(Request $request)
    {
        /**
         * @Route("/afiliados", name="afiliados")
         */
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            $em = $this->getDoctrine()->getManager();
            $results = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->where('e.status BETWEEN 0 AND 1')
                ->orderBy('e.status', 'DESC')
                ->getQuery()
                ->getResult();
            $empresas = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->where('e.status = 1')
                ->getQuery()
                ->getResult();
            $paises = $em->getRepository('EntitiesBundle:Paises')
                ->createQueryBuilder('p')
                ->getQuery()
                ->getResult();
            $contactos = $em->getRepository('EntitiesBundle:Contactos')
                ->createQueryBuilder('c')
                ->where('c.status = 1')
                ->getQuery()
                ->getResult();
            $regiones = $em->getRepository('EntitiesBundle:Region')
                ->createQueryBuilder('r')
                ->getQuery()
                ->getResult();
            return $this->render('AfiliadosBundle:Default:afiliados.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'results' => $results,
                'paises' => $paises,
                'empresas' => $empresas,
                'nivel' => $this->getUser()->getTipoUsuario(),
                'contactos' => $contactos,
                'regiones' => $regiones
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function afiliadosDetallesAction($id, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            $impuestos = $em->getRepository('EntitiesBundle:Archivos')->findOneBy(['rif' => $id]);
            $pendiente = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->where('e.rif = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
            $paises = $em->getRepository('EntitiesBundle:Paises')
                ->createQueryBuilder('p')
                ->getQuery()
                ->getResult();
            $grupos = $em->getRepository('EntitiesBundle:GrupoEmpresa')
                ->createQueryBuilder('g')
                ->getQuery()
                ->getResult();
            $segmentos = $em->getRepository('EntitiesBundle:SegmentoEmpresa')
                ->createQueryBuilder('s')
                ->getQuery()
                ->getResult();
            $sectores = $em->getRepository('EntitiesBundle:Sector')
                ->createQueryBuilder('sc')
                ->getQuery()
                ->getResult();
            return $this->render('AfiliadosBundle:Default:detallesafiliados.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'p' => $pendiente,
                'asistencia' => $empresa->getIdAsistenciaTecnica(),
                'capital' => $empresa->getIdCapital(),
                'productosEmpresa' => $empresa->getIdProductosEmpresa(),
                'representantes' => $empresa->getIdRepresentante(),
                'exportaciones' => $empresa->getIdExportacion(),
                'archivos' => $empresa->getIdArchivo(),
                'islr' => ($impuestos != NULL) ? $impuestos->getIdIslr() : '',
                'iva' => ($impuestos != NULL) ? $impuestos->getIdIva() : '',
                'productoControlado' => $empresa->getIdProductoControlado(),
                'grupos' => $grupos,
                'segmentos' => $segmentos,
                'sectores' => $sectores,
                'sectorAfiliado' => $empresa->getIdSector(),
                'paises' => $paises,
                'nivel' => $this->getUser()->getTipoUsuario()
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function editarEmpresaAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $nombre = $request->get('nombreEmpresa');
            $rif = $request->get('rifEmpresa');
            $registro = $request->get('registroEmpresa');
            $fundacion = $request->get('fundacionEmpresa');
            $actividad = $request->get('actividadEmpresa');
            $direccionFiscal = $request->get('direccionFiscalEmpresa');
            $faxOficina = $request->get('faxOficina');
            $telefonoOficina = $request->get('telefonoOficina');
            $ventasBrutas = $request->get('ventasBrutas');
            $sitioWeb = $request->get('webEmpresa');
            $telefonoPlanta = $request->get('telefonoPlanta');
            $faxPlanta = $request->get('faxPlanta');
            $correo = $request->get('correoEmpresa');
            $direccionOficina = $request->get('direccionOficina');
            $direccionPlanta = $request->get('direccionPlanta');
            $nombreEjecutivo = $request->get('nombrePrincipalPersonal');
            $cargoEjecutivo = $request->get('cargoPrincipalPersonal');
            $correoEjecutivo = $request->get('correoPrincipalPersonal');
            $cantidadAdministrativo = $request->get('cantidadAdministrativo');
            $cantidadAlmacen = $request->get('cantidadAlmacen');
            $codigoAfiliado = $request->get('codigoAfiliado');
            $fechaAprobacion = $request->get('fechaAprobacion');
            $tipoMiembro = $request->get('tipoMiembro');
            $grupoAfiliado = $request->get('grupoAfiliado');
            $segmentoAfiliado = $request->get('segmentoAfiliado');
            $empresaAdherenteSi = $request->get('empresaAdherenteSi');
            $empresaAdherenteNo = $request->get('empresaAdherenteNo');
            $status = $request->get('status');
            $idGrupo = $request->get('grupoAfiliado');
            $idSegmento = $request->get('segmentoAfiliado');
            $nit = ($actividad == 1) ? $request->get('nit') : '';

            $empresaOrigen = $request->get('empresaOrigenAsistencia');
            $serviciosAsistencia = $request->get('serviciosAsistencia');
            $responsableAsistencia = $request->get('responsableAsistencia');
            $materiaPrimaAsistencia = $request->get('materiaPrimaAsistencia');
            $productosAsistencia = $request->get('productosAsistencia');

            $porcentajeCapitalPrivado = $request->get('porcentajePrivadoCapital');
            $porcentajeCapitalExtranjero = $request->get('porcentajeExtranjeroCapital');
            $porcentajeCapitalPublico = $request->get('porcentajePublicoCapital');
            $principalAccionista = $request->get('principalAccionistaCapital');
            $casaMatriz = $request->get('nombreCasaMatrizCapital');

            $porcentajeActividad = $request->get('porcentajeActividadProductos');
            $capacidadInstalada = $request->get('capacidadInstaladaProductos');
            $productosFabrica = $request->get('productosFabricaProductos');

            $nombreRepresentante = $request->get('nombreRepresentante');
            $cargoRepresentante = $request->get('cargoRepresentante');
            $direccionOficinaRepresentante = $request->get('direccionRepresentante');
            $telefonoRepresentante = $request->get('telefonoRepresentante');
            $faxRepresentante = $request->get('faxRepresentante');
            $correoRepresentante = $request->get('correoRepresentante');

            $empresaReferencia = $request->get('empresaReferencia1');
            $personaReferencia = $request->get('personaReferencia1');
            $empresaReferencia2 = $request->get('empresaReferencia2');
            $personaReferencia2 = $request->get('personaReferencia2');

            $registroArchivo = $request->files->get('registroArchivo');
            $rifArchivo = $request->files->get('rifArchivo');
            ($actividad == 1) ? $nitArchivo = $request->files->get('nitArchivo') : '';
            $iva = $request->files->get('ivaArchivo');
            $islr = $request->files->get('impuestoArchivo');
            $rasda = $request->files->get('rasdaArchivo');
            $autorizacion = $request->files->get('autorizacionArchivo');
            $leyDrogas = $request->files->get('leyDrogasArchivo');
            $operador = $request->files->get('operadorArchivo');
            $archivoOpcional = $request->files->get('importanteArchivo');

            $paisOrigenAsistencia = $request->get('paisOrigenAsistencia');
            $paisOrigenCapital = $request->get('paisOrigenCapital');
            $paisDeExportacion = $request->get('paisExportacion');

            $sectorAfiliado = $request->get('sectorAfiliado');
            $em = $this->getDoctrine()->getManager();
            (!empty($paisOrigenAsistencia)) ? $paisAsistencia = $em->getRepository('EntitiesBundle:Paises')->find($paisOrigenAsistencia) : '';
            (!empty($paisOrigenCapital)) ? $paisCapital = $em->getRepository('EntitiesBundle:Paises')->find($paisOrigenCapital) : '';
            (!empty($paisDeExportacion)) ? $paisExportacion = $em->getRepository('EntitiesBundle:Paises')->find($paisDeExportacion) : '';
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($rif);
            $asistencia = $em->getRepository('EntitiesBundle:AsistenciaTecnica')->findOneBy(array('rif' => $rif));
            $capital = $em->getRepository('EntitiesBundle:Capital')->findOneBy(array('rif' => $rif));
            $productosEmpresa = $em->getRepository('EntitiesBundle:ProductosEmpresa')->findOneBy(array('rif' => $rif));
            $representantes = $em->getRepository('EntitiesBundle:Representantes')->findOneBy(array('rif' => $rif));
            $exportaciones = $em->getRepository('EntitiesBundle:Exportaciones')->findOneBy(array('rif' => $rif));
            $referencias = $em->getRepository('EntitiesBundle:Referencias')->findBy(array('rifRegistrado' => $rif));
            $archivos = $em->getRepository('EntitiesBundle:Archivos')->findOneBy(array('rif' => $rif));
            $valorAgregado = $em->getRepository('EntitiesBundle:Iva')->findBy(array('idArchivo' => $archivos));
            $impuestoRenta = $em->getRepository('EntitiesBundle:Islr')->findBy(array('idArchivo' => $archivos));
            $productosControlados = $em->getRepository('EntitiesBundle:ProductosControlados')->findOneBy(array('rif' => $rif));

            /**
             * CODIGO DE SECTOR
             * */

            $sector = $em->getRepository('EntitiesBundle:Sector')->findBy(['idSector' => $sectorAfiliado]);
            $bdArray = array();
            if (!empty($sectorAfiliado)) {

                $sectores = $em->getRepository('EntitiesBundle:Empresas')
                    ->createQueryBuilder('em')
                    ->select('em.rif AS rif, sc.idSector AS idSector')
                    ->innerJoin('em.idSector', 'sc')
                    ->where('em.rif = :rif')
                    ->setParameter('rif', $rif)
                    ->getQuery()
                    ->getResult();

                if (!empty($sectores)) {
                    if (count($sectorAfiliado) > count($sectores)) {
                        for ($j = 0; $j < count($sectores); $j++) {
                            //$bdArray = [$sectores[$j]['idSector']];
                            array_push($bdArray, $sectores[$j]['idSector']);
                        }
                        for ($i = 0; $i < count($sectorAfiliado); $i++) {
                            if (!in_array((int)$sectorAfiliado[$i], $bdArray)) {
                                $empresa->addIdSector($sector[$i]);
                            }
                        }
                        for ($k = 0; $k < count($sectores); $k++) {
                            if (!in_array($sectores[$k]['idSector'], $sectorAfiliado)) {
                                $eliminar = $em->getRepository('EntitiesBundle:Sector')->findOneBy(['idSector' => $sectores[$k]['idSector']]);
                                $empresa->removeIdSector($eliminar);
                            }
                        }
                    } else if (count($sectores) > count($sectorAfiliado)) {
                        for ($k = 0; $k < count($sectores); $k++) {
                            array_push($bdArray, $sectores[$k]['idSector']);
                        }
                        for ($i = 0; $i < count($sectores); $i++) {
                            if (!in_array((string)$sectores[$i]['idSector'], $sectorAfiliado)) {
                                $eliminar = $em->getRepository('EntitiesBundle:Sector')->findOneBy(['idSector' => $sectores[$i]['idSector']]);
                                $empresa->removeIdSector($eliminar);
                            }
                        }
                        for ($j = 0; $j < count($sectorAfiliado); $j++) {
                            //$bdArray = [$sectores[$j]['idSector']];
                            /*if (!in_array((string)$sectores[$j]['idSector'], $sectorAfiliado)) {
                                $empresa->addIdSector($sector[$j]);
                            } else */
                            if (!in_array((int)$sectorAfiliado[$j], $bdArray)) {
                                $empresa->addIdSector($sector[$j]);
                            }
                        }
                    } else if (count($sectores) == count($sectorAfiliado)) {
                        for ($i = 0; $i < count($sectorAfiliado); $i++) {
                            $bdArray = [$sectores[$i]['idSector']];
                            if (!in_array((int)$sectorAfiliado[$i], $bdArray)) {
                                $eliminar = $em->getRepository('EntitiesBundle:Sector')->findOneBy(['idSector' => $sectores[$i]['idSector']]);
                                $empresa->removeIdSector($eliminar);
                                $empresa->addIdSector($sector[$i]);
                            }
                        }
                    }
                } else {
                    for ($i = 0; $i < count($sectorAfiliado); $i++) {
                        $empresa->addIdSector($sector[$i]);
                    }
                }
            }

            $empresa->setNombre($nombre);
            $empresa->setRegistroEmpresa($registro);
            $empresa->setFundacion(new \DateTime($fundacion));
            $empresa->setActividad($actividad);
            $empresa->setDireccionFiscal($direccionFiscal);
            $empresa->setFaxOficina($faxOficina);
            $empresa->setTelefonoOficina($telefonoOficina);
            $empresa->setVentasBrutas($ventasBrutas);
            $empresa->setSitioWeb($sitioWeb);
            $empresa->setTelefonoPlanta($telefonoPlanta);
            $empresa->setFaxPlanta($faxPlanta);
            $empresa->setCorreo($correo);
            $empresa->setDireccionOficina($direccionOficina);
            $empresa->setDireccionPlanta($direccionPlanta);
            $empresa->setNombreEjecutivo($nombreEjecutivo);
            $empresa->setCargoEjecutivo($cargoEjecutivo);
            $empresa->setCorreoEjecutivo($correoEjecutivo);
            $empresa->setCantidadAdministrativo($cantidadAdministrativo);
            $empresa->setCantidadAlmacen($cantidadAlmacen);

//            $empresas = $this->getDoctrine()->getRepository('EntitiesBundle:EmpresasCasos')->findBy(['codigo' => $codigo]);
//            if (count($sectorAfiliado) > 1) {
//                for ($i = 0; $i < count($sectorAfiliado); $i++) {
//                    $sector = $this->getDoctrine()->getRepository('EntitiesBundle:Sector')->findOneBy(['idSector' => $sectorAfiliado[$i]]);
//                    $empresa[$i]->getIdSector();
////                    $empresas[$i]->setCodigo($caso);
////                    $empresas[$i]->setRif($rif);
////                    $empresas[$i]->setIdUsuario($id_usuario);
//                }
//            }
//            elseif (count($afiliado) === 1) {
//                for ($i = 0; $i < count($empresas); $i++) {
//                    $rif = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas')->findOneBy(['rif' => $afiliado]);
//                    $empresas[$i]->setCodigo($caso);
//                    $empresas[$i]->setRif($rif);
//                    $empresas[$i]->setIdUsuario($id_usuario);
//                }
//            }

            if ($status == 1) {
                $grupo = $em->getRepository('EntitiesBundle:GrupoEmpresa')->find($idGrupo);
                $segmento = $em->getRepository('EntitiesBundle:SegmentoEmpresa')->find($idSegmento);
                $empresa->setCodigoAfiliado($codigoAfiliado);
                $empresa->setFechaAprobacion(new \DateTime($fechaAprobacion));
                $empresa->setTipoMiembro($tipoMiembro);
                $empresa->setIdGrupo($grupo);
                $empresa->setIdSegmento($segmento);
                if ($empresaAdherenteSi) {
                    $empresa->setEmpresaAdherente(1);
                } else {
                    $empresa->setEmpresaAdherente(NULL);
                }
            }
            $empresa->setNit(($actividad == 1) ? $nit : NULL);
            //Referencias.
            $empresa->setEmpresaReferencia1($empresaReferencia);
            $empresa->setPersonaReferencia1($personaReferencia);
            $empresa->setEmpresaReferencia2($empresaReferencia2);
            $empresa->setPersonaReferencia2($personaReferencia2);

            if (!empty($asistencia)) {
                $asistencia->setEmpresaOrigen($empresaOrigen);
                $asistencia->setServicios($serviciosAsistencia);
                $asistencia->setResponsable($responsableAsistencia);
                $asistencia->setMateriaPrima($materiaPrimaAsistencia);
                $asistencia->setProductosComercializa($productosAsistencia);
                $asistencia->setIdPais($paisAsistencia);
            } else {
                $asistencia = new AsistenciaTecnica();
                $asistencia->setRif($empresa);
                $asistencia->setEmpresaOrigen($empresaOrigen);
                $asistencia->setServicios($serviciosAsistencia);
                $asistencia->setResponsable($responsableAsistencia);
                $asistencia->setMateriaPrima($materiaPrimaAsistencia);
                $asistencia->setProductosComercializa($productosAsistencia);
                $asistencia->setIdPais($paisAsistencia);
                $em->persist($asistencia);
            }
            if (!empty($capital)) {
                $capital->setPorentajeCapitalPrivado($porcentajeCapitalPrivado);
                $capital->setPorcentajeCapitalExtranjero($porcentajeCapitalExtranjero);
                $capital->setIdPais($paisCapital);
                $capital->setPorcentajeCapitalPublic($porcentajeCapitalPublico);
                $capital->setPrincipalAccionista($principalAccionista);
                $capital->setNombreCasaMatriz($casaMatriz);
            } else {
                $capital = new Capital();
                $capital->setPorentajeCapitalPrivado($porcentajeCapitalPrivado);
                $capital->setPorcentajeCapitalExtranjero($porcentajeCapitalExtranjero);
                $capital->setIdPais($paisCapital);
                $capital->setPorcentajeCapitalPublic($porcentajeCapitalPublico);
                $capital->setPrincipalAccionista($principalAccionista);
                $capital->setNombreCasaMatriz($casaMatriz);
                $capital->setRif($empresa);
                $em->persist($capital);
            }
            if (!empty($productosEmpresa)) {
                $productosEmpresa->setPorcentajeActividad($porcentajeActividad);
                $productosEmpresa->setCapacidadInstalada($capacidadInstalada);
                $productosEmpresa->setProductos($productosFabrica);
            } else {
                $productosEmpresa = new ProductosEmpresa();
                $productosEmpresa->setPorcentajeActividad($porcentajeActividad);
                $productosEmpresa->setCapacidadInstalada($capacidadInstalada);
                $productosEmpresa->setProductos($productosFabrica);
                $productosEmpresa->setRif($empresa);
                $em->persist($productosEmpresa);
            }
            if (!empty($representantes)) {
                $representantes->setNombre($nombreRepresentante);
                $representantes->setCargo($cargoRepresentante);
                $representantes->setDireccionOficina($direccionOficinaRepresentante);
                $representantes->setTelefono($telefonoRepresentante);
                $representantes->setFax($faxRepresentante);
                $representantes->setCorreo($correoRepresentante);
            } else {
                $representantes = new Representantes();
                $representantes->setNombre($nombreRepresentante);
                $representantes->setCargo($cargoRepresentante);
                $representantes->setDireccionOficina($direccionOficinaRepresentante);
                $representantes->setTelefono($telefonoRepresentante);
                $representantes->setFax($faxRepresentante);
                $representantes->setCorreo($correoRepresentante);
                $representantes->setRif($empresa);
                $em->persist($representantes);
            }
            if (!empty($exportaciones)) {
                $exportaciones->setIdPais($paisExportacion);
            } else {
                $exportaciones = new Exportaciones();
                $exportaciones->setIdPais($paisExportacion);
                $exportaciones->setRif($empresa);
                $em->persist($exportaciones);
            }
            if (!empty($archivos)) {
                if (!empty($registroArchivo)) {
                    $registroDeEmpresa = md5(uniqid()) . '.' . $registroArchivo->guessExtension();
                    $registroArchivo->move('assets/documentos_afiliados', $registroDeEmpresa);
                    $archivos->setRegistroEmpresa($registroDeEmpresa);
                }
                if (!empty($rifArchivo)) {
                    $rifDeEmpresa = md5(uniqid()) . '.' . $rifArchivo->guessExtension();
                    $rifArchivo->move('assets/documentos_afiliados', $rifDeEmpresa);
                    $archivos->setRifEmpresa($rifDeEmpresa);
                }
                if ($actividad == 1) {
                    if (!empty($nitArchivo)) {
                        $nitEmpresa = md5(uniqid()) . '.' . $nitArchivo->guessExtension();
                        $nitArchivo->move('assets/documentos_afiliados', $nitEmpresa);
                        $archivos->setNitEmpresa($nitEmpresa);
                    }
                }
                for ($i = 0; $i < 3; $i++) {
                    if ($iva[$i] != NULL) {
                        $ivaArchivo = md5(uniqid()) . '.' . $iva[$i]->guessExtension();
                        $iva[$i]->move('assets/documentos_afiliados', $ivaArchivo);
                        $valorAgregado[$i]->setIdArchivo($archivos);
                        $valorAgregado[$i]->setIva($ivaArchivo);
                    } else {
                        break;
                    }
                }
                for ($i = 0; $i < 3; $i++) {
                    if ($islr[$i] != NULL) {
                        $impuestoArchivo = md5(uniqid()) . '.' . $islr[$i]->guessExtension();
                        $islr[$i]->move('assets/documentos_afiliados', $impuestoArchivo);
                        $impuestoRenta[$i]->setIdArchivo($archivos);
                        $impuestoRenta[$i]->setIslr($impuestoArchivo);
                    } else {
                        break;
                    }
                }
            } else {
                if (!empty($registroArchivo) && !empty($rifArchivo) && !empty($iva) && !empty($islr)) {
                    $registroDeEmpresa = md5(uniqid()) . '.' . $registroArchivo->guessExtension();
                    $rifDeEmpresa = md5(uniqid()) . '.' . $rifArchivo->guessExtension();
                    $registroArchivo->move('assets/documentos_afiliados', $registroDeEmpresa);
                    $rifArchivo->move('assets/documentos_afiliados', $rifDeEmpresa);
                    if ($actividad == 1) {
                        $nitEmpresa = md5(uniqid()) . '.' . $nitArchivo->guessExtension();
                        $nitArchivo->move('assets/documentos_afiliados', $nitEmpresa);
                    }
                    $archivos = new Archivos();
                    $archivos->setRif($empresa);
                    $archivos->setRegistroEmpresa($registroDeEmpresa);
                    $archivos->setRifEmpresa($rifDeEmpresa);
                    $archivos->setNitEmpresa(($actividad == 1) ? $nitEmpresa : NULL);
                    $em->persist($archivos);
                    for ($i = 0; $i < count($iva); $i++) {
                        $ivaArchivo = md5(uniqid()) . '.' . $iva[$i]->guessExtension();
                        $impuestoArchivo = md5(uniqid()) . '.' . $islr[$i]->guessExtension();
                        $iva[$i]->move('assets/documentos_afiliados', $ivaArchivo);
                        $islr[$i]->move('assets/documentos_afiliados', $impuestoArchivo);
                        $valorAgregado = new Iva();
                        $valorAgregado->setIdArchivo($archivos);
                        $valorAgregado->setIva($ivaArchivo);
                        $em->persist($valorAgregado);
                        $impuestoRenta = new Islr();
                        $impuestoRenta->setIdArchivo($archivos);
                        $impuestoRenta->setIslr($impuestoArchivo);
                        $em->persist($impuestoRenta);
                    }
                }
            }

            if (!empty($productosControlados)) {
                if (isset($rasda)) {
                    $rasdaEmpresa = md5(uniqid()) . '.' . $rasda->guessExtension();
                    $rasda->move('assets/documentos_afiliados', $rasdaEmpresa);
                    $productosControlados->setRegistroActividadesSuceptibles((isset($rasda) ? $rasdaEmpresa : NULL));
                }
                if (isset($autorizacion)) {
                    $autorizacionEmpresa = md5(uniqid()) . '.' . $autorizacion->guessExtension();
                    $autorizacion->move('assets/documentos_afiliados', $autorizacionEmpresa);
                    $productosControlados->setAutorizacionManejoSustancias(isset($autorizacion) ? $autorizacionEmpresa : NULL);
                }
                if (isset($leyDrogas)) {
                    $leyDrogasEmpresa = md5(uniqid()) . '.' . $leyDrogas->guessExtension();
                    $leyDrogas->move('assets/documentos_afiliados', $leyDrogasEmpresa);
                    $productosControlados->setLeyDrogas((isset($leyDrogas)) ? $leyDrogasEmpresa : NULL);
                }
                if (isset($operador)) {
                    $operadorEmpresa = md5(uniqid()) . '.' . $operador->guessExtension();
                    $operador->move('assets/documentos_afiliados', $operadorEmpresa);
                    $productosControlados->setPermisoOperadorExplosivos((isset($operador)) ? $operadorEmpresa : NULL);
                }
                if (isset($archivoOpcional)) {
                    $archivoOpcionalEmpresa = md5(uniqid()) . '.' . $archivoOpcional->guessExtension();
                    $archivoOpcional->move('assets/documentos_afiliados', $archivoOpcionalEmpresa);
                    $productosControlados->setArchivoOpcional((isset($archivoOpcional)) ? $archivoOpcionalEmpresa : NULL);
                }
            } else {
                $productosControlados = new ProductosControlados();
                $productosControlados->setRif($empresa);
                if (!empty($rasda)) {
                    $rasdaEmpresa = md5(uniqid()) . '.' . $rasda->guessExtension();
                    $rasda->move('assets/documentos_afiliados', $rasdaEmpresa);
                    $productosControlados->setRegistroActividadesSuceptibles($rasdaEmpresa);
                }
                if (!empty($autorizacion)) {
                    $autorizacionEmpresa = md5(uniqid()) . '.' . $autorizacion->guessExtension();
                    $autorizacion->move('assets/documentos_afiliados', $autorizacionEmpresa);
                    $productosControlados->setAutorizacionManejoSustancias($autorizacionEmpresa);
                }
                if (!empty($leyDrogas)) {
                    $leyDrogasEmpresa = md5(uniqid()) . '.' . $leyDrogas->guessExtension();
                    $leyDrogas->move('assets/documentos_afiliados', $leyDrogasEmpresa);
                    $productosControlados->setLeyDrogas($leyDrogasEmpresa);
                }
                if (!empty($operador)) {
                    $operadorEmpresa = md5(uniqid()) . '.' . $operador->guessExtension();
                    $operador->move('assets/documentos_afiliados', $operadorEmpresa);
                    $productosControlados->setPermisoOperadorExplosivos($operadorEmpresa);
                }
                if (!empty($archivoOpcional)) {
                    $archivoOpcionalEmpresa = md5(uniqid()) . '.' . $archivoOpcional->guessExtension();
                    $archivoOpcional->move('assets/documentos_afiliados', $archivoOpcionalEmpresa);
                    $productosControlados->setArchivoOpcional($archivoOpcionalEmpresa);
                }
                $em->persist($productosControlados);
            }
            $em->flush();
            return $this->redirectToRoute('afiliados_detalles', array('id' => $rif));
        }
    }

    public function contactoEmpresaAction(Request $request)
    {
        /**
         * @Route("/afiliados_contacto_empresa", name="afiliados_contacto_empresa")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $nombre = $request->get('nombre');
            $telefonoOficina = $request->get('telefonoOficina');
            $telefonoCelular = $request->get('telefonoCelular');
            $cargo = $request->get('cargo');
            $idRegion = $request->get('region');
            $correo = $request->get('correo');

            $empresa = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas')->find($id);
            $region = $this->getDoctrine()->getRepository('EntitiesBundle:Region')->find($idRegion);
            $contacto = new Contactos();
            $contacto->setNombre($nombre);
            $contacto->setTelefonoCelular($telefonoCelular);
            $contacto->setTelefonoOficina($telefonoOficina);
            $contacto->setCargo($cargo);
            $contacto->setId($region);
            $contacto->setCorreo($correo);
            $representante = $request->get('esRepresentante');
            if (isset($representante)) {
                $contacto->setRepresentante(TRUE);
            } else {
                $contacto->setRepresentante(FALSE);
            }
            $contacto->setStatus(1);
            $contacto->setRif($empresa);
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            return $this->redirectToRoute('afiliados_contactos');
        }
    }

    public
    function grafica2Action(Request $request)
    {
        /**
         * @Route("/afiliados_grafica2", name="afiliados_grafica2")
         */
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getRepository('EntitiesBundle:Empresas');
            $pendientes = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 3')
                ->where('e.fechaSolicitud BETWEEN :desde AND :hasta')
                ->setParameters(array('desde' => $request->get('desde'), 'hasta' => $request->get('hasta')))
                ->getQuery()->getSingleScalarResult();
            $activos = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 1')
                ->where('e.fechaSolicitud BETWEEN :desde AND :hasta')
                ->setParameters(array('desde' => $request->get('desde'), 'hasta' => $request->get('hasta')))
                ->getQuery()->getSingleScalarResult();
            $inactivos = $em->createQueryBuilder('e')
                ->select('count(e.status)')
                ->where('e.status = 0')
                ->where('e.fechaSolicitud BETWEEN :desde AND :hasta')
                ->setParameters(array('desde' => $request->get('desde'), 'hasta' => $request->get('hasta')))
                ->getQuery()->getSingleScalarResult();
            return new Response('<html>
            <body>
              <div id="donut">
                <script type="text/javascript">
                  Morris.Donut({
                  element: "donut",
                  resize: true,
                  data: [
                  {label: "Activos", value:' . $activos . '},
                  {label: "Pendientes", value:' . $pendientes . '},
                  {label: "Inactivos", value:' . $inactivos . '}
                  ]
                  });
                </script>
              </div>

            </body>
          </html>
          ');
        }
    }

    public
    function nuevaEmpresaAction(Request $request)
    {
        /**
         * @Route("/afiliados_nuevos", name="afiliados_nuevos")
         */
        if ($request->isMethod('POST')) {
            try {
                $currentUrl = $request->get('currentUrl');
                //Empresas
                $nombre = $request->get('nombreEmpresa');
                $rif = $request->get('rifEmpresa');
                $registro = $request->get('registroEmpresa');
                $fundacion = $request->get('fundacionEmpresa');
                $actividad = $request->get('actividadEmpresa');
                $direccionFiscal = $request->get('direccionFiscalEmpresa');
                $faxOficina = $request->get('faxOficina');
                $telefonoOficina = $request->get('telefonoOficina');
                $ventasBrutas = $request->get('ventasBrutas');
                $webEmpresa = $request->get('webEmpresa');
                $telefonoPlanta = $request->get('telefonoPlanta');
                $faxPlanta = $request->get('faxPlanta');
                $correoEmpresa = $request->get('correoEmpresa');
                $direccionOficina = $request->get('direccionOficina');
                $direccionPlanta = $request->get('direccionPlanta');
                $responsable = $request->get('responsableEmpresa');
                if ($actividad == 1) {
                    $nit = $request->get('nit');
                }
                //Asistencia Tecnica
                $paisAsistencia = $request->get('paisOrigenAsistencia');
                $paisOrigenAsistencia = $this->getDoctrine()
                    ->getRepository('EntitiesBundle:Paises')
                    ->find($paisAsistencia);
                $empresaAsistencia = $request->get('empresaOrigenAsistencia');
                $serviciosAsistencia = $request->get('serviciosAsistencia');
                $responsableAsistencia = $request->get('responsableAsistencia');
                $materiaPrimaAsistencia = $request->get('materiaPrimaAsistencia');
                $productosAsistencia = $request->get('productosAsistencia');
                //Capital
                $porcentajePrivado = $request->get('porcentajePrivadoCapital');
                $porcentajeExtranjero = $request->get('porcentajeExtranjeroCapital');
                $paisCapital = $request->get('paisOrigenCapital');
                $paisOrigenCapital = $this->getDoctrine()
                    ->getRepository('EntitiesBundle:Paises')
                    ->find($paisCapital);
                $porcentajePublico = $request->get('porcentajePublicoCapital');
                $principalAccionista = $request->get('principalAccionistaCapital');
                $nombreMatriz = $request->get('nombreCasaMatrizCapital');
                //Productos Empresa
                $porcentajeActividad = $request->get('porcentajeActividadProductos');
                $capacidadInstalada = $request->get('capacidadInstaladaProductos');
                $productosFabrica = $request->get('productosFabricaProductos');
                //Personal Empresa
                $nombrePrincipal = $request->get('nombrePrincipalPersonal');
                $cargoPrincipal = $request->get('cargoPrincipalPersonal');
                $correoPrincipal = $request->get('correoPrincipalPersonal');
                //Personal ocupado empresa
                $cantidadAdministrativo = $request->get('cantidadAdministrativo');
                $cantidadAlmacen = $request->get('cantidadAlmacen');
                //Representantes
                $nombreRepresentante = $request->get('nombreRepresentante');
                $cargoRepresentante = $request->get('cargoRepresentante');
                $direccionRepresentante = $request->get('direccionRepresentante');
                $telefonoRepresentante = $request->get('telefonoRepresentante');
                $faxRepresentante = $request->get('faxRepresentante');
                $correoRepresentante = $request->get('correoRepresentante');
                //Referencias
                $empresaReferencia = $request->get('empresaReferencia1');
                $personaReferencia = $request->get('personaReferencia1');
                $empresaReferencia2 = $request->get('empresaReferencia2');
                $personaReferencia2 = $request->get('personaReferencia2');
                //Exportaciones
                $paisExportacion = $request->get('paisExportacion');
                $paisAlQueExporta = $this->getDoctrine()
                    ->getRepository('EntitiesBundle:Paises')
                    ->find($paisExportacion);
                //Archivos
                $registroArchivo = $request->files->get('registroArchivo');
                $rifArchivo = $request->files->get('rifArchivo');
                $nitArchivo = $request->files->get('nitArchivo');
                //Productos controlados
                $rasda = $request->files->get('rasdaArchivo');
                $autorizacion = $request->files->get('autorizacionArchivo');
                $leyDrogas = $request->files->get('leyDrogasArchivo');
                $operador = $request->files->get('operadorArchivo');
                $archivoOpcional = $request->files->get('importanteArchivo');
                //IVA
                $iva = $request->files->get('ivaArchivo');
                //ISLR
                $islr = $request->files->get('impuestoArchivo');
                //Inserciones
                $empresa = new Empresas();
                $empresa->setRif($rif);
                $empresa->setNombre($nombre);
                $empresa->setRegistroEmpresa($registro);
                $empresa->setFundacion(new \DateTime($fundacion));
                $empresa->setActividad($actividad);
                $empresa->setDireccionFiscal($direccionFiscal);
                $empresa->setFaxOficina($faxOficina);
                $empresa->setTelefonoOficina($telefonoOficina);
                $empresa->setVentasBrutas($ventasBrutas);
                $empresa->setSitioWeb($webEmpresa);
                $empresa->setTelefonoPlanta($telefonoPlanta);
                $empresa->setFaxPlanta($faxPlanta);
                $empresa->setCorreo($correoEmpresa);
                $empresa->setDireccionOficina($direccionOficina);
                $empresa->setDireccionPlanta($direccionPlanta);
                $empresa->setResponsable($responsable);
                $empresa->setStatus(3);
                $empresa->setFechaCreacion(new \DateTime('now'));
                $empresa->setNombreEjecutivo($nombrePrincipal);
                $empresa->setCargoEjecutivo($cargoPrincipal);
                $empresa->setCorreoEjecutivo($correoPrincipal);
                $empresa->setCantidadAdministrativo($cantidadAdministrativo);
                $empresa->setCantidadAlmacen($cantidadAlmacen);
                $empresa->setNit(($actividad == 1) ? $nit : NULL);
                //Referencia 1
                $empresa->setEmpresaReferencia1($empresaReferencia);
                $empresa->setPersonaReferencia1($personaReferencia);
                //Referencia 2
                $empresa->setEmpresaReferencia2($empresaReferencia2);
                $empresa->setPersonaReferencia2($personaReferencia2);
                $asistencia = new AsistenciaTecnica();
                $em = $this->getDoctrine()->getManager();
                $em->persist($empresa);
                $asistencia->setRif($empresa);
                $asistencia->setIdPais($paisOrigenAsistencia);
                $asistencia->setEmpresaOrigen($empresaAsistencia);
                $asistencia->setServicios($serviciosAsistencia);
                $asistencia->setResponsable($responsableAsistencia);
                $asistencia->setMateriaPrima($materiaPrimaAsistencia);
                $asistencia->setProductosComercializa($productosAsistencia);
                $em->persist($asistencia);
                $capital = new Capital();
                $capital->setPorentajeCapitalPrivado($porcentajePrivado);
                $capital->setPorcentajeCapitalExtranjero($porcentajeExtranjero);
                $capital->setIdPais($paisOrigenCapital);
                $capital->setPorcentajeCapitalPublic($porcentajePublico);
                $capital->setPrincipalAccionista($principalAccionista);
                $capital->setNombreCasaMatriz($nombreMatriz);
                $capital->setRif($empresa);
                $em->persist($capital);
                $productosEmpresa = new ProductosEmpresa();
                $productosEmpresa->setPorcentajeActividad($porcentajeActividad);
                $productosEmpresa->setCapacidadInstalada($capacidadInstalada);
                $productosEmpresa->setProductos($productosFabrica);
                $productosEmpresa->setRif($empresa);
                $em->persist($productosEmpresa);
                /*for ($i = 0; $i < count($empresaReferencia); $i++) {
                    if ($empresaReferencia[$i] != NULL AND $personaReferencia[$i] != NULL) {
                        $referenciaEmpresa = $this->getDoctrine()
                            ->getRepository('EntitiesBundle:Empresas')
                            ->find($empresaReferencia[$i]);
                        $referenciaPersona = $this->getDoctrine()
                            ->getRepository('EntitiesBundle:Contactos')
                            ->find($personaReferencia[$i]);
                        $referencias = new Referencias();
                        $referencias->setRif((isset($empresaReferencia)) ? $referenciaEmpresa : NULL);
                        $referencias->setIdContacto((isset($personaReferencia)) ? $referenciaPersona : NULL);
                        $referencias->setRifRegistrado($empresa);
                        $em->persist($referencias);
                    }
                }*/
                $representante = new Representantes();
                $representante->setRif($empresa);
                $representante->setNombre($nombreRepresentante);
                $representante->setCargo($cargoRepresentante);
                $representante->setDireccionOficina($direccionRepresentante);
                $representante->setTelefono($telefonoRepresentante);
                $representante->setFax($faxRepresentante);
                $representante->setCorreo($correoRepresentante);
                $em->persist($representante);
                if ($request->get('paisExportaSi')) {
                    $exportaciones = new Exportaciones();
                    $exportaciones->setRif($empresa);
                    $exportaciones->setIdPais($paisAlQueExporta);
                    $em->persist($exportaciones);
                }
                if (!empty($registroArchivo) && !empty($rifArchivo) && !empty($iva) && !empty($islr)) {
                    $registroDeEmpresa = md5(uniqid()) . '.' . $registroArchivo->guessExtension();
                    $rifDeEmpresa = md5(uniqid()) . '.' . $rifArchivo->guessExtension();
                    $registroArchivo->move('assets/documentos_afiliados', $registroDeEmpresa);
                    $rifArchivo->move('assets/documentos_afiliados', $rifDeEmpresa);
                    if ($actividad == 1) {
                        $nitEmpresa = md5(uniqid()) . '.' . $nitArchivo->guessExtension();
                        $nitArchivo->move('assets/documentos_afiliados', $nitEmpresa);
                    }
                    $archivos = new Archivos();
                    $archivos->setRif($empresa);
                    $archivos->setRegistroEmpresa($registroDeEmpresa);
                    $archivos->setRifEmpresa($rifDeEmpresa);
                    $archivos->setNitEmpresa(($actividad == 1) ? $nitEmpresa : NULL);
                    $em->persist($archivos);
                    for ($i = 0; $i < count($iva); $i++) {
                        $ivaArchivo = md5(uniqid()) . '.' . $iva[$i]->guessExtension();
                        $impuestoArchivo = md5(uniqid()) . '.' . $islr[$i]->guessExtension();
                        $iva[$i]->move('assets/documentos_afiliados', $ivaArchivo);
                        $islr[$i]->move('assets/documentos_afiliados', $impuestoArchivo);
                        $valorAgregado = new Iva();
                        $valorAgregado->setIdArchivo($archivos);
                        $valorAgregado->setIva($ivaArchivo);
                        $em->persist($valorAgregado);
                        $impuestoRenta = new Islr();
                        $impuestoRenta->setIdArchivo($archivos);
                        $impuestoRenta->setIslr($impuestoArchivo);
                        $em->persist($impuestoRenta);
                    }
                } else {
                    echo 'No se cargaron los archivos necesarios';
                }
                if (isset($rasda)) {
                    $rasdaEmpresa = md5(uniqid()) . '.' . $rasda->guessExtension();
                    $rasda->move('assets/documentos_afiliados', $rasdaEmpresa);
                }
                if (isset($autorizacion)) {
                    $autorizacionEmpresa = md5(uniqid()) . '.' . $autorizacion->guessExtension();
                    $autorizacion->move('assets/documentos_afiliados', $autorizacionEmpresa);
                }
                if (isset($leyDrogas)) {
                    $leyDrogasEmpresa = md5(uniqid()) . '.' . $leyDrogas->guessExtension();
                    $leyDrogas->move('assets/documentos_afiliados', $leyDrogasEmpresa);
                }
                if (isset($operador)) {
                    $operadorEmpresa = md5(uniqid()) . '.' . $operador->guessExtension();
                    $operador->move('assets/documentos_afiliados', $operadorEmpresa);
                }
                if (isset($archivoOpcional)) {
                    $archivoOpcionalEmpresa = md5(uniqid()) . '.' . $archivoOpcional->guessExtension();
                    $archivoOpcional->move('assets/documentos_afiliados', $archivoOpcionalEmpresa);
                }
                $productosControlados = new ProductosControlados();
                $productosControlados->setRif((isset($empresa)) ? $empresa : NULL);
                $productosControlados->setRegistroActividadesSuceptibles((isset($rasda) ? $rasdaEmpresa : NULL));
                $productosControlados->setAutorizacionManejoSustancias(isset($autorizacionEmpresa) ? $autorizacionEmpresa : NULL);
                $productosControlados->setLeyDrogas((isset($leyDrogasEmpresa)) ? $leyDrogasEmpresa : NULL);
                $productosControlados->setPermisoOperadorExplosivos((isset($operadorEmpresa)) ? $operadorEmpresa : NULL);
                $productosControlados->setArchivoOpcional((isset($archivoOpcionalEmpresa)) ? $archivoOpcionalEmpresa : NULL);
                $em->persist($productosControlados);
                $em->flush();
                if ($currentUrl == 'afiliados_dashboard') {
                    return $this->redirectToRoute('afiliados_pendientes');
                } else {
                    return new RedirectResponse('http://www.asoquim.com/');
                }
            } catch (UniqueConstraintViolationException $e) {
                echo "El RIF que ha insertado ya se encuentra en uso.";
            }
        }
    }

    public
    function pendientesAction()
    {
        /**
         * @Route("/pendientes", name="pendientes")
         */
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $paises = $em->getRepository('EntitiesBundle:Paises')
                ->createQueryBuilder('p')
                ->getQuery()
                ->getResult();
            $grupos = $em->getRepository('EntitiesBundle:GrupoEmpresa')
                ->createQueryBuilder('g')
                ->getQuery()
                ->getResult();
            $segmentos = $em->getRepository('EntitiesBundle:SegmentoEmpresa')
                ->createQueryBuilder('s')
                ->getQuery()
                ->getResult();
            $pendientes = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->where('e.status = 3')
                ->getQuery()
                ->getResult();
            $empresas = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->getQuery()
                ->getResult();
            $sectores = $em->getRepository('EntitiesBundle:Sector')
                ->createQueryBuilder('sc')
                ->getQuery()
                ->getResult();
            return $this->render('AfiliadosBundle:Default:pendientes.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'nivel' => $this->getUser()->getTipoUsuario(),
                'paises' => $paises,
                'grupos' => $grupos,
                'segmentos' => $segmentos,
                'pendientes' => $pendientes,
                'empresas' => $empresas,
                'sectores' => $sectores
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public
    function contactosAction(Request $request)
    {
        /**
         * @Route("/contactos", name="contactos")
         */
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $empresas = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->select('e.nombre, e.rif')
                ->getQuery()
                ->getResult();
            $regiones = $em->getRepository('EntitiesBundle:Region')
                ->createQueryBuilder('r')
                ->getQuery()
                ->getResult();
            $con = $this->getDoctrine()->getRepository('EntitiesBundle:Contactos');
            $qb = $con->createQueryBuilder('c')
                ->orderBy('c.status', 'DESC')
                ->getQuery();
            $results = $qb->getResult();
            $contacto = new Contactos();
            $form = $this->createForm(ContactosType::class, $contacto);
            $form->handleRequest($request);
            if ($request->isMethod('POST')) {
                $representante = $request->get('esRepresentante');
                if (isset($representante)) {
                    $contacto->setRepresentante(TRUE);
                } else {
                    $contacto->setRepresentante(FALSE);
                }
                $em->persist($contacto);
                $em->flush();
                return $this->redirectToRoute('afiliados_contactos');
            }
            return $this->render('AfiliadosBundle:Default:contactos.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'results' => $results,
                'form' => $form->createView(),
                'nivel' => $this->getUser()->getTipoUsuario(),
                'empresas' => $empresas,
                'regiones' => $regiones
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public
    function desactivarContactoAction(Request $request)
    {
        /**
         * @Route("/desactivar_contacto", name="desactivar_contacto")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('contacto');
            $em = $this->getDoctrine()->getManager();
            $contacto = $em->getRepository('EntitiesBundle:Contactos')->find($id);
            if (!$contacto) {
                throw $this->createNotFoundException("No se encontro un contacto con el id " . $id);
            }
            $contacto->setStatus(0);
            $em->flush();
            return new JsonResponse(array('success' => TRUE));
        }
    }

    public
    function activarContactoAction(Request $request)
    {
        /**
         * @Route("/activar_contacto", name="activar_contacto")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('contacto');
            $em = $this->getDoctrine()->getManager();
            $contacto = $em->getRepository('EntitiesBundle:Contactos')->find($id);
            if (!$contacto) {
                throw $this->createNotFoundException("No se encontro un contacto con el id " . $id);
            }
            $contacto->setStatus(1);
            $em->flush();
            return new JsonResponse(array('success' => TRUE));
        }
    }

    public
    function editarContactoAction(Request $request)
    {
        /**
         * @Route("/editar_contacto", name="editar_contacto")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('idContacto');
            $nombre = $request->get('nombre');
            $telefonoOficina = $request->get('telefonoOficina');
            $telefonoCelular = $request->get('telefonoCelular');
            $cargo = $request->get('cargo');
            $correo = $request->get('correo');
            $rif = $request->get('rif');
            $idRegion = $request->get('region');
            $representante = $request->get('esRepresentante');
            $empresa = $this->getDoctrine()
                ->getRepository('EntitiesBundle:Empresas')
                ->find($rif);
            $em = $this->getDoctrine()->getManager();
            $contacto = $em->getRepository('EntitiesBundle:Contactos')->find($id);
            $region = $em->getRepository('EntitiesBundle:Region')->find($idRegion);
            if (!$contacto) {
                throw $this->createNotFoundException("No se encontro un contacto con el id " . $id);
            }
            $listas = $em->getRepository('EntitiesBundle:DetallesLista')->findByIdContacto($contacto);
            foreach ($listas as $lista) {
                if ($lista->getIdLista()->getIdGrupo()) {
                    $mailer = new \comunicadeBundle\Resources\Modelos\MailReporter('ampusers', 'amp12345');
                    $respuesta = $mailer->UserEdit($lista->getIdLista()->getNombre(), $contacto->getCorreo(), $correo);
                }
            }
            $contacto->setNombre($nombre);
            $contacto->setTelefonoOficina($telefonoOficina);
            $contacto->setTelefonoCelular($telefonoCelular);
            $contacto->setCargo($cargo);
            $contacto->setCorreo($correo);
            $contacto->setRif($empresa);
            $contacto->setId($region);
            if (isset($representante)) {
                $contacto->setRepresentante(TRUE);
            } else {
                $contacto->setRepresentante(FALSE);
            }
            $em->flush();
            return $this->redirectToRoute('afiliados_contactos');
        }
    }

    public
    function pagosAfiliadosAction($id, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();
            $qb = $this->getDoctrine()->getEntityManager();
            $empresa = $this->getDoctrine()
                ->getRepository('EntitiesBundle:Empresas')
                ->find($id);
            $datosEmpresa = $em->getRepository('EntitiesBundle:Empresas')
                ->createQueryBuilder('e')
                ->select('e.nombre, e.fechaCobranza')
                ->where('e.rif = :rif')
                ->setParameter('rif', $id)
                ->getQuery()->getOneOrNullResult();
            $pago = $em->getRepository('EntitiesBundle:Pagos')
                ->createQueryBuilder('p')
                ->where('p.rif = :rif')
                ->setParameter('rif', $empresa)
                ->getQuery()->getResult();
            $pagos = new Pagos();
            $form = $this->createForm(PagosType::class, $pagos);
            $form->handleRequest($request);
            if ($request->isMethod('POST')) {
                $pagos->setRif($empresa);
                $em->persist($pagos);
                $em->flush();
                return $this->redirectToRoute('afiliados_pagos', array('id' => $id));
            }
            return $this->render('AfiliadosBundle:Default:pagosafiliados.html.twig', array(
                'username' => $this->getUser()->getUsername(),
                'foto' => $this->getUser()->getFoto(),
                'form' => $form->createView(),
                'results' => $pago,
                'nivel' => $this->getUser()->getTipoUsuario(),
                'afiliado' => $datosEmpresa
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }


    public function fechaCobranzaAction($id, Request $request)
    {
        if ($request->isMethod('POST')) {
            $fecha = $request->get('fechaCobranza');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            if (!$empresa) {
                return $this->createNotFoundException("No se encontro una empresa con el rif " . $id);
            }
            $empresa->setFechaCobranza($fecha);
            $em->flush($empresa);
            return $this->redirectToRoute('afiliados_pagos', array('id' => $id));
        }
    }

    public function editarFechaCobranzaAction($id, Request $request)
    {
        if ($request->isMethod('POST')) {
            $fecha = $request->get('fechaCobranza');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            if (!$empresa) {
                return $this->createNotFoundException("No se encontró una empresa con el rif " . $id);
            }
            $empresa->setFechaCobranza($fecha);
            $em->flush();
            return $this->redirectToRoute('afiliados_pagos', array('id' => $id));
        }
    }

    public
    function editarPagoAction(Request $request)
    {
        /**
         * @Route("/editar_pago", name="editar_pago")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('idPago');
            $rif = $request->get('rif');
            $fechaPago = $request->get('fechaPago');
            $fechaCobranza = $request->get('fechaCobranza');
            $monto = $request->get('monto');
            $descripcion = $request->get('descripcion');
            $empresa = $this->getDoctrine()
                ->getRepository('EntitiesBundle:Empresas')
                ->find($rif);
            $em = $this->getDoctrine()->getManager();
            $pago = $em->getRepository('EntitiesBundle:Pagos')->find($id);
            if (!$pago) {
                throw $this->createNotFoundException("No se encontro un contacto con el id " . $id);
            }
            $pago->setFechaPago(new \DateTime($fechaPago));
            $pago->setFechaCobranza(new \DateTime($fechaCobranza));
            $pago->setMonto($monto);
            $pago->setRif($empresa);
            $pago->setDescripcion($descripcion);
            $em->flush();
            return $this->redirectToRoute('afiliados_pagos', array('id' => $rif));
        }
    }

    public
    function eliminarPagoAction(Request $request)
    {
        /**
         * @Route("/eliminar_pago", name="eliminar_pago")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();
            $pago = $em->getRepository('EntitiesBundle:Pagos')->find($id);
            $em->remove($pago);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public
    function activarAfiliadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $rif = $request->get('rif');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($rif);
            if (!$rif) {
                throw $this->createNotFoundException("No se encontro un afiliado con el rif " . $rif);
            }
            $empresa->setStatus(1);
            $empresa->setRazonDesactivacion(NULL);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public
    function desactivarAfiliadoAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $rif = $request->get('rif');
            $razon = $request->get('razon');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($rif);
            if (!$empresa) {
                throw $this->createNotFoundException("No se encontro un afiliado con el rif " . $rif);
            }
            $empresa->setStatus(0);
            $empresa->setRazonDesactivacion($razon);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public
    function aprobarAfiliadoAction(Request $request)
    {
        /**
         * @Route("/aprobar_empresa", name="aprobar_empresa")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('empresaAprobada');
            $codigoAfiliado = $request->get('codigoAfiliado');
            $fechaAprobacion = $request->get('fechaAprobacion');
            $tipoMiembro = $request->get('tipoMiembro');
            $empresaAdherente = $request->get('empresaAdherente');
            $grupoAfiliado = $request->get('grupoAfiliado');
            $segmentoAfiliado = $request->get('segmentoAfiliado');
            $sectorAfiliado = $request->get('sectorAfiliado');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            $grupo = $em->getRepository('EntitiesBundle:GrupoEmpresa')->find($grupoAfiliado);
            $segmento = $em->getRepository('EntitiesBundle:SegmentoEmpresa')->find($segmentoAfiliado);
            if (!$empresa) {
                throw $this->createNotFoundException("No se encontro un afiliado con el rif " . $id);
            }
            $empresa->setCodigoAfiliado($codigoAfiliado);
            $empresa->setFechaAprobacion(new \DateTime($fechaAprobacion));
            $empresa->setTipoMiembro(($tipoMiembro == 1) ? 'Adherente' : 'Activo');
            $empresa->setIdGrupo($grupo);
            $empresa->setIdSegmento($segmento);
            $empresa->setStatus(1);
            for ($i = 0; $i < count($sectorAfiliado); $i++) {
                $sector = $em->getRepository('EntitiesBundle:Sector')->find($sectorAfiliado[$i]);
                $empresa->addIdSector($sector);
            }
            if (isset($empresaAdherente)) {
                $empresa->setEmpresaAdherente(TRUE);
            } else {
                $empresa->setEmpresaAdherente(FALSE);
            }
            $em->flush();
            return $this->redirectToRoute('afiliados_pendientes');
        }
    }

    public
    function rechazarAfiliadoAction(Request $request)
    {
        /**
         * @Route("/rechazar_empresa", name="rechazar_empresa")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('rif');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            if (!$empresa) {
                throw $this->createNotFoundException("No se encontro un afiliado con el rif " . $id);
            }
            $em->remove($empresa);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public
    function cancelarAfiliadoAction(Request $request)
    {
        /**
         * @Route("/cancelar_empresa", name="cancelar_empresa")
         */
        if ($request->isMethod('POST')) {
            $id = $request->get('rif');
            $razon = $request->get('razon');
            $em = $this->getDoctrine()->getManager();
            $empresa = $em->getRepository('EntitiesBundle:Empresas')->find($id);
            $empresa->setStatus(2);
            $empresa->setRazonCancelacion($razon);
            $em->flush();
            return new JsonResponse(array(
                'success' => TRUE
            ));
        }
    }

    public
    function contactoReferenciaAction(Request $request)
    {
        /**
         * @Route("/contactos_referencia", name="contactos_referencia")
         */
        if ($request->isMethod('POST')) {
            $rif = $request->get('rif');
            $em = $this->getDoctrine()->getManager();
            $query = $em->getRepository('EntitiesBundle:Contactos')
                ->createQueryBuilder('c')
                ->select('c.idContacto, c.nombre')
                ->where('c.status = 1')
                ->andWhere('c.rif = :rif')
                ->setParameter('rif', $rif)
                ->getQuery()
                ->getResult();
//            $query = $em->createQuery('SELECT c.idContacto, c.nombre FROM EntitiesBundle:Contactos c WHERE c.status = 1 AND c.rif = :rif')
//                ->setParameter('rif', $rif);
//            $result = $query->getResult();
            if ($query) {
                return new JsonResponse(array(
                    'success' => TRUE,
                    'contactos' => $query
                ));
            } else {
                return new JsonResponse(array(
                    'success' => FALSE
                ));
            }
        }
    }

}